$(document).ready(function() {
    var Getpath = window.location.href;
   

    /*######################################################################################recordin List*/
    if (Getpath.indexOf("admin/app/recording/list") != -1) {
   /*     $(".sonata-bc").append(`<div class="backloadingRecord"><div class="spinner"><div class="cube1"></div><div class="cube2"></div></div></div>`);
        var arrayListRecordingToggle = [];
        var countTr = $('.table').children('tbody').children('tr:first').children('td').length -1;
        $(".table tbody tr").each(function(index) {
            var test =  $.trim($(this).find('td:nth-child('+countTr+')').text());
            var testt = $.trim($(this).find('td:nth-child(2)').text());
            var test1 =test.replace(/\s/g, '');
            var test2 = testt.replace(/\s/g, '');
           
            var Concat = test1 + test2;
            $(this).addClass(Concat);
            // $(this).find('.btn-show-all').addClass(Concat+"b");
            arrayListRecordingToggle.push(Concat);

        });

        function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
        }
        var uniqueList = arrayListRecordingToggle.filter(onlyUnique);
        console.log(uniqueList);
    
        var checkerLoading = false;
        for (var i = 0; i < uniqueList.length; i++) {
            $('.' + uniqueList[i]).each(function(index) {
                if (index == 0) {
                    $(this).find('.sonata-ba-list-field-actions').append('<a data-check-class="' + uniqueList[i] + '" class="btn btn-info btn-sm ToggleRecord "><i class="fa fa-arrow-circle-down" aria-hidden="true"></i>&nbsp;&nbsp; D-P/S-DATE</a>');
                    $(this).removeClass(uniqueList[i]);
                }
                if (index != 0) {
                    $(this).hide();
                    checkerLoading = true;
                }

            });
        }
        setInterval(function() {
            $('.backloadingRecord').hide();
        }, 4500)

        $('.ToggleRecord').on('click', function() {
            var cheker = false;
            var dataclassS = $(this).attr('data-check-class');
            $(".table tbody tr").each(function(index) {
                if (index != 0) {
                    if ($(this).hasClass(dataclassS)) {
                        $(this).toggle("slow");
                        $(this).toggleClass('border-row-recording');
                        cheker = true;
                    }
                }
            });
            if (cheker == false) {
                swal({
                    title: '',
                    text: 'There are no other lines containing same date and production department!',
                    type: 'info',
                    timer: 2500,
                    allowOutsideClick: true,
                });
            } else {
                $("i", this).toggleClass("fa-arrow-circle-down fa-arrow-circle-up");

                $(this).toggleClass("btn-info btn-success");
            }

        });*/
    }
    /*######################################################################################recordin List*/

    if (Getpath.indexOf('/admin/app/farms/list') != -1 || Getpath.indexOf('/admin/app/farms/list') != -1 || Getpath.indexOf('/admin/app/productiondepartement/list') != -1 ||
        Getpath.indexOf('/admin/app/cardlocation/list') != -1 || Getpath.indexOf('/admin/app/recording/list') != -1 || Getpath.indexOf('/admin/app/crop/list') != -1 ||
        Getpath.indexOf('/admin/app/bug/list') != -1 || Getpath.indexOf('/admin/app/croptype/list') != -1 || Getpath.indexOf('/admin/app/bugtype/list') != -1 || Getpath.indexOf('/admin/app/user/list') != -1) {
        //alert(1);
        $('.breadcrumb').find('li:nth-child(2)').remove();
    }
    if(Getpath.indexOf('search') === -1){
        $('.box').parent('div').parent('div').addClass('NotDeleteBox');
        $('.cms-block-element').parent('div').hide();
        $('.NotDeleteBox').show();

    }

    $(".closeE").click(function() {
        $("#myModal").hide();
    });
    $(".table").on('click', ".imgModal", function() {
        var modal = $("#myModal"),
            img = $(this).attr("src");
        modal.find("img").attr("src", img);
        modal.show();

    });


    $('.picker-switch').hide();
    if (Getpath.indexOf('/cropblock/create') != -1 || Getpath.indexOf('/cardlocation/create') != -1) {
        
        var d = new Date();
        var datestring = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
        $('.StartDateMyDate').val(datestring);
    }


    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.split(search).join(replacement);
    };

    var TextSwitch = $('.form-inline').find('.pull-right').find('.btn-group').clone();
    $('.form-inline').find('.pull-right').find('.btn-group').remove();
    if (Getpath.indexOf("admin/app/user/list") === -1) {
        $(TextSwitch).appendTo('.navbar-collapse');
        $('.navbar-collapse').find('.btn-group').addClass('nav navbar-nav navbar-right');
        $('.navbar-collapse').find('.btn-group').find('button').addClass('btngrouShare');
        $('.navbar-collapse').find('.btn-group').find('button').html('<i class="fa fa-share-square-o"></i>&nbsp;&nbsp;Share');
    }


    if (Getpath.indexOf("/admin/app/bug/list") != -1 || Getpath.indexOf("/admin/app/croptype/list") != -1 ||
        Getpath.indexOf("/admin/app/crop/list") != -1 || Getpath.indexOf("/admin/app/bugtype/list") != -1 || Getpath.indexOf("/admin/app/user/list") != -1) {

        $(`<div class="nav navbar-nav navbar-right " id="Dswitch"><label class="switch"><input type="checkbox"><span class="slider All"><span class="spanAll">&nbsp;Yes</span>&nbsp;&nbsp;<span class="spanAll">All</span> </span></label></div>`).appendTo('.navbar-collapse');
        $('.slider').on('click', function() {
            $(this).toggleClass('All Yes');
            if ($(this).hasClass("Yes")) {
                $('.sonata-ba-list-field-boolean').find('.label-danger').closest('tr').hide();
            } else {
                $('.sonata-ba-list-field-boolean').find('.label-danger').closest('tr').show();
            }
        });
        $('.slider').trigger('click');
    }
    if($('.btncheckadmin').length >0)
    {
        $('#Dswitch').hide();
    }
    if (Getpath.indexOf("/admin/app/cropblock/list") != -1 || Getpath.indexOf("/admin/app/cardlocation/list") != -1) {

        $(`<div class="nav navbar-nav navbar-right " id="Dswitch"><label class="switchCC"><input type="checkbox"><span class="sliderCC All"><span class="spanAll">&nbsp;Not end Date</span>&nbsp;&nbsp;<span style="margin-left: 42px;"class="spanAll">All</span> </span></label></div>`).appendTo('.navbar-collapse');
        $('.sliderCC').on('click', function() {
            $(this).toggleClass('All Not');
            if ($(this).hasClass("Not")) {
                if (Getpath.indexOf("/admin/app/cardlocation/list") != -1) {
                    $(".table tbody tr td:nth-child(3)").each(function() {
                        var test = $(this).text();
                        if (test.length != 65) {
                            $(this).addClass("DateEndH")
                        }
                    });
                    $('.DateEndH').closest('tr').hide();
                } else {
                    $(".table tbody tr td:nth-child(5)").each(function() {
                        var test = $(this).text();
                        if (test.length != 65) {
                            $(this).addClass("DateEndH")
                        }
                    });
                    $('.DateEndH').closest('tr').hide();
                }

            } else {
                $('.DateEndH').closest('tr').show();
            }
            $(".EndDateH:empty").parent().remove();
        });
        // $('.slider').trigger('click');
    }
  

    if (Getpath.indexOf("/edit") != -1) {
        $('.sticky-wrapper').remove();
    }
    if (Getpath.indexOf("/list?filter") === -1 || Getpath.indexOf("/list?filters=reset") != -1) {
        $('.sonata-filters-box').hide();
    }

    $('.user-menu').find('.dropdown-toggle').remove();
    $('.user-menu').find('li').first().remove();
    $('.user-footer').unwrap();
    $('.pull-left').unwrap();

    /*##############################################################################*/
    $('.navbar-custom-menu').find('.navbar-nav').prepend('<li id="dropDrop" class="dropdown"></li>');
    var ItemTableMenu = $('.navbar-custom-menu').find('.navbar-nav').find('.dropdown-add').clone();
    $("#dropDrop").html(ItemTableMenu);
    $("#dropDrop").prepend('<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true"> <i class="fa fa-table fa-fw" aria-hidden="true"></i> <i class="fa fa-caret-down" aria-hidden="true"></i> </a>');
    $("#dropDrop a").each(function() {
        var hrefA = $(this).attr('href');
        var hrefB = hrefA.replaceAll("create", "list");
        $(this).attr('href', hrefB);
    });
    /*##############################################################################*/

    $('.fa-plus-circle').removeClass('fa-plus-circle').addClass('fa-plus-square');
    $('.fa-minus-circle').removeClass('fa-plus-circle').addClass('fa-trash');
    $('.fa-list').removeClass('fa-list').addClass('fa-table');
    var btn_createEdit = $("button[name='btn_create_and_edit']");
    var btn_update_and_edit = $("button[name='btn_update_and_edit']");
    if (btn_createEdit.length) {
        btn_createEdit.remove();
    }
    if (btn_update_and_edit.length && Getpath.indexOf('admin/app/user/') === -1 && Getpath.indexOf('/edit') === -1) {
        btn_update_and_edit.remove();
    }
    var hiddenUser = $('.hiddenUser');
    if (hiddenUser.length) {
        hiddenUser.hide();
        hiddenUser.parent('div').parent('div').hide();
    }


    $('.sonata-ba-field').find('.date').parent('.input-group').css('width', '100%');
    $('.sonata-ba-field').find('.date').css('width', '100%');
    var classToggle = $('.btn-toggle-info');
    if (classToggle.length > 0) {
        $(`<div class="modal fade" id="myModal" role="dialog"> <div class="modal-dialog"><div class="modal-content"> <div class="modal-header"> <button type="button" class="close" data-dismiss="modal">&times;</button> <h4 class="modal-title text-info text-center">Clone Crop Block and change</h4> </div> <div class="modal-body"> <div class="form-group"> <label for="cropClone">Name *</label> <input type="text" style="width: 100%;" class="form-control" id="nameClone" name="nameClone" > </div> <div class="form-group"> <label for="cropClone">Crop *</label> <select style="width: 100%;" class="selectCropJs form-control" id="cropClone" name="cropClone" ></select> </div>  <div class="form-group"> <label for="cropClone">End Date *</label> <div style="width:100%" class='input-group date from-control' id='endtDatePicker'> <input type='text' class="form-control" name="endDate" id="endDateClone" /> <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> </div> </div> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> <a class="btn btn-success vailder-new-crop" >valider</a> </div> </div> </div></div> <a style="margin-top: 15px; float:right" class="btn btn-info btnclone" data-toggle="modal" data-target="#myModal"><i class="fa fa-files-o" aria-hidden="true"></i>&nbsp;Clone and Change</a><a id="toggle-btn" style="margin-top: 15px; float:right" class="btn btn-primary"><i class="fa fa-arrow-circle-down" aria-hidden="true"></i>&nbsp;&nbsp;GPS info</a>`).insertAfter(classToggle.closest('div'));
        $('.hideToggle').parent('div').parent('div').hide();
        $('.polygoninformation ').parent('div').parent('div').css('margin-top', '48px');
        $('select.cropBlockClone').find('option').clone().appendTo('.selectCropJs');
        $('.js-example-basic-single').select2({
            minimumResultsForSearch: -1
        });
        /*$('#startDatePicker').datetimepicker({
            format: 'YYYY-MM-D'
        });*/
        $('#endtDatePicker').datetimepicker({
            format: 'YYYY-MM-D'
        });
        /*#aa#*/
        /*        <div class="form-group"> <label for="cropClone">Start Date *:</label> <div style="width:100%" class='input-group date from-control' id='startDatePicker'> <input type='text' class="form-control" name="startdateClone" id="startDateClone"/> <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span> </span> </div> </div>
        */       
     $(document).on('click', 'a.vailder-new-crop', function() {
            var name = $('#nameClone').val();
            /*var startDate = $('#startDateClone').val(); startDate: startDate,*/
            var crop = $('#cropClone').val();
            var endDate = $('#endDateClone').val();

            if (name != "" && crop != "" && endDate != "") {
                var objectNew = {
                    name: name,
                    
                    crop: crop,
                    endDate: endDate
                }
                var jsonNew = JSON.stringify(objectNew);
                $('.cloneAndChange').val(jsonNew);
                $('#myModal').modal('hide');
                
                swal("Good job!", "Cloned successfully", "success");
                return;
            } else {
                swal("Recall!", "You must fill in the mandatory fields if you want to clone the information from crop block to other", "warning");
            }
        });

        $('#toggle-btn').on('click', function() {

            $('.hideToggle').parent('div').parent('div').slideToggle("slow");
            $("i", this).toggleClass("fa-arrow-circle-down fa-arrow-circle-up");
            $(this).toggleClass("btn-primary btn-success");
        });
    }

    if (Getpath.indexOf('/cropblock/create') === -1 ) {
        $('.btnclone').remove();
    }
    var tabhidden = $('.nav-tabs-custom');
    if (tabhidden.length > 0) {
        tabhidden.find('.nav-tabs').hide();
    }
    if (Getpath.indexOf('/create') != -1 && Getpath.indexOf('/app/user/create') === -1) {
        $('.icheckbox_square-blue').addClass('checked');
    }

    if (Getpath.indexOf('/create') != -1 && Getpath.indexOf('/app/user/create') === -1) {
        $('.icheckbox_square-blue').addClass('checked');
    }
    /*################################################################################################################################*/
    /*######################################################## my Function  ###########################################################*/


    function changeMyStringToFloat(MyString) {

        var niveau1 = MyString.replaceAll("}", " "),
            niveau2 = niveau1.replaceAll("{lat:", " "),
            niveau3 = niveau2.replaceAll("lng :", ","),
            niveau4 = niveau3.replaceAll("}", " "),
            niveau5 = niveau4.replaceAll("},", " "),
            niveau6 = niveau5.replaceAll(",,", ",");

        var temp = new Array();
        temp = niveau6.split(",");
        for (a in temp) {
            temp[a] = parseFloat(temp[a]);
        }

        var PolygonArray = [];

        for (var i = 1; i < temp.length - 1; i += 2) {
            var b = new Object();
            b = {
                lat: temp[i - 1],
                lng: temp[i]
            };

            //console.log(b);
            PolygonArray.push(b);


        }
        //console.log(PolygonArray);

        return PolygonArray
    }

    function changeMyString(StringPolygonInfo) {

                niveau1 = StringPolygonInfo.replaceAll("),", "}"),
                niveau2 = niveau1.replaceAll("(", "{lat:"),
                niveau3 = niveau2.replaceAll(",", ",lng :"),
                niveau4 = niveau3.replaceAll("}", "},");
                niveau5 = niveau4.replaceAll(")", "},");


            return niveau5;
        }
        /*################################################################################################################################*/
        /*######################################################## my function end ########################################################################*/

    /*################################################################################################################################*/
    /*######################################################## my function DB ########################################################################*/


    var overlays = [];
    var markers = [];

    function removePolygon() {
        for (var i = 0; i < overlays.length; i++) {
            overlays[i].setMap(null);
        }
        overlays = [];
    }

    function removeMarkers() {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
    }

    function setMarkers(latt, lngg) {
        var shape = {
            coords: [0, 0, 200, 0, 0, 200],
            type: 'poly'
        };
        var lat1 = latt;
        var lang1 = langg;

        var marker = new google.maps.Marker({
            position: {
                lat: latt,
                lng: lngg
            },
            map: map,
            shape: shape,
            icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
            zIndex: 1
        });

        markers.push(marker);

    }

    function setMarkerslatlangname(lat, lang, name) {
        removeMarkers();
        var shape = {
            coords: [10, 1, 10, 20, 18, 20, 18, 1],
            type: 'poly'
        };
        marker = new google.maps.Marker({
            position: {
                lat: parseFloat(lat),
                lng: parseFloat(lang),
            },
            animation: google.maps.Animation.BOUNCE,
            label: {
                text: name,
                color: 'white'
            },
            map: map,
            shape: shape,
            icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
            zIndex: 1
        });


        markers.push(marker);
    }

    function setMarkerslatlangnameNotRemovelastMarker(lat, lang, name) {

        var shape = {
            coords: [10, 1, 10, 20, 18, 20, 18, 1],
            type: 'poly'
        };
        marker = new google.maps.Marker({
            position: {
                lat: parseFloat(lat),
                lng: parseFloat(lang),
            },
            animation: google.maps.Animation.BOUNCE,
            label: {
                text: name,
                color: 'white'
            },
            map: map,
            shape: shape,
            icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
            zIndex: 1
        });


        markers.push(marker);
    }


    if (Getpath.indexOf("/farms/create") != -1 ||
        Getpath.indexOf("/productiondepartement/create") != -1 ||
        Getpath.indexOf("/cardlocation/create") != -1 ||
        Getpath.indexOf("/cropblock/create") != -1 || Getpath.indexOf("/recording/create") != -1 || (Getpath.indexOf("/edit") != -1 && (Getpath.indexOf("app/farms/") || Getpath.indexOf("app/productiondepartement/") || Getpath.indexOf("app/cardlocation/") || Getpath.indexOf("app/cropblock/")))

    ) {
        $(".tab-content .tab-pane .MapMap .sonata-ba-collapsed-fields").attr("id", "map");

        $(".mapRecording  .sonata-ba-collapsed-fields").attr("id", "map");


        if ($("#map").length) {

            var map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: 52.0412,
                    lng: -67.6666
                },
                zoom: 6,
                mapTypeId: google.maps.MapTypeId.HYBRID,
                labels: true
            });

            var drawingManager = new google.maps.drawing.DrawingManager({
                //drawingMode: google.maps.drawing.OverlayType.MARKER,
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: ['polygon']
                },
                polygonOptions: {
                    fillColor: '#4dff4d',
                    fillOpacity: 0.6,
                    strokeWeight: 3,
                    clickable: false,
                    zIndex: 1
                }

            });
            drawingManager.setMap(map);
            /*#########################################################Search adress################################################################*/
            $("<div class 'col-lg-6'><input type='text' placeholder='Search Adress' id='search-in-map' class='form-control'/></div>").insertBefore("#map");

            var input = document.getElementById("search-in-map");
            // disactivate enter key onkeypress event
            jQuery('#search-in-map').on('keypress', function(e) {
                if (e.keyCode === 13) e.preventDefault();
            });
            var autocomplete = new google.maps.places.Autocomplete(input);

            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("Please enter a valid address! Or click on the address of the list!");
                    return;
                }

                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(14); // Why 17? Because it looks good.
                }
                return false;
            });
            /*#########################################################End search adress######################################################################*/

            /*########################################################## Map function #########################################################*/

            google.maps.event.addListener(drawingManager, 'polygoncomplete', function(pol) {
                InfoPolygon = pol.getPath().getArray().toString();
                var InfoPolChange = changeMyString(InfoPolygon);
                var arrayOfcenter = polygonCenter(pol);
                var centerOfPolyLat = parseFloat(arrayOfcenter[0]);
                var centerOfPolyLang = parseFloat(arrayOfcenter[1]);



                $('.polygoninformation').val(InfoPolChange);
                $('.centerOfPolyLat').val(centerOfPolyLat);
                $('.centerOfPolyLang').val(centerOfPolyLang);

                var area = $('.area');
                if (area.length) {
                    var areaPoly = google.maps.geometry.spherical.computeArea(pol.getPath());
                    $('.area').val(Number(areaPoly).toFixed(2));

                }
            });

            google.maps.event.addListener(drawingManager, 'markercomplete', function(mar) {
                var markerLat = mar.getPosition().lat().toString();
                var markerLng = mar.getPosition().lng().toString();
                $('.latitude').val(markerLat);
                $('.longitude').val(markerLng);

            });

            google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {


                if (event.type == 'polygon') {

                    swal("Good job!", "Draw successfully", "success");
                }
                if (event.type == 'marker') {

                    swal("Good job!", "You successfully placed the card", "success");
                }

            });

            var oldZoom = null;
            var oldCenter = null;
            google.maps.event.addListener(map, "zoom_changed", function() {
                oldZoom = map.getZoom();
                $('.positionzoom').val(oldZoom);
            
            });
            google.maps.event.addListener(map, "center_changed", function() {
                oldCenter = map.getCenter();
                var latitude = oldCenter.lat();
                var longitude = oldCenter.lng();
                $('.positionlat').val(latitude);
                $('.positionlang').val(longitude);
              

            });

            function polygonCenter(poly) {
                var lowx,
                    highx,
                    lowy,
                    highy,
                    lats = [],
                    lngs = [],
                    vertices = poly.getPath();

                for (var i = 0; i < vertices.length; i++) {
                    lngs.push(vertices.getAt(i).lng());
                    lats.push(vertices.getAt(i).lat());
                }

                lats.sort();
                lngs.sort();
                lowx = lats[0];
                highx = lats[vertices.length - 1];
                lowy = lngs[0];
                highy = lngs[vertices.length - 1];
                center_x = lowx + ((highx - lowx) / 2);
                center_y = lowy + ((highy - lowy) / 2);
                var arrayPolyu = [];
                arrayPolyu.push(center_x);
                arrayPolyu.push(center_y);

                return arrayPolyu;
            };
            /* marker.addListener('click', function() {
                 alert(1);
                         });
                */

            /*################################################################################################################################*/
            /*########################################################end function  map ########################################################################*/




            /***********************Production departement**************************************/


            $('.farm-cordonner').on('change', function() {
                var FarmId = $(this).val();
                var DATA = 'FarmId=' + FarmId;
                $('#map').append('<div class="loader"></div>');

                $.ajax({
                    type: "GET",
                    url: "/admin/app/productiondepartement/coordinates_polygon",
                    data: DATA,
                    dataType: "json",
                    success: function(data) {
                        removePolygon();
                        var PolygonCoords = changeMyStringToFloat(data[0].polygoninformation);

                        // Construct the polygon.
                        var SetPolygonInfo = new google.maps.Polygon({
                            paths: PolygonCoords,
                            strokeColor: '#000000',
                            strokeOpacity: 0.9,
                            strokeWeight: 3,
                            fillColor: '#ff0000',
                            fillOpacity: 0.4
                        });
                        ///$('.loader').hide();
                        SetPolygonInfo.setMap(map);
                        overlays.push(SetPolygonInfo);
                        var myLatlng = new google.maps.LatLng(data[0].positionlat, data[0].positionlang);
                        map.setCenter(myLatlng);
                        map.setZoom(data[0].positionzoom);



                        $.ajax({
                            type: "GET",
                            url: "/admin/app/productiondepartement/coordinates_polygon_production_departemnet",
                            data: DATA,
                            dataType: "json",
                            success: function(dataa) {
                                if (dataa.length != 0) {
                                    var PolygonCoordsDepartement = [];
                                    var SetPolygonInfoDepartement = [];
                                    for (var i = 0; i < dataa.length; i++) {
                                        PolygonCoordsDepartement[i] = changeMyStringToFloat(dataa[i].polygoninformation);
                                        // Construct the polygon.
                                        SetPolygonInfoDepartement[i] = new google.maps.Polygon({
                                            paths: PolygonCoordsDepartement,
                                            strokeColor: '#000000',
                                            strokeOpacity: 0.9,
                                            strokeWeight: 3,
                                            fillColor: '#ffa500',
                                            fillOpacity: 0.4
                                        });
                                        SetPolygonInfoDepartement[i].setMap(map);
                                        overlays.push(SetPolygonInfoDepartement[i]);
                                        var shape = {
                                            coords: [10, 1, 10, 20, 18, 20, 18, 1],
                                            type: 'poly'
                                        };
                                        marker = new google.maps.Marker({
                                            position: {
                                                lat: dataa[i].centerOfPolyLat,
                                                lng: dataa[i].centerOfPolyLang
                                            },
                                            //animation: google.maps.Animation.BOUNCE,
                                            label: {
                                                text: dataa[i].name,
                                                color: 'white'
                                            },
                                            map: map,
                                            shape: shape,

                                            zIndex: 1
                                        });
                                        var Latbounce = parseFloat($('.centerOfPolyLat').val());
                                        var langbounce = parseFloat($('.centerOfPolyLang').val());


                                        if (Latbounce == dataa[i].centerOfPolyLat && langbounce == dataa[i].centerOfPolyLang) {
                                            marker.setAnimation(google.maps.Animation.BOUNCE);
                                        }


                                        $('.loader').hide();
                                    }

                                } else {
                                    $('.loader').hide();
                                }

                            },
                            error: function(msg) {
                                console.log(msg);
                                $('.loader').hide();

                            }
                        });


                    },
                    error: function(msg) {
                        console.log(msg);
                        $('.loader').hide();
                    }
                });
                return false;
            });
            /***********************end Production departement**************************************/
            /*********************** CroBlock **************************************/

            $('.Production-departement-cordonner').attr("disabled", 'disabled');

            $('.farms-departement-cordonner').on('change', function() {

                $('.Production-departement-cordonner').attr("disabled", 'disabled');
                $('.Production-departement-cordonner').find('option').remove().end();


                var FarmProductionId = $(this).val();
                var DATA = 'FarmProductionId=' + FarmProductionId;

                $.ajax({
                    type: "GET",
                    url: "/admin/app/cropblock/farm_production_departement_cropblock",
                    data: DATA,
                    dataType: "json",
                    success: function(data) {
                        $('.Production-departement-cordonner').prop('disabled', false);

                        var mySelect = $('.Production-departement-cordonner');
                        mySelect.append(
                            $("<option style='display:none;'></option>").val("").html("Choose a Production Departement")
                        );
                        for (var i = 0; i < data.length; i++) {
                            var val = data[i].id;
                            var name = data[i].name

                            mySelect.append(
                                $("<option style='display:none;'></option>").val(data[i].id).html(data[i].name)
                            );
                        }

                    },
                    error: function(msg) {
                        console.log(msg);
                        $('.loader').hide();
                    }
                });
                return false;

            });



            $('.Production-departement-cordonner').on('change', function() {

                var ProductionId = $(this).val();
                var DATA = 'ProductionId=' + ProductionId;

                $('#map').append('<div class="loader"></div>');

                $.ajax({
                    type: "GET",
                    url: "/admin/app/cropblock/coordinates_polygon_cropblock",
                    data: DATA,
                    dataType: "json",
                    success: function(data) {
                        removePolygon();
                        $('.loader').hide();
                        var myLatlng = new google.maps.LatLng(data[0].positionlat, data[0].positionlang);
                        map.setCenter(myLatlng);
                        map.setZoom(data[0].positionzoom);

                        $.ajax({
                            type: "GET",
                            url: "/admin/app/cropblock/departement_production_cropblock",
                            data: DATA,
                            dataType: "json",
                            success: function(dataa) {
                                if (dataa.length != 0) {
                                    var PolygonCoordsDepartement = [];
                                    var SetPolygonInfoDepartement = [];
                                    for (var i = 0; i < dataa.length; i++) {
                                        PolygonCoordsDepartement[i] = changeMyStringToFloat(dataa[i].polygoninformation);
                                        // Construct the polygon.
                                        SetPolygonInfoDepartement[i] = new google.maps.Polygon({
                                            paths: PolygonCoordsDepartement,
                                            strokeColor: '#000000',
                                            strokeOpacity: 0.9,
                                            strokeWeight: 3,
                                            fillColor: '#ffff33',
                                            fillOpacity: 0.4
                                        });
                                        SetPolygonInfoDepartement[i].setMap(map);
                                        overlays.push(SetPolygonInfoDepartement[i]);
                                        $('.loader').hide();
                                    }

                                } else {
                                    $('.loader').hide();
                                }

                            },
                            error: function(msg) {
                                console.log(msg);
                                $('.loader').hide();

                            }
                        });

                        var PolygonCoords = changeMyStringToFloat(data[0].polygoninformation);

                        // Construct the polygon.
                        var SetPolygonInfo = new google.maps.Polygon({
                            paths: PolygonCoords,

                            strokeColor: '#000000',
                            strokeOpacity: 0.9,
                            strokeWeight: 3,
                            fillColor: '#2A3F54',
                            fillOpacity: 0.4
                        });
                        SetPolygonInfo.setMap(map);
                        overlays.push(SetPolygonInfo);


                    },
                    error: function(msg) {
                        console.log(msg);
                        $('.loader').hide();
                    }
                });
                return false;
            });



            /*********************** end CroBlock **************************************/

            /*********************** CardLocation **************************************/

            $('.Production-departement-cordonner-cardLocation').attr("disabled", 'disabled');

            $('.farms-departement-cordonner-cardLocation').on('change', function() {

                $('.Production-departement-cordonner-cardLocation').attr("disabled", 'disabled');
                $('.Production-departement-cordonner-cardLocation').find('option').remove().end();


                var FarmProductionId = $(this).val();
                var DATA = 'FarmProductionId=' + FarmProductionId;

                $.ajax({
                    type: "GET",
                    url: "/admin/app/cropblock/farm_production_departement_cropblock",
                    data: DATA,
                    dataType: "json",
                    success: function(data) {
                        $('.Production-departement-cordonner-cardLocation').prop('disabled', false);
                        var mySelect = $('.Production-departement-cordonner-cardLocation');
                        mySelect.append(
                            $("<option style='display:none;'></option>").val("").html("Choose a Production Departement")
                        );
                        for (var i = 0; i < data.length; i++) {
                            var val = data[i].id;
                            var name = data[i].name

                            mySelect.append(
                                $("<option style='display:none;'></option>").val(data[i].id).html(data[i].name)
                            );
                        }

                    },
                    error: function(msg) {
                        console.log(msg);
                        $('.loader').hide();
                    }
                });
                return false;

            });



            $('.Production-departement-cordonner-cardLocation').on('change', function() {


                var productionDepartementId = $(this).val();
                var DATA = 'productionDepartementId=' + productionDepartementId;
                var PriorityMax = [];
                $('#map').append('<div class="loader"></div>');

                $.ajax({
                    type: "GET",
                    url: "/admin/app/cardlocation/coordinates_polygon_cardlocation",
                    data: DATA,
                    dataType: "json",
                    success: function(data) {
                        removePolygon();
                        var myLatlng = new google.maps.LatLng(data[0].positionlat, data[0].positionlang);
                        map.setCenter(myLatlng);
                        map.setZoom(data[0].positionzoom);

                        $.ajax({
                            type: "GET",
                            url: "/admin/app/cardlocation/cardlocation_markers",
                            data: DATA,
                            dataType: "json",
                            success: function(dataa) {
                                
                                $('.loader').hide();
                                removeMarkers();
                                
                                for (var i = 0; i < dataa.length; i++) {
                                    PriorityMax.push(dataa[i].priority);
                                       var  marker = new google.maps.Marker({

                                        position: {
                                            lat: parseFloat(dataa[i].latitude),
                                            lng: parseFloat(dataa[i].longitude),
                                        },
                                        map: map,
                                        icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                                        zIndex: 1
                                    });

                                    markers.push(marker);
                                }
                            var max = 0;
                                if(PriorityMax.length > 0){
                                     max = PriorityMax.reduce(function(a, b) {
                                        return Math.max(a, b);
                                    });
                                }

                              $('.priorityVal').val(max + 1);

                            },
                            error: function(msg) {
                                console.log(msg);
                                $('.loader').hide();
                            }
                        });



                        var PolygonCoords = changeMyStringToFloat(data[0].polygoninformation);
                        // Construct the polygon.
                        var SetPolygonInfo = new google.maps.Polygon({
                            paths: PolygonCoords,
                            strokeColor: '#000000',
                            strokeOpacity: 0.9,
                            strokeWeight: 3,
                            fillColor: '#2A3F54',
                            fillOpacity: 0.4
                        });
                        SetPolygonInfo.setMap(map);
                        overlays.push(SetPolygonInfo);


                    },
                    error: function(msg) {
                        console.log(msg);
                        $('.loader').hide();
                    }
                });
                return false;
            });


            /*********************** end CardLocation **************************************/



            /*################################################################################################################################*/
            /*######################################################## end my function DB ########################################################################*/

        } /* end if Length Map */
    } /* end if GetPath */

  

    /*****************trigger event*************************/
    var farmTrigger = $('.farm-cordonner').find(':selected').val();


    if (farmTrigger != "") {
        $('.farm-cordonner').val(farmTrigger).trigger('change');
    }
    if ((Getpath.indexOf("/app/productiondepartement") != -1 && Getpath.indexOf("/cardlocation/create") != -1) || Getpath.indexOf("/cardlocation/create") != -1) {



        if (drawingManager != null) {

            drawingManager.setOptions({
                drawingControl: false,
                drawingMode: null,
            });

            drawingManager.setOptions({
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: ['marker']
                },
                markerOptions: {
                    icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
                },

            });
        }

    }

    if (Getpath.indexOf("/app/productiondepartement") != -1 && Getpath.indexOf("/cardlocation/create") != -1) {

        $('.Production-departement-cordonner-cardLocation').prop('disabled', false);
        $('.farms-departement-cordonner-cardLocation').parent('div').parent('div').remove();
        var ProductionCardLocationTrigger = $('.Production-departement-cordonner-cardLocation').find(':selected').val();


        if (ProductionCardLocationTrigger != "") {
            $('.Production-departement-cordonner-cardLocation').val(ProductionCardLocationTrigger).trigger('change');
        }
    }

    if (Getpath.indexOf("/app/productiondepartement") != -1 && Getpath.indexOf("/cropblock/create") != -1) {

        $('.Production-departement-cordonner').attr("disabled", false);
        $('.Production-departement-cordonner').hide();
        $('.Production-departement-cordonner').parent('div').parent('div').hide();
        $('.farms-departement-cordonner').parent('div').parent('div').remove();
        var ProductionCrobBlockTrigger = $('.Production-departement-cordonner').find(':selected').val();

        if (ProductionCrobBlockTrigger != "") {
            $('.Production-departement-cordonner').val(ProductionCrobBlockTrigger).trigger('change');
        }
    }



    if (Getpath.indexOf("recording/create") != -1 && Getpath.indexOf("/admin/app/cardlocation/") != -1) {



        $(".card-location-recording").append('<div class="backloadingRecord"><div class="spinner"><div class="cube1"></div><div class="cube2"></div></div></div>');
        $.ajax({
            type: "GET",
            url: "/admin/app/recording/all_bugs",
            dataType: "json",
            success: function(datau) {

                setInterval(function() {
                    $('.backloadingRecord').hide();
                }, 1500);
                var html = "<thead><tr><th>Bug</th><th>Amount</th></tr></thead>";
                for (var i = 0; i < datau.length; i++) {
                    html += "<tr><td><label>" + datau[i].name + "-P" + datau[i].priority + "</label></td> <td><input class='amount form-control' placeholder='Amount " + " " + datau[i].name + "-P" + datau[i].priority + "' type='text' id=" + datau[i].id + "></td></tr>";
                }
                $('<table border="0" class="table table-striped" cellpadding="0" width="100%" id="providersFormElementsTable"><thead><tr></thead></table>').insertAfter(".card-location-recording");
                $("#providersFormElementsTable").html(html);
                $('#providersFormElementsTable').append('<br/>');

                $(".amount").on('keyup', function() {
                    var items = "";
                    $('.amount').each(function() {
                        var val = parseInt($(this).val());
                        var id = parseInt(this.id);
                        if (!isNaN(val)) {
                            items += "/" + id + "/" + val;
                        }
                    });
                    if (items.length) {
                        $(".listRecording").val(items);
                    }


                });



            },
            error: function(msg) {
                console.log(msg);

            }
        });



        var RecordingCardLocationTrigger = $('.card-location-recording').find(':selected').val();
        //$('.record').find('.sonata-ba-action').trigger('click');
        if (RecordingCardLocationTrigger.length) {

            $('#map').append('<div class="loader"></div>');
            if (drawingManager != null) {
                drawingManager.setOptions({
                    drawingControl: false,
                    drawingMode: null,
                });
            }
            var recdingcardlocationtext = $('.card-location-recording').find(':selected').text();
            var lat = $('.card-location-recording').find(':selected').attr('data-lat');
            var lang = $('.card-location-recording').find(':selected').attr('data-lang');
            var name = $('.card-location-recording').find(':selected').attr('data-name');


            var cardId = parseInt(RecordingCardLocationTrigger);
            $('.Production-departement-cordonner-recording').parent('div').parent('div').remove();

            var DATA = 'cardId=' + cardId;
            $.ajax({
                type: "GET",
                url: "/admin/app/cardlocation/cardlocation_show_production_departement",
                data: DATA,
                dataType: "json",
                success: function(dataa) {
                    var pordId = dataa[0].id;
                    var PolygonCoords = changeMyStringToFloat(dataa[0].polygoninformation);



                    var lat = dataa[0].ProductiondepartementCardlocation[0].latitude;
                    var lang = dataa[0].ProductiondepartementCardlocation[0].longitude;
                    var name = dataa[0].ProductiondepartementCardlocation[0].name;
                    var myLatlng = new google.maps.LatLng(dataa[0].ProductiondepartementCardlocation[0].positionlat, dataa[0].ProductiondepartementCardlocation[0].positionlang);
                    map.setCenter(myLatlng);
                    map.setZoom(dataa[0].ProductiondepartementCardlocation[0].positionzoom);
                    setMarkerslatlangname(lat, lang, name);



                    // Construct the polygon.
                    var SetPolygonInfo = new google.maps.Polygon({
                        paths: PolygonCoords,
                        strokeColor: '#000000',
                        strokeOpacity: 0.9,
                        strokeWeight: 3,
                        fillColor: '#ffa500',
                        fillOpacity: 0.4
                    });
                    SetPolygonInfo.setMap(map);
                    overlays.push(SetPolygonInfo);



                    $('.loader').hide();

                },
                error: function(msg) {
                    console.log(msg);
                    $('.loader').hide();

                }
            });

            return false;


        }



    }

    /*****************trigger event end*************************/
    /*######################################################## show  Map after listfiled production dept #######################################################################
    ###############################################################################################################################
    ###############################################################################################################################
    ###############################################################################################################################*/

    if (Getpath.indexOf("app/farms") != -1 && Getpath.indexOf("/productiondepartement/list") != -1) {
        $('.sticky-wrapper').find('.navbar-left').append('<ul class="nav navbar-nav navbar-left"><li><a class="sonata-action-element" href="##" onClick="history.go(-1); return false;"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a></li></ul>');
        var PathName = window.location.pathname;
        var IdFarm = PathName.match(/\d+/)[0];

        $('.content-wrapper').append('<div class="row row-map-add" ><div id="map"></div></div>');

        var map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: 52.0412,
                lng: -67.6666
            },
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.HYBRID,
            labels: true
        });

        var FarmId = IdFarm;
        var DATA = 'FarmId=' + FarmId;
        $('#map').append('<div class="loader"></div>');

        $.ajax({
            type: "GET",
            url: "/admin/app/productiondepartement/coordinates_polygon",
            data: DATA,
            dataType: "json",
            success: function(data) {

                removePolygon();
                var PolygonCoords = changeMyStringToFloat(data[0].polygoninformation);

                // Construct the polygon.
                var SetPolygonInfo = new google.maps.Polygon({
                    paths: PolygonCoords,
                    strokeColor: '#000000',
                    strokeOpacity: 0.9,
                    strokeWeight: 3,
                    fillColor: '#ff0000',
                    fillOpacity: 0.4
                });
                ///$('.loader').hide();
                SetPolygonInfo.setMap(map);
                overlays.push(SetPolygonInfo);
                var myLatlng = new google.maps.LatLng(data[0].positionlat, data[0].positionlang);
                map.setCenter(myLatlng);
                map.setZoom(data[0].positionzoom);



                $.ajax({
                    type: "GET",
                    url: "/admin/app/productiondepartement/coordinates_polygon_production_departemnet",
                    data: DATA,
                    dataType: "json",
                    success: function(dataa) {

                        if (dataa.length != 0) {

                            var PolygonCoordsDepartement = [];
                            var SetPolygonInfoDepartement = [];


                            for (var i = 0; i < dataa.length; i++) {
                                PolygonCoordsDepartement[i] = changeMyStringToFloat(dataa[i].polygoninformation);
                                // Construct the polygon.
                                SetPolygonInfoDepartement[i] = new google.maps.Polygon({
                                    paths: PolygonCoordsDepartement,
                                    strokeColor: '#000000',
                                    strokeOpacity: 0.9,
                                    strokeWeight: 3,
                                    fillColor: '#ffa500',
                                    fillOpacity: 0.4,

                                });
                                SetPolygonInfoDepartement[i].setMap(map);
                                overlays.push(SetPolygonInfoDepartement[i]);

                                var shape = {
                                    coords: [10, 1, 10, 20, 18, 20, 18, 1],
                                    type: 'poly'
                                };
                                marker = new google.maps.Marker({
                                    position: {
                                        lat: dataa[i].centerOfPolyLat,
                                        lng: dataa[i].centerOfPolyLang
                                    },
                                    //animation: google.maps.Animation.BOUNCE,
                                    label: {
                                        text: dataa[i].name,
                                        color: 'white'
                                    },
                                    map: map,
                                    shape: shape,

                                    zIndex: 1
                                });

                            }
                            $('.loader').hide();

                        } else {
                            $('.loader').hide();
                        }

                    },
                    error: function(msg) {
                        console.log(msg);
                        $('.loader').hide();

                    }
                });


            },
            error: function(msg) {
                console.log(msg);
                $('.loader').hide();
            }
        });
        return false;


    }

    /*######################################################## update farm  #######################################################################
  ###############################################################################################################################  */
    if (Getpath.indexOf("/app/farms") != -1 && Getpath.indexOf("/edit") != -1) {

        var polyUpdate = $('.polygoninformation').val();
        var lang = parseFloat($('.positionlang').val());
        var lat = parseFloat($('.positionlat').val());
        var zm = parseFloat($('.positionzoom').val());
        var myLatlngF = new google.maps.LatLng(lat, lang);

        map.setZoom(zm);
        map.setCenter(myLatlngF);
        polyUpdateCord = changeMyStringToFloat(polyUpdate);
        // Construct the polygon.
        polyUpdateE = new google.maps.Polygon({
            paths: polyUpdateCord,
            strokeColor: '#000000',
            strokeOpacity: 0.9,
            strokeWeight: 3,
            fillColor: '#ff0000',
            fillOpacity: 0.4,

        });
        polyUpdateE.setMap(map);
    }



    /*######################################################## show  Map after listfiled Crop block #######################################################################
    ###############################################################################################################################
    ###############################################################################################################################
    ###############################################################################################################################*/

    if (Getpath.indexOf("app/productiondepartement") != -1 && Getpath.indexOf("/cropblock/list") != -1) {
        $('.sticky-wrapper').find('.navbar-left').append('<ul class="nav navbar-nav navbar-left"><li><a class="sonata-action-element" href="##" onClick="history.go(-1); return false;"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a></li></ul>');

        var PathName = window.location.pathname;
        var ProdId = PathName.match(/\d+/)[0];
        var ProductionId = ProdId;

        $('.content-wrapper').append('<div class="row row-map-add" ><div id="map"></div></div>');

        var map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: 52.0412,
                lng: -67.6666
            },
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.HYBRID,
            labels: true
        });


        var DATA = 'ProductionId=' + ProductionId;

        $('#map').append('<div class="loader"></div>');

        $.ajax({
            type: "GET",
            url: "/admin/app/cropblock/coordinates_polygon_cropblock",
            data: DATA,
            dataType: "json",
            success: function(data) {
                removePolygon();
                $('.loader').hide();
                var myLatlng = new google.maps.LatLng(data[0].positionlat, data[0].positionlang);
                map.setCenter(myLatlng);
                map.setZoom(data[0].positionzoom);

                $.ajax({
                    type: "GET",
                    url: "/admin/app/cropblock/departement_production_cropblock",
                    data: DATA,
                    dataType: "json",
                    success: function(dataa) {
                        if (dataa.length != 0) {
                            var PolygonCoordsDepartement = [];
                            var SetPolygonInfoDepartement = [];
                            for (var i = 0; i < dataa.length; i++) {
                                PolygonCoordsDepartement[i] = changeMyStringToFloat(dataa[i].polygoninformation);
                                // Construct the polygon.
                                SetPolygonInfoDepartement[i] = new google.maps.Polygon({
                                    paths: PolygonCoordsDepartement,
                                    strokeColor: '#000000',
                                    strokeOpacity: 0.9,
                                    strokeWeight: 3,
                                    fillColor: '#ffa500',
                                    fillOpacity: 0.4
                                });
                                SetPolygonInfoDepartement[i].setMap(map);
                                overlays.push(SetPolygonInfoDepartement[i]);
                                var shape = {
                                    coords: [10, 1, 10, 20, 18, 20, 18, 1],
                                    type: 'poly'
                                };

                                marker = new google.maps.Marker({
                                    position: {
                                        lat: dataa[i].centerOfPolyLat,
                                        lng: dataa[i].centerOfPolyLang
                                    },
                                    //animation: google.maps.Animation.BOUNCE,
                                    label: {
                                        text: dataa[i].name,
                                        color: 'white'
                                    },
                                    map: map,
                                    shape: shape,

                                    zIndex: 1
                                });
                                $('.loader').hide();
                            }

                        } else {
                            $('.loader').hide();
                        }

                    },
                    error: function(msg) {
                        console.log(msg);
                        $('.loader').hide();

                    }
                });

                var PolygonCoords = changeMyStringToFloat(data[0].polygoninformation);

                // Construct the polygon.
                var SetPolygonInfo = new google.maps.Polygon({
                    paths: PolygonCoords,

                    strokeColor: '#000000',
                    strokeOpacity: 0.9,
                    strokeWeight: 3,
                    fillColor: '#ffa500',
                    fillOpacity: 0.4
                });
                SetPolygonInfo.setMap(map);
                overlays.push(SetPolygonInfo);


            },
            error: function(msg) {
                console.log(msg);
                $('.loader').hide();
            }
        });
        return false;


    }


    /*######################################################## show  Map after listfiled card location  #######################################################################
  ###############################################################################################################################
  ###############################################################################################################################
  ###############################################################################################################################*/

    if (Getpath.indexOf("app/productiondepartement") != -1 && Getpath.indexOf("/cardlocation/list") != -1) {
        $('.sticky-wrapper').find('.navbar-left').append('<ul class="nav navbar-nav navbar-left"><li><a class="sonata-action-element" href="##" onClick="history.go(-1); return false;"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a></li></ul>');

        var PathName = window.location.pathname;
        var ProdId = PathName.match(/\d+/)[0];



        $('.content-wrapper').append('<div class="row row-map-add" ><div id="map"></div></div>');

        var map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: 52.0412,
                lng: -67.6666
            },
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.HYBRID,
            labels: true
        });

        var productionDepartementId = ProdId;
        var DATA = 'productionDepartementId=' + productionDepartementId;

        $('#map').append('<div class="loader"></div>');

        $.ajax({
            type: "GET",
            url: "/admin/app/cardlocation/coordinates_polygon_cardlocation",
            data: DATA,
            dataType: "json",
            success: function(data) {
                removePolygon();
                var myLatlng = new google.maps.LatLng(data[0].positionlat, data[0].positionlang);
                map.setCenter(myLatlng);
                map.setZoom(data[0].positionzoom);

                $.ajax({
                    type: "GET",
                    url: "/admin/app/cardlocation/cardlocation_markers",
                    data: DATA,
                    dataType: "json",
                    success: function(dataa) {
                        $('.loader').hide();
                        removeMarkers();
                        var marker = [];
                        for (var i = 0; i < dataa.length; i++) {


                            marker = new google.maps.Marker({

                                position: {
                                    lat: parseFloat(dataa[i].latitude),
                                    lng: parseFloat(dataa[i].longitude),
                                },
                                map: map,
                                label: {
                                    text: dataa[i].name,
                                    color: 'white'
                                },
                                icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                                zIndex: 1
                            });

                            markers.push(marker[i])
                        }


                    },
                    error: function(msg) {
                        console.log(msg);
                        $('.loader').hide();
                    }
                });



                var PolygonCoords = changeMyStringToFloat(data[0].polygoninformation);
                // Construct the polygon.
                var SetPolygonInfo = new google.maps.Polygon({
                    paths: PolygonCoords,
                    strokeColor: '#000000',
                    strokeOpacity: 0.9,
                    strokeWeight: 3,
                    fillColor: '#ffa500',
                    fillOpacity: 0.4
                });
                SetPolygonInfo.setMap(map);
                overlays.push(SetPolygonInfo);


            },
            error: function(msg) {
                console.log(msg);
                $('.loader').hide();
            }
        });
        return false;
    }

    if (Getpath.indexOf("/cropblock") != -1 && Getpath.indexOf("edit") != -1) {

        $('.farms-departement-cordonner').parent('div').parent('div').remove();
        $('.Production-departement-cordonner').prop('disabled', false);
        $('.Production-departement-cordonner').parent('div').parent('div').css("display", "none");

        var polyUpdate = $('.polygoninformation').val();
        var lang = parseFloat($('.positionlang').val());
        var lat = parseFloat($('.positionlat').val());
        var zm = parseFloat($('.positionzoom').val());
        var myLatlngF = new google.maps.LatLng(lat, lang);

        map.setZoom(zm);
        map.setCenter(myLatlngF);
        polyUpdateCord = changeMyStringToFloat(polyUpdate);
        // Construct the polygon.
        polyUpdateE = new google.maps.Polygon({
            paths: polyUpdateCord,
            strokeColor: '#000000',
            strokeOpacity: 0.9,
            strokeWeight: 3,
            fillColor: '#ffff00',
            fillOpacity: 0.4,
        });

        polyUpdateE.setMap(map);
    }
    if (Getpath.indexOf("/cardlocation") != -1 && Getpath.indexOf("/edit") != -1) {

        $('.Production-departement-cordonner-cardLocation').prop('disabled', false);
        $('.farms-departement-cordonner-cardLocation').parent('div').parent('div').remove();
        var ProductionCardLocationTrigger = $('.Production-departement-cordonner-cardLocation').find(':selected').val();
        var name = $('.name-cardLocation').val();
        var latt = parseFloat($('.latitude').val());
        var langg = parseFloat($('.longitude').val());
        if (drawingManager != null) {

            drawingManager.setOptions({
                drawingControl: false,
                drawingMode: null,
            });

            drawingManager.setOptions({
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: ['marker']
                },
                markerOptions: {
                    icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                },

            });
        }

        if (ProductionCardLocationTrigger != "") {
            $('.Production-departement-cordonner-cardLocation').val(ProductionCardLocationTrigger).trigger('change');
        }
        var shape = {
            coords: [10, 1, 10, 20, 18, 20, 18, 1],
            type: 'poly'
        };
        marker = new google.maps.Marker({
            position: {
                lat: latt,
                lng: langg
            },
            animation: google.maps.Animation.BOUNCE,
            label: {
                text: name,
                color: 'white'
            },

            shape: shape,
            map: map,
            zIndex: 1
        });

    }

    if (Getpath.indexOf("admin/app/crop") != -1 || Getpath.indexOf("admin/app/bug") != -1) {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('.admin-preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input[type='file']").change(function() {
            readURL(this);
        });

    }
    /*###################################################################################################*/

    //$('.bootstrap-datetimepicker-widget').hide();  
      /*********************** Recording **************************************/

    if (Getpath.indexOf("admin/app/recording/create") != -1) {





        $('.tableRecording').find('.sonata-ba-collapsed-fields').remove();
        $('#search-in-map').parent().remove();
         $('.mapRecording').hide(); 
        $('.tableRecording').hide();
        var myLatlngPoly_record = "";
        var zoom_record = "";
        drawingManager.setOptions({
            drawingControl: false,
            drawingMode: null,
        });

      
          $('.Production-departement-cordonner-recording').on('change', function() {

           
              //console.log(markers);
            $('.mapRecording').hide(); 
            $('.tableRecording').hide();

            $(".sonata-bc").append(`<div class="backloadingRecord"><div class="spinner"><div class="cube1"></div><div class="cube2"></div></div></div>`);
            var productionDepartementIdd = $(this).val();
            var DATAA = 'productionDepartementId=' + productionDepartementIdd;
            var nameProd = $('.Production-departement-cordonner-recording').select2('data');
            $('.table-responsive').remove();
            var html = "";
            var rows = "";

            var DATA = 'ProductionId=' + $(this).val();
            $.ajax({
                type: "GET",
                url: "/admin/app/cropblock/coordinates_polygon_cropblock",
                data: DATA,
                dataType: "json",
                success: function(datapoly) {
                    removePolygon();
                    myLatlngPoly_record = new google.maps.LatLng(datapoly[0].positionlat, datapoly[0].positionlang);
                    zoom_record = datapoly[0].positionzoom;
                    var PolygonCoords = changeMyStringToFloat(datapoly[0].polygoninformation);
                    // Construct the polygon.
                    var SetPolygonInfo = new google.maps.Polygon({
                        paths: PolygonCoords,
                        strokeColor: '#000000',
                        strokeOpacity: 0.9,
                        strokeWeight: 3,
                        fillColor: '#ffa500',
                        fillOpacity: 0.4
                    });
                    SetPolygonInfo.setMap(map);
                    overlays.push(SetPolygonInfo);

                },
                error: function(msg) {
                    console.log(msg);

                }
            });

            $('.modalMy').prepend('<span class="closeE notPrint" id="close-map-record">&times;</span>');
           /*get markers */

            $.ajax({
                type: "GET",
                url: "/admin/app/cardlocation/cardlocation_markers",
                data: DATAA,
                dataType: "json",
                success: function(dataa) {
                    if (dataa.length <= 0) {
                        $('.tableRecording').show();
                        $('.alert-error-couting').remove();
                        $('.tableRecording .box-body').append('<div id="alert-error-couting" style="margin :5px;" class="alert alert-error alert-error-couting text-center">You need to create the countings device for this production departement !!</div>');
                        $('.sonata-ba-form-actions').hide();
                        $('.table-responsive').remove();
                        $('.hrtab').remove();
                        $('.backloadingRecord').hide();

                    } else {
                        removeMarkers();
                        for (var i = 0; i < dataa.length; i++) {
                            var marker = new google.maps.Marker({
                                position: {
                                    lat: parseFloat(dataa[i].latitude),
                                    lng: parseFloat(dataa[i].longitude),
                                },
                                map: map,
                                label: {
                                    text: dataa[i].name,
                                    color: 'white'
                                },
                                icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                                zIndex: 1
                            });

                            markers.push(marker);
                        }
                        
                        $('.table-responsive').remove();
                        $('.hrtab').remove();
                        $('.alert-error-couting').remove();
                        $('.sonata-ba-form-actions').show();    

                        html = "<thead><tr><th align='center' >Bugs&nbsp;/&nbsp;Cards <a class='btn btn-default switch-bug'><i class='fa fa-picture-o' aria-hidden='true'></i>&nbsp;switch</a></th>";
                    /*get all bugs */
                        $.ajax({
                            type: "GET",
                            url: "/admin/app/recording/all_bugs",
                            dataType: "json",
                            success: function(datau) {
                                if (datau.length <= 0) {
                                    $('.tableRecording').show();
                                    $('.alert-error-bug').remove();
                                    $('.tableRecording .box-body').append('<div id="alert-error-bug" style="margin :5px;" class="alert alert-error alert-error-bug text-center">You need to activated the bugs for your farm !!</div>');
                                    $('.sonata-ba-form-actions').hide();
                                    $('.table-responsive').remove();
                                    $('.hrtab').remove();
                                    $('.backloadingRecord').hide();
                                } else {
                                    $('.table-responsive').remove();
                                    $('.hrtab').remove();
                                    $('.table-responsive').remove();
                                    $('.hrtab').remove();
                                    $('.alert-error-bug').remove();
                                    $('.sonata-ba-form-actions').show();

                                        

                                    var ListCardLocationRecord = [];
                                    for (var t = 0; t < dataa.length; t++) {
                                        html += "<th style='color:#1c383a;     text-align: center;' align='center'><i class='fa fa-map-marker' aria-hidden='true'></i>&nbsp;" + dataa[t].name + "</th>";
                                        ListCardLocationRecord.push(dataa[t].id);
                                    }
                                    html += "</tr></thead><tbody></tbody>";
                                    rows = "";
                                    for (var i = 0; i < datau.length; i++) {
                                        var TabInd = i + 1;
                                        var color = "";
                                        if(datau[i].nameType == "Pest")
                                        {
                                            color= "#006600";
                                        }else{
                                            color = "red";
                                        }
                                        rows += "<tr><td align='left'><label class='busgname' style='color:"+color+";'><i class=' fa fa-bug' aria-hidden='true'></i>&nbsp;" + datau[i].name + "-P" + datau[i].priority + "</label><img  class='imgModal' style='width:40px !important ; height:40px !important; display:none' src='/images/bugs/"+datau[i].path+"'></td>";
                                        for (y = 0; y < dataa.length; y++) {
                                            rows += "<td><input style=' margin:auto; width: 66px;' class='amount  " + dataa[y].id + " form-control' name=" + dataa[y].id + " data-cardlatitude='"+dataa[y].latitude+"' data-cardlongitude='"+dataa[y].longitude+"' placeholder='" + datau[i].name + "' data-tabtab='" + TabInd + "' tabindex='" + TabInd + "' type='text' maxlength='4'  id=" + datau[i].id + "></td>";
                                            TabInd += datau.length;
                                        }

                                    }
                                    rows += "</tr>";

                                    $('.tableRecording .box-body').append('<hr class="hrtab notPrint" style="border:1px solid #1c383a; " align="center" width="100%" /><div class="table-responsive"><table border="0" class="table table-bordered table-striped" cellpadding="0" width="100%" id="providersFormElementsTable"></table></div>');
                                    $("#providersFormElementsTable").html(html);
                                    $(rows).appendTo("#providersFormElementsTable tbody");
                                    $('<a class="btn btn-info print notPrint" style="display:none;"><i class="fa fa-print" aria-hidden="true"></i>&nbsp;Print</a>&nbsp;<a type="button" class="btn btn-info show-map-record notPrint" data-latlang="' + myLatlngPoly_record + '" data-zoom="' + zoom_record + '"><i class="fa fa-map" aria-hidden="true"></i>&nbsp;Show Map</a>').insertBefore('#providersFormElementsTable');

                                    $('.backloadingRecord').hide();
                                    $('.tableRecording').show();
                                       /*start arrows key*/
                                      $('table#providersFormElementsTable').keydown(function(e){

                                        var $table = $(this);
                                        var $active = $('input:focus,select:focus',$table);
                                        var $next = null;
                                        var focusableQuery = 'input:visible,select:visible,textarea:visible';
                                        var position = parseInt( $active.closest('td').index()) + 1;
                                       
                                        switch(e.keyCode){
                                            case 37: // <Left>
                                                $next = $active.parent('td').prev().find(focusableQuery);   
                                                break;
                                            case 38: // <Up>                    
                                                $next = $active
                                                    .closest('tr')
                                                    .prev()                
                                                    .find('td:nth-child(' + position + ')')
                                                    .find(focusableQuery)
                                                ;
                                                
                                                break;
                                            case 39: // <Right>
                                                $next = $active.closest('td').next().find(focusableQuery);            
                                                break;
                                            case 40: // <Down>
                                                $next = $active
                                                    .closest('tr')
                                                    .next()                
                                                    .find('td:nth-child(' + position + ')')
                                                    .find(focusableQuery)
                                                ;
                                                break;
                                        }       
                                        if($next && $next.length)
                                        {        
                                            $next.focus();
                                        }
                                      });
                                      /*end arrows key*/
     
                                    var listCropBlock = [];
                                                     $.ajax({
                                                            type: "GET",
                                                            url: "/admin/app/cropblock/cropblock_recording_date",
                                                            data: DATA,
                                                            dataType: "json",
                                                            success: function(cropData) {
                                                                    console.log(cropData);
                                                                for (var t=0;t<cropData.length;t++)
                                                                    {
                                                                        listCropBlock.push(cropData[t]);
                                                                    }
                                                                     
                                                            },
                                                            error: function(msg) {
                                                                console.log(msg);
                                                                $('.loader').hide();

                                                            }
                                                        });

                                                     console.log(listCropBlock);
                                    //$('#providersFormElementsTable').append('<div class="row row-map-add" ><div id="map"></div></div>');
                                    $("#providersFormElementsTable .amount").on('keyup', function() {
                                        var listResult = [];
                                       
                                        for (var m = 0; m < ListCardLocationRecord.length; m++) {
                                            var list = [];
                                            $('#providersFormElementsTable .amount').each(function() {

                                                if ($(this).hasClass(ListCardLocationRecord[m])) {

                                                    var valamount = parseInt($(this).val());
                                                    var idbug = parseInt($(this).attr('id'));
                                                    var idcart = parseInt($(this).attr('name'));
                                                    var cardLat = parseFloat($(this).attr('data-cardlatitude'));
                                                    var cardLong = parseFloat($(this).attr('data-cardlongitude'));

                                              
                                                    var recordCard = new Object();


                                                    if (!isNaN(valamount)) {
                                                        recordCard = {
                                                            amount: valamount,
                                                            bugId: idbug,
                                                            cardId: idcart,
                                                            cardLat : cardLat,
                                                            cardLong : cardLong,
                                                            CropBlockId : 0 ,
                                                        }

                                                        list.push(recordCard);
                                                    }
                                                }
                                            });
                                            listResult.push(list);

                                        }
                                      
                                        if (listResult.length > 0) {

                                                
                                                      
                                        
                                        for(var s=0;s<listResult.length;s++)
                                        {
                                           for (var v =0;v<listResult[s].length;v++)
                                           {
                                            
                                             for (var t=0;t<listCropBlock.length;t++)
                                                 {
                                                    var PolygonCoordsDepartement = changeMyStringToFloat(listCropBlock[t].polygoninformation);
                                                    var bermudaTriangle = new google.maps.Polygon({paths: PolygonCoordsDepartement});
                                                    var resultCheck = google.maps.geometry.poly.containsLocation(new google.maps.LatLng(listResult[s][v].cardLat, listResult[s][v].cardLong), bermudaTriangle) ;
                                                     
                                                     if(resultCheck == true)
                                                     {
                                                        listResult[s][v].CropBlockId = listCropBlock[t].id;
                                                     }
                                                  }
                                           } 
                                           
                                       }
                                                       

                                            var json = JSON.stringify(listResult);
                                           
                                              console.log(listResult);
                                            $(".listRecording").val(json);
                                            
                                        }

                                    });
                                }
                            },
                            error: function(msg) {
                                console.log(msg);

                            }
                        });
                    }

                },
                error: function(msg) {
                    console.log(msg);
                    $('.loader').hide();
                }
            });
        });
          $(document).on('click','a.switch-bug',function(){
                $('.imgModal').toggle();
                $('.busgname').toggle();    
                $("i", this).toggleClass("fa-bug fa-picture-o");
            });


        $(document).on('click', 'a.show-map-record', function() {
            $('.mapRecording').toggle();
             google.maps.event.trigger(map, "resize");
             map.setCenter(myLatlngPoly_record);
             map.setZoom(zoom_record);
            $('.mapRecording ').removeClass('modalMy');
            $('.mapRecording').attr('style', '');
            $('a.print').show();
            $(this).hide();
            $
        });

        $(document).on('click', 'span#close-map-record', function() {

            $('.modalMy').hide();

        });
        $(document).on('click', 'a.print', function() {
            
                google.maps.event.trigger(map, "resize");
                map.setCenter(myLatlngPoly_record);
                map.setZoom(zoom_record);
                window.print();
        });

        $('.sonata-ba-form-actions').addClass('notPrint');
    } /*endif getpath recording*/

    /*********************** Recording **************************************/


    /** start conf bug**/

    if(Getpath.indexOf("/admin/app/bug/list") != -1)
    {
        $.ajax({
            type:'GET',
            url : '/admin/app/bug/check_list_of_bug',
            dataType:'json',
            success:function(data){
                    if(data.check == true)
                    {
                        $('.btnCheckConfBug').remove();

                    }else{
                        for(var i =0;i<data.length;i++)
                        {
                             $('.sonata-ba-list-field-actions').each(function(){
                                    var idBugTable = $(this).attr('objectid');
                                    if(idBugTable == data[i].bug_id)
                                    {
                                        $(this).find('a.btnCheckConfBug').css("background-color", "#009900");
                                        $(this).find('a.btnCheckConfBug').attr('href', $(this).find('a.btnCheckConfBug').attr('data-remove'));
                                        $(this).find('a.btnCheckConfBug i').toggleClass('fa-check-square-o fa-square-o');
                                        $(this).find('a.btnCheckConfBug').attr('title', 'Remove Bug to your farm');

                                    }   
                             });
                        }
                    }
            }
        });
    }

      /** end conf bug **/

      /** start conf crop **/

        if(Getpath.indexOf("/admin/app/crop/list") != -1)
        {
           $.ajax({
            type:'GET',
            url : '/admin/app/crop/check_list_of_crop',
            dataType:'json',
            success:function(data){
                    if(data.check == true)
                    {
                        $('.btnCheckConfCrop').remove();

                    }else{
                        for(var i =0;i<data.length;i++)
                        {
                             $('.sonata-ba-list-field-actions').each(function(){
                                    var idCropTable = $(this).attr('objectid');
                                    if(idCropTable == data[i].crop_id)
                                    {
                                        $(this).find('a.btnCheckConfCrop').css("background-color", "#009900");
                                        $(this).find('a.btnCheckConfCrop').attr('href', $(this).find('a.btnCheckConfCrop').attr('data-remove'));
                                        $(this).find('a.btnCheckConfCrop i').toggleClass('fa-check-square-o fa-square-o');
                                        $(this).find('a.btnCheckConfCrop').attr('title', 'Remove Crop to your farm');
                                    }   
                             });
                        }
                    }
            }
        });
        }
/**/

      /** end conf crop**/
        /*     $('.get-crops-config').attr("disabled", 'disabled');

            $('.get-bugs-config').on('change', function() {

                $('.get-crops-config').attr("disabled", 'disabled');
                $('.get-crops-config').find('option').remove().end();


                    var bugId = $(this).val();
                    alert(bugId);
                    var DATA = 'bugId=' + bugId;

                    $.ajax({
                        type: "GET",
                        url: "/admin/app/bugscropsconfig/config_crop",
                        data: DATA,
                        dataType: "json",
                        success: function(data) {

                            console.log(data);
                            var mySelect = $('.get-crops-config');
                            if(data.length == 0)
                            {
                                $('.get-crops-config').prop('disabled', false);
                                 $('.get-crops-config').find('option').remove().end();
                                  mySelect.append(
                                $("<option style='display:none;' selected></option>").val("").html("All the crops are configured for this bug ")
                                );
                            }
                            else{
                                 console.log(data);
                                $('.get-crops-config').prop('disabled', false);

                             
                                mySelect.append(
                                    $("<option style='display:none;'></option>").val("").html("Choose a Crop")
                                );
                                for (var i = 0; i < data.length; i++) {
                                    var val = data[i].id;
                                    var name = data[i].name

                                    mySelect.append(
                                        $("<option style='display:none;'></option>").val(data[i].id).html(data[i].name)
                                    );
                                }

                            }
                           

                        },
                        error: function(msg) {
                            console.log(msg);
                            $('.loader').hide();
                        }
                    });
                return false;

            });
        if(Getpath.indexOf("admin/app/bugscropsconfig/") != -1 && Getpath.indexOf("edit") != -1){
             $('.get-crops-config').removeAttr("disabled")
        }*/



});

$(window).load(function() {
    $('li.picker-switch').hide();
    $('.datepicker tbody').on('click', function() {
        $('.bootstrap-datetimepicker-widget').css("display", "none");
    });
});