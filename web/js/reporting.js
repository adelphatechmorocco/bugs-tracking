$(document).ready(function() { //




    /*#########################################   generale Reporting  ###############################################################################*/
    $('.select2-bug').select2();
    $('.select2-crop').select2();
    $('.select2-date').select2();
    $('.select2-allFarms').select2();
    $(function() {
        $('#datetimepicker1').datetimepicker({
            format: "YYYY-MM-DD",
        });
    });

    $('.alert').fadeIn(2000);
    $(".alert button.close").click(function(e) {
        $(this).parent().fadeOut('slow');
    });

    /*#########################################   map   ###############################################################################*/

    /*################################## map my functionn################################## */
    /*########################################################################################*/
    var overlays = [];
    var markers = [];

    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.split(search).join(replacement);
    };

    function changeMyStringToFloat(MyString) {

        var niveau1 = MyString.replaceAll("}", " "),
            niveau2 = niveau1.replaceAll("{lat:", " "),
            niveau3 = niveau2.replaceAll("lng :", ","),
            niveau4 = niveau3.replaceAll("}", " "),
            niveau5 = niveau4.replaceAll("},", " "),
            niveau6 = niveau5.replaceAll(",,", ",");

        var temp = new Array();
        temp = niveau6.split(",");
        for (a in temp) {
            temp[a] = parseFloat(temp[a]);
        }

        var PolygonArray = [];

        for (var i = 1; i < temp.length - 1; i += 2) {
            var b = new Object();
            b = {
                lat: temp[i - 1],
                lng: temp[i]
            };

            PolygonArray.push(b);

        }

        return PolygonArray;
    }


    function removePolygon() {
        for (var i = 0; i < overlays.length; i++) {
            overlays[i].setMap(null);
        }
        overlays = [];
    }

    function removeMarkers() {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        markers = [];
    }

    function setMarkers(latt, lngg) {
        var shape = {
            coords: [0, 0, 200, 0, 0, 200],
            type: 'poly'
        };
        var lat1 = parseFloat(latt);
        var lang1 = parseFloat(langg);

        var marker = new google.maps.Marker({
            position: {
                lat: latt,
                lng: lngg
            },
            map: map,
            shape: shape,
            icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
            zIndex: 1
        });

        markers.push(marker);

    }

    function pinSymbol(color) {
        return {
            path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
            fillColor: color,
            fillOpacity: 1,
            strokeColor: '#000',
            strokeWeight: 2,
            scale: 1,
        };
    }

    /*
        function setMarkerslatlangname(lat,lang,detColor,name,amount) {
           
            marker = new google.maps.Marker({
                position: {
                    lat: parseFloat(lat),
                    lng: parseFloat(lang),
                },

                label: {
                    text: name,
                    color: 'white'
                },
                map: map,
                 
                icon: pinSymbol(detColor),
                zIndex: 1
            });
            
                  var html =  '<div id="content">'+
                      '<div id="siteNotice">'+
                      '</div>'+
                      '<h1 id="firstHeading" style="color:'+detColor+';" class="firstHeading">'+name+'</h1>'+
                      '</div>';

                    marker.infoWindow = new google.maps.InfoWindow({
                         content: html,
                         maxWidth: 200
                     });

                  google.maps.event.addListener(marker, 'click', function () {
                       infowindow.setContent('<h3>' + this.title + '</h3>');
                      infowindow.open(map, this);
                   });
                    google.maps.event.addListener(marker, 'click', function () {
                        marker.infoWindow.open(map, marker);

                    });
            


            markers.push(marker);
        }*/

    function ChoiceColor(key) {
        var color;
        switch (key) {
            case 4:
                color = '#ff0000';
                break;
            case 3:
                color = '#ff9900';
                break;
            case 2:
                color = '#ffff00';
                break;
            case 1:
                color = '#66ff33';

        }
        return color;
    }

    function getHighestWindowPosition(coors) {
        var lat = -5000,
            lng = 0,
            i = 0,
            n = coors.length;

        for (; i !== n; ++i) {
            if (coors[i].lat > lat) {
                lat = coors[i].lat;
                lng = coors[i].lng;
            }
        }
        return {
            lat: lat,
            lng: lng
        };

    }

    function attachPolygonInfoWindow(polygon, coors, color, bug, crop, cropbBlock, date, amount) {
        var html = '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" style="color:' + color + ';" class="firstHeading">' + cropbBlock + '</h1>' +
            '<div id="bodyContent">' +
            '<p>bug :   <span style="color:' + color + ';">' + bug + '</span></p>' +
            '<p>Crop:   <span style="color:' + color + ';">' + crop + '</span></p>' +
            '<p>Scouting date : <span style="color:' + color + ';">' + date + '</span></p>' +
            '<p>AVG amount: <span style="color:' + color + ';">' + amount + '</span></p>' +
            '</div></div>';

        polygon.infoWindow = new google.maps.InfoWindow({
            content: html,
            maxWidth: 200
        });

        polygon.infoWindow.setPosition(coors);

        google.maps.event.addListener(polygon, 'click', function() {
            polygon.infoWindow.open(map, polygon);

        });
        /*google.maps.event.addListener(polygon, 'mouseout', function () {
            polygon.infoWindow.close();
        });*/
    }


/*
    function FuncGraphChart(data, key) {

        var color = [];
        color.push(ChoiceColor(key));
        $('#containerGraph').html('');

        new Morris.Line({

            element: 'containerGraph',
            data: data,
            xkey: 'date',
            ykeys: ['amount'],
            labels: ['Value'],
            fillOpacity: 0.4,
            hideHover: 'auto',
            behaveLikeLine: true,
            resize: true,
            pointFillColors: ['#ffffff'],
            pointStrokeColors: ['black'],
            lineColors: color,
            colors: ['black'],
            xLabelAngle: 45,
            labels: ['AVG AMOUNT', 'Date']

        });

    }*/


    function FuncGraphChart(dataObjects,dataCategories) {

        $('#containerGraph').html('');

        $('#containerGraph').highcharts({
                title: {
                    text: 'AVG Bug-Crop/Date',
                    x: -20 //center
                },
               xAxis: {

                    categories: dataCategories
                },
                yAxis: {
                    title: {
                        text: 'Number bug'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
               
                series: dataObjects,
            });
             $('.highcharts-button').hide();
             $('.highcharts-credits').hide();
    }


    function listcheckDenger(list, amount) {
        if (list[4].max < amount) {
            return 4;
        } else {
            for (var key in list) {
                if (list[key].min >= amount || list[key].max >= amount) {
                    return key;

                }
            }

        }
    }
          function groupBy( array , f )
                {
                      var groups = {};
                      array.forEach( function( o )
                      {
                        var group = JSON.stringify( f(o) );
                        groups[group] = groups[group] || [];
                        groups[group].push( o );  
                      });
                      return Object.keys(groups).map( function( group )
                      {
                        return groups[group]; 
                      })
                }
        function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
        }


    /*##################################  end map my function################################## */
    /*########################################################################################*/

    var map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: 52.0412,
            lng: -67.6666
        },
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.HYBRID,
        labels: true
    });


    /* farm init */
    if (typeof FarmPolygoninformation !== 'undefined') {
        var PolygonCoordsFarm = changeMyStringToFloat(FarmPolygoninformation);

        // Construct the polygon.
        var SetPolygonInfoFarm = new google.maps.Polygon({
            paths: PolygonCoordsFarm,
            strokeColor: '#000000',
            strokeOpacity: 0.9,
            strokeWeight: 3,
            fillColor: '#00ccff',
            fillOpacity: 0.3
        });
        SetPolygonInfoFarm.setMap(map);
        var myLatlng = new google.maps.LatLng(FarmPositionlat, FarmPositionlang);
        map.setCenter(myLatlng);
        map.setZoom(parseInt(FarmPositionzoom));
    }
    /* end farm init */

    /** debut crop block **/

    if (typeof CropBlockPolygoninformation !== 'undefined') {
        var colorCropOnClickTable = ChoiceColor(parseInt(ReportingKeyIntervalDanger));
        var PolygonCoordsCropBlock = changeMyStringToFloat(CropBlockPolygoninformation);

        // Construct the polygon.
        var SetPolygonInfoCropBlock = new google.maps.Polygon({
            paths: PolygonCoordsCropBlock,
            strokeColor: colorCropOnClickTable,
            strokeOpacity: 0.9,
            strokeWeight: 3,
            fillColor: colorCropOnClickTable,
            fillOpacity: 0.4
        });
        SetPolygonInfoCropBlock.setMap(map);
        overlays.push(SetPolygonInfoCropBlock);

        var cordsCropBlock = new google.maps.LatLng(CropBlockCenterOfPolyLat, CropBlockCenterOfPolyLang);
        attachPolygonInfoWindow(SetPolygonInfoCropBlock, cordsCropBlock, colorCropOnClickTable, $('select[name="bug-value"]  option:selected').text(), $('select[name="crop-value"]  option:selected').text(), CropBlockName, $('select[name="date-value"] option:selected').val(), ReportingMoyenneamount);
    }
    /** end crop block **/

    /* start card amount */
    if (typeof cardamountpoistion !== 'undefined') {

        removeMarkers();
        var lent = ReportingsBugCrop.length;
        var arrayIntervalDangers = ReportingsBugCrop[lent - 1].listChekDanger;
        var infowindow = new google.maps.InfoWindow();

        for (var i = 0; i < cardamountpoistion.length; i++) {

            var keyDanger = listcheckDenger(arrayIntervalDangers, cardamountpoistion[i].amount);
            var colorPin = ChoiceColor(parseInt(keyDanger));
            var amount = cardamountpoistion[i].amount;
            var marker = new google.maps.Marker({
                position: {

                    lat: parseFloat(cardamountpoistion[i].latitude),
                    lng: parseFloat(cardamountpoistion[i].longitude),
                },
                label: {
                    text: amount.toString(),
                    color: 'white'
                },
                map: map,


                icon: pinSymbol(colorPin),
                zIndex: 1
            });


            markers.push(marker);


        }
    }




    /* end card amount */


    /*start graph init */

    if (typeof ReportingsBugCrop !== 'undefined') {

            var dataObjects = []; 
            var dataCategories = [];
            var dataData = [];
            var color = ChoiceColor(ReportingsBugCrop[lent - 1].keyIntervalDanger);
            var name  =ReportingsBugCrop[lent - 1].cropBlockName; 
            if (ReportingsBugCrop.length) {
                for (var m = 0; m < ReportingsBugCrop.length; m++) {
                    dataCategories.push(ReportingsBugCrop[m].dateScouting.date.substr(0, 10));    
                    dataData.push(ReportingsBugCrop[m].moyenneamount);
                    }
                    var object = { name:name,data:dataData,color:color};
                    dataObjects.push(object);
                }
        
               
        var lent = ReportingsBugCrop.length;
        var arrayIntervalDangers = ReportingsBugCrop[lent - 1].listChekDanger;
        $('#1intervaldanger').html('<span style="color:black; font-weight: bold; float:left;">' + arrayIntervalDangers[1].min + ' </span><span style="color:black; font-weight: bold; float:right;">' + arrayIntervalDangers[1].max + '</span>');
        $('#2intervaldanger').html('<span style="color:black ; font-weight: bold; float:left;">' + arrayIntervalDangers[2].min + ' </span><span style="color:black; font-weight: bold; float:right;">' + arrayIntervalDangers[2].max + '</span>');
        $('#3intervaldanger').html('<span style="color:black ; font-weight: bold; float:left;">' + arrayIntervalDangers[3].min + ' </span><span style="color:black;  font-weight: bold; float:right;">' + arrayIntervalDangers[3].max + '</span>');
        $('#4intervaldanger').html('<span style="color:black ; font-weight: bold; float:left;">' + arrayIntervalDangers[4].min + ' </span><span style="color:black; font-weight: bold; float:right;">' + arrayIntervalDangers[4].max + '</span>');


        FuncGraphChart(dataObjects,dataCategories);

    }

    /*end graph init*/


    /* start farm change global*/
    $(document).on('change', '.select2-allFarms', function() {

        $('.backloadingRecord').show();
        FarmId = parseInt($(this).val());
        DataIdFarm = "farmId=" + FarmId;
        $.ajax({
            type: "GET",
            url: "/reporting/details/datesRecord/",
            data: DataIdFarm,
            dataType: "json",
            success: function(data) {
                if (data.length) {
                    html = '<option selected="selected" value="0">Date Recording</option>';
                    for (var j = 0; j < data.length; j++) {
                        var jsonDate = data[j].dateScouting.date;
                        html += '<option value="' + jsonDate.substr(0, 10) + '">' + jsonDate.substr(0, 10) + '</option>'
                    }

                    $('select[name="date-value"]')
                        .empty()
                        .append(html);
                    $('.backloadingRecord').hide();

                } else {
                    $('select[name="date-value"]')
                        .empty()
                        .append('<option selected="selected" value="0">Recording Date, No record exist for this Farm</option>');
                    $('.backloadingRecord').hide();
                    swal({
                        title: '',
                        text: 'No record exist for this Farm,try with other!',
                        type: 'info',
                        timer: 2500,
                        allowOutsideClick: true,
                    });
                }

            },
            error: function(err) {
                $('.backloadingRecord').hide();
                console.log(err);
            }
        });

        removePolygon();
        for (var i = 0; i < allFarms.length; i++) {

            if (allFarms[i].id == FarmId) {
                if (typeof allFarms[i].polygoninformation !== 'undefined') {
                    var PolygonCoordsFarm = changeMyStringToFloat(allFarms[i].polygoninformation);

                    // Construct the polygon.
                    var SetPolygonInfoFarm = new google.maps.Polygon({
                        paths: PolygonCoordsFarm,
                        strokeColor: '#000000',
                        strokeOpacity: 0.9,
                        strokeWeight: 3,
                        fillColor: '#00ccff',
                        fillOpacity: 0.3
                    });
                    SetPolygonInfoFarm.setMap(map);
                    var myLatlng = new google.maps.LatLng(allFarms[i].positionlat, allFarms[i].positionlang);
                    map.setCenter(myLatlng);
                    map.setZoom(parseInt(allFarms[i].positionzoom));
                }

            }
        }

    });
    /* end farm change global */

    /** start filter go**/

    $(document).on('click', 'a.btn-go', function(e) {

        var bug = $('select[name="bug-value"]  option:selected').val(),
            crop = $('select[name="crop-value"] option:selected').val(),
            date = $('select[name="date-value"] option:selected').val();



        if (bug == 0 || crop == 0 || date == "") {

            swal({
                title: '',
                text: 'You have to fill all the fields to filter!',
                type: 'info',
                timer: 2500,
                allowOutsideClick: true,
            });
            return false;
        }
        $('.backloadingRecord').show();
        $.ajax({
            type: "GET",
            url: "/reporting/details/filter",
            data: {
                date: date,
                cropId: crop,
                bugId: bug,
                farmId: FarmId
            },
            dataType: "json",
            success: function(data) {
                /*debut map*/
                if (data.cropblocks.length) {
                    removePolygon();
                    var SetPolygonInfoCropBlock = [];
                    for (var i = 0; i < data.cropblocks.length; i++) {
                        for (var j = 0; j < data.Reportings.length; j++) {
                            if (data.Reportings[j].cropBlockId == data.cropblocks[i].id) {

                                var color = ChoiceColor(parseInt(data.Reportings[j].keyIntervalDanger));
                                SetPolygonInfoCropBlock[i] = new google.maps.Polygon({
                                    paths: changeMyStringToFloat(data.cropblocks[i].polygoninformation),
                                    strokeColor: color,
                                    strokeOpacity: 0.9,
                                    strokeWeight: 3,
                                    fillColor: color,
                                    fillOpacity: 0.4
                                });
                                SetPolygonInfoCropBlock[i].setMap(map);
                                overlays.push(SetPolygonInfoCropBlock[i]);
                                var cords = new google.maps.LatLng(data.cropblocks[i].centerOfPolyLat, data.cropblocks[i].centerOfPolyLang);
                                attachPolygonInfoWindow(SetPolygonInfoCropBlock[i], cords, color, data.Reportings[j].bugName, data.Reportings[j].cropName, data.cropblocks[i].name, date, data.Reportings[j].moyenneamount);

                            }
                        }

                    }
                /*end map*/
               /*debut chart*/
             
                  

                var dataChart = [];
                var dataCategories = [];
                var dataObjects = []; 
                if (data.ReportingsBugCrop.length) {
                     var result1 = groupBy(data.ReportingsBugCrop, function(item)
                        {
                          return [item.cropBlockId, item.prodDeptId];
                        });
                   
                     for(var i = 0;i<result1.length;i++)
                        {
                            for(var j=0;j<result1[i].length;j++)
                            {
                                dataCategories.push(result1[i][j].dateScouting.date.substr(0, 10)); 
                              
                            }
                        }

                  var unique = dataCategories.filter( onlyUnique );
                  var ObjectCategoriesUniqueSort = unique.sort();

                   for(var i = 0;i<result1.length;i++)
                     {
                            var color = ChoiceColor(result1[i][0].keyIntervalDanger);
                            var name  =  result1[i][0].cropBlockName;
                            var object = { name:name,color:color};
                            var dataData = [];
                            
                             for( var k =0;k<ObjectCategoriesUniqueSort.length;k++)
                                {
                                     var check = null;
                                     for(var j=0;j<result1[i].length;j++)
                                         {
                                           var dateObject = result1[i][j].dateScouting.date.substr(0, 10);
                                            if(dateObject == ObjectCategoriesUniqueSort[k])
                                            {
                                                 check = result1[i][j].moyenneamount;
                                            }
                                          }                                     
                                      dataData.push(check);
                                 }
                             object.data = dataData;
                             dataObjects.push(object);

                        }
                         FuncGraphChart(dataObjects,ObjectCategoriesUniqueSort);
                     }



                var lent = data.ReportingsBugCrop.length;
                var arrayIntervalDangers = data.ReportingsBugCrop[lent - 1].listChekDanger;

                $('#1intervaldanger').html('<span style="color:black; font-weight: bold; float:left;">' + arrayIntervalDangers[1].min + ' </span><span style="color:black; font-weight: bold; float:right;">' + arrayIntervalDangers[1].max + '</span>');
                $('#2intervaldanger').html('<span style="color:black; font-weight: bold; float:left;">' + arrayIntervalDangers[2].min + ' </span><span style="color:black; font-weight: bold; float:right;">' + arrayIntervalDangers[2].max + '</span>');
                $('#3intervaldanger').html('<span style="color:black; font-weight: bold; float:left;">' + arrayIntervalDangers[3].min + ' </span><span style="color:black; font-weight: bold; float:right;">' + arrayIntervalDangers[3].max + '</span>');
                $('#4intervaldanger').html('<span style="color:black; font-weight: bold; float:left;">' + arrayIntervalDangers[4].min + ' </span><span style="color:black; font-weight: bold; float:right;">' + arrayIntervalDangers[4].max + '</span>');

                //FuncGraphChart(dataChart, data.ReportingsBugCrop[0].keyIntervalDanger);

                /*end chart*/
                 
                /*start marker*/
                        removeMarkers();
                        var infowindow = new google.maps.InfoWindow();

                        for (var i = 0; i < data.amoutpositioncard.length; i++) {

                            var keyDanger = listcheckDenger(arrayIntervalDangers, data.amoutpositioncard[i].amount);
                            var colorPin = ChoiceColor(parseInt(keyDanger));
                            var amount = data.amoutpositioncard[i].amount;
                            var marker = new google.maps.Marker({
                                position: {

                                    lat: parseFloat(data.amoutpositioncard[i].latitude),
                                    lng: parseFloat(data.amoutpositioncard[i].longitude),
                                },
                                label: {
                                    text: amount.toString(),
                                    color: 'white'
                                },
                                map: map,


                                icon: pinSymbol(colorPin),
                                zIndex: 1
                            });


                            markers.push(marker);


                        }
                /* end marker*/

                    $('.backloadingRecord').hide();

                } else {
                    $('#containerGraph').html('');
                     $('#1intervaldanger').html('');
                     $('#2intervaldanger').html('');
                     $('#3intervaldanger').html('');
                     $('#4intervaldanger').html('');
                     removeMarkers();
                     removePolygon();
                    $('.backloadingRecord').hide();
                    swal({
                        title: '',
                        text: 'No record exist for this filter,try with other!',
                        type: 'info',
                        timer: 2500,
                        allowOutsideClick: true,
                    });
                }
               


            },
            error: function(msg) {
                console.log(msg);
            }
        });
        return false;

    });

    /** end filter go**/

    /*    $(function () {
            alert(1);
            $('#containerGraph').highcharts({
                title: {
                    text: 'AVG Bug/Crop',
                    x: -20 //center
                },
               xAxis: {

                    categories: ["2017-12-22","2018-01-05", "2018-02-05"]
                },
                yAxis: {
                    title: {
                        text: 'Number bug'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    valueSuffix: '°C'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
               
                series: [{
                    name: 'Tokyo',
                    data: [null,50, 30],
                    color: '#000000'
                }, {
                    name: 'New York',
                    data: [90,100,null]
                }, {
                    name: 'Berlin',
                    data: [10,15]
                }, {
                    name: 'London',
                    data: [33,80, 30]
                }]
            });
             $('.highcharts-button').hide();
             $('.highcharts-credits').hide();
    });*/
    



    /*######################################### end map  ###############################################################################*/
    /*#########################################  end map ###############################################################################*/



});