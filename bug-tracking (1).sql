-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Ven 17 Novembre 2017 à 18:16
-- Version du serveur :  10.1.21-MariaDB
-- Version de PHP :  7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bug-tracking`
--

-- --------------------------------------------------------

--
-- Structure de la table `acl_classes`
--

CREATE TABLE `acl_classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `acl_entries`
--

CREATE TABLE `acl_entries` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `object_identity_id` int(10) UNSIGNED DEFAULT NULL,
  `security_identity_id` int(10) UNSIGNED NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) UNSIGNED NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `acl_object_identities`
--

CREATE TABLE `acl_object_identities` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_object_identity_id` int(10) UNSIGNED DEFAULT NULL,
  `class_id` int(10) UNSIGNED NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `acl_object_identity_ancestors`
--

CREATE TABLE `acl_object_identity_ancestors` (
  `object_identity_id` int(10) UNSIGNED NOT NULL,
  `ancestor_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `acl_security_identities`
--

CREATE TABLE `acl_security_identities` (
  `id` int(10) UNSIGNED NOT NULL,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `bt_bug`
--

CREATE TABLE `bt_bug` (
  `id` bigint(20) NOT NULL,
  `bugtype_id` bigint(20) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `priority` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `nameType` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `bt_bug`
--

INSERT INTO `bt_bug` (`id`, `bugtype_id`, `name`, `description`, `path`, `updated_at`, `created_at`, `priority`, `enabled`, `nameType`) VALUES
(2, 2, 'abaaa', 'Donec rutrum congue leo eget malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Praesent sapien massa, convallis a pellentesque nec, egestas', '597715279c7f2.JPG', '2017-11-17 14:24:14', '0000-00-00 00:00:00', 8, 0, 'Pest'),
(3, 1, 'anaaa', 'qsdsqd', '597715530ad90.JPG', '2017-11-17 14:24:14', '0000-00-00 00:00:00', 6, 0, 'Beneficial'),
(5, 1, 'ahaaa', 'Donec rutrum congue leo eget malesuada. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ultricies ligula sed magna dictum porta. Quisque velit nisi, pretium ut lacinia in, elementum id enim.', '59884a87e5487.JPG', '2017-11-17 14:24:14', '2017-08-07 13:09:59', 10, 0, 'Beneficial'),
(6, 2, 'alaa', 'Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Donec rutrum congue leo eget malesuada. Proin eget tortor risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla porttitor accumsan tincidunt. Vivamus suscipit tortor eget felis porttitor volutpat', '59ca19cc55468.jpg', '2017-11-17 17:17:34', '2017-08-17 12:41:36', 1, 1, 'Pest'),
(7, 2, 'ayaaa', 'Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Cras ultricies ligula sed magna dictum porta. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Curabitur aliquet quam id dui posuere blandit. Nulla porttitor accumsan tincidunt. Proin eget tortor risus.', '5996f6b17b762.JPG', '2017-11-17 17:17:34', '2017-08-18 16:16:17', 2, 1, 'Pest'),
(8, 1, 'name', NULL, NULL, '2017-09-14 23:11:02', '2017-09-07 21:17:45', 1, 0, 'Beneficial'),
(9, 1, 'namememememe', 'aaaaaaaaaaaaaaaaa', '59bba56244c60.jpg', '2017-11-17 14:24:14', '2017-09-15 12:03:14', 6, 0, 'Beneficial'),
(10, 1, 'BUGBUG11', 'descript', '59e5df1282fe3.jpg', '2017-11-17 14:24:14', '2017-10-17 12:44:34', 2, 0, 'Beneficial'),
(11, 2, 'namename1111', 'dessss', '59e5df3671bd9.jpg', '2017-10-17 12:45:10', '2017-10-17 12:45:10', 321, 0, 'Pest'),
(12, 1, '321papapa', 'desss', '59e87cc88d5f4.jpg', '2017-11-17 14:24:14', '2017-10-19 12:22:00', 9, 0, 'Beneficial'),
(13, 1, 'nameP', NULL, NULL, '2017-11-01 12:52:32', '2017-11-01 12:52:32', 4, 0, 'Beneficial');

-- --------------------------------------------------------

--
-- Structure de la table `bt_bugs_crops_config`
--

CREATE TABLE `bt_bugs_crops_config` (
  `id` bigint(20) NOT NULL,
  `bug_id` bigint(20) NOT NULL,
  `crop_id` bigint(20) NOT NULL,
  `max` int(11) NOT NULL,
  `min` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `bt_bugs_crops_config`
--

INSERT INTO `bt_bugs_crops_config` (`id`, `bug_id`, `crop_id`, `max`, `min`) VALUES
(1, 6, 1, 100, 50),
(2, 6, 5, 60, 10),
(5, 7, 1, 120, 60),
(6, 7, 5, 200, 100);

-- --------------------------------------------------------

--
-- Structure de la table `bt_bug_type`
--

CREATE TABLE `bt_bug_type` (
  `id` bigint(20) NOT NULL,
  `BugType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `create_at` datetime DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `bt_bug_type`
--

INSERT INTO `bt_bug_type` (`id`, `BugType`, `description`, `updated_at`, `create_at`, `enabled`) VALUES
(1, 'Beneficial', 'Sed porttitor lectus nibh. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Proin eget tortor risus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(2, 'Pest', 'Curabitur aliquet quam id dui posuere blandit. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Sed porttitor lectus nibh.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Structure de la table `bt_card_location`
--

CREATE TABLE `bt_card_location` (
  `id` bigint(20) NOT NULL,
  `ProductionDepartement_id` bigint(20) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `positionlat` double NOT NULL,
  `positionlang` double NOT NULL,
  `positionzoom` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `priority` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `bt_card_location`
--

INSERT INTO `bt_card_location` (`id`, `ProductionDepartement_id`, `name`, `latitude`, `longitude`, `updated_at`, `created_at`, `positionlat`, `positionlang`, `positionzoom`, `start_date`, `end_date`, `priority`) VALUES
(16, 18, 'aaa-36363-C1', '33.984897501325335', '-6.843532919883728', '2017-10-17 16:51:09', '2017-10-17 16:51:09', 33.984235766954, -6.8428807952621, 17, '2017-10-19', '2017-10-19', 8),
(17, 19, 'skhiratp-proddepttt-C17', '33.96073201717031', '-6.915653035975993', '2017-10-20 15:23:14', '2017-10-20 15:23:14', 33.960466089818, -6.9155927575373, 19, '2017-10-20', '2017-11-04', 6),
(18, 19, 'skhiratp-proddepttt-C18', '33.96044058365998', '-6.915669129230082', '2017-10-20 15:23:58', '2017-10-20 15:23:58', 33.960459415759, -6.9155042446398, 19, '2017-10-20', '2017-11-04', 4),
(19, 19, 'skhiratp-proddepttt-C19', '33.960638580581076', '-6.915663764812052', '2017-10-20 15:24:39', '2017-10-20 15:24:39', 33.960470539191, -6.9154452360415, 19, '2017-10-20', '2017-12-09', 7),
(20, 19, 'skhiratp-proddepttt-C20', '33.960605210345754', '-6.915697711519897', '2017-10-23 13:36:49', '2017-10-23 13:36:49', 33.960486111994, -6.9155095252388, 19, '2017-10-23', NULL, 2),
(21, 17, 'skhiratp-321-C21', '33.959922937834456', '-6.9151511616655625', '2017-10-31 11:12:03', '2017-10-31 11:12:03', 33.960078993499, -6.9149785735824, 17, '2017-10-31', '2017-11-04', 3),
(22, 17, 'a', '33.96026109165854', '-6.915387196058873', '2017-11-03 17:25:25', '2017-11-03 17:25:25', 33.959731940129, -6.9144099452712, 17, '2017-11-03', NULL, 4),
(23, 19, 'cardSouf', '33.960647479308285', '-6.915614604949951', '2017-11-17 17:13:56', '2017-11-17 17:13:56', 33.96069078285, -6.9155005985119, 19, '2017-11-17', '2017-11-17', 8);

-- --------------------------------------------------------

--
-- Structure de la table `bt_crop`
--

CREATE TABLE `bt_crop` (
  `id` bigint(20) NOT NULL,
  `croptype_id` bigint(20) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `enabled` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `bt_crop`
--

INSERT INTO `bt_crop` (`id`, `croptype_id`, `name`, `description`, `path`, `updated_at`, `created_at`, `enabled`) VALUES
(1, 2, 'African Violet', 'Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Nulla quis lorem ut libero malesuada feugiat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur arcu erat, accumsan id', '59bba63178775.jpg', '2017-11-17 17:17:34', '0000-00-00 00:00:00', 1),
(2, 3, 'Crop1', 'Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Nulla porttitor accumsan tincidunt. Nulla quis lorem ut libero malesuada feugiat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Curabitur arcu erat, accumsan id', '597715b8ad6fd.jpg', '2017-08-17 18:03:41', '0000-00-00 00:00:00', 0),
(4, 2, 'aaaa', 'Cras ultricies ligula sed magna dictum porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Quisque velit nisi, pretium ut lacinia in, elementum id enim', '597715efaa24b.jpg', '2017-08-18 14:10:51', '0000-00-00 00:00:00', 0),
(5, 2, 'Cyclamen', 'done', '59bb9aeee5cad.jpg', '2017-11-17 17:17:34', '2017-09-15 11:18:38', 1),
(6, 2, 'test', 'des', '59e87c9da240f.jpg', '2017-10-31 16:24:21', '2017-10-19 12:21:17', 1);

-- --------------------------------------------------------

--
-- Structure de la table `bt_crop_block`
--

CREATE TABLE `bt_crop_block` (
  `id` bigint(20) NOT NULL,
  `productiondepartement_id` bigint(20) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `area` double NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `crop_id` bigint(20) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `polygoninformation` longtext COLLATE utf8_unicode_ci,
  `positionlat` double NOT NULL,
  `positionlang` double NOT NULL,
  `positionzoom` int(11) NOT NULL,
  `centerOfPolyLat` double NOT NULL,
  `centerOfPolyLang` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `bt_crop_block`
--

INSERT INTO `bt_crop_block` (`id`, `productiondepartement_id`, `name`, `area`, `start_date`, `end_date`, `crop_id`, `updated_at`, `created_at`, `polygoninformation`, `positionlat`, `positionlang`, `positionzoom`, `centerOfPolyLat`, `centerOfPolyLang`) VALUES
(14, 17, 'aaaaaaa', 1034.7894, '2017-10-21', '2017-10-17', 1, '2017-10-17 12:36:44', '2017-10-17 12:36:44', '{lat:33.96020699119424,lng : -6.91527396440506},{lat:33.95988663476967,lng : -6.915338337421417},{lat:33.95954847945716,lng : -6.915209591388702},{lat:33.96002011675985,lng : -6.915070116519928},{lat:33.96029597887588,lng : -6.915284693241119},', 33.960078993499, -6.9149785735824, 17, 33.95992222916652, -6.915204226970673),
(15, 17, 'cropBlock121', 496.049, '2017-10-23', NULL, 5, '2017-10-23 11:30:52', '2017-10-23 11:30:52', '{lat:33.96031377640102,lng : -6.915474962443113},{lat:33.95992222998751,lng : -6.915528606623411},{lat:33.95977095020876,lng : -6.915517877787352},{lat:33.960251485046676,lng : -6.915324758738279},', 33.960078993499, -6.9149785735824, 17, 33.96004236330489, -6.915426682680845),
(16, 17, 'cropBlock', 269.8595, '2017-10-23', '2017-10-23', 1, '2017-10-23 12:32:45', '2017-10-23 12:32:45', '{lat:33.960126902201196,lng : -6.9149707071483135},{lat:33.95970865845695,lng : -6.915077995508909},{lat:33.96005571192186,lng : -6.914863418787718},', 33.960078993499, -6.9149785735824, 17, 33.95991778032907, -6.9149707071483135),
(17, 17, 'papa', 35.3269, '2017-10-01', NULL, 1, '2017-10-23 13:30:03', '2017-10-23 13:30:03', '{lat:33.960175845483676,lng : -6.915072714909911},{lat:33.960077958890544,lng : -6.915056621655822},{lat:33.96011355402837,lng : -6.914992248639464},', 33.959941062201, -6.9151984309026, 18, 33.96012690218711, -6.915032481774688),
(18, 17, 'crop resr', 302.3664, '2017-10-31', '2017-10-31', 5, '2017-10-31 13:18:24', '2017-10-31 13:18:24', '{lat:33.96008480994141,lng : -6.915348159473069},{lat:33.95981339654774,lng : -6.915455447833665},{lat:33.95960427416199,lng : -6.91532133738292},', 33.95997941298, -6.9152306630856, 19, 33.9598445420517, -6.915388392608293),
(19, 17, 'aaaa', 648.6432, '2017-10-31', '2017-10-31', 1, '2017-10-31 13:39:37', '2017-10-31 13:39:37', '{lat:33.9602877879558,lng : -6.915430111403111},{lat:33.96001192581323,lng : -6.915011686796788},{lat:33.95985174738445,lng : -6.9152262635179795},', 33.960390450446, -6.9148283698776, 17, 33.96006976767012, -6.91522089909995),
(20, 20, 'aaaa', 253.1779, '2017-10-31', '2017-10-31', 5, '2017-10-31 13:44:00', '2017-10-31 13:44:00', '{lat:33.96003862218868,lng : -6.914636177534703},{lat:33.95982505095032,lng : -6.914732737059239},{lat:33.95978945569179,lng : -6.914518160338048},', 33.960096791069, -6.9154819487774, 17, 33.959914038940234, -6.914625448698644),
(21, 17, 'cropBB1', 216.3457, '2017-10-31', '2017-10-31', 1, '2017-10-31 13:47:23', '2017-10-31 13:47:23', '{lat:33.96040739030772,lng : -6.915429006694467},{lat:33.96030282990918,lng : -6.915249298690469},{lat:33.960271684233724,lng : -6.915528248428018},{lat:33.96042296312205,lng : -6.915506790755899},{lat:33.960342874332326,lng : -6.9154129134403775},', 33.960059502112, -6.9152344881475, 19, 33.96034732367789, -6.915388773559243),
(22, 20, 'papa1', 82.7977, '2017-10-31', '2017-10-31', 1, '2017-10-31 13:49:24', '2017-10-31 13:49:24', '{lat:33.96010483220667,lng : -6.914568017600686},{lat:33.95998247384551,lng : -6.914573382018716},{lat:33.96001806902325,lng : -6.9145116912113735},{lat:33.96005143948889,lng : -6.91447682249418},{lat:33.96006478767146,lng : -6.914458047031076},{lat:33.96009815811876,lng : -6.914509009002359},', 33.959954941285, -6.9144530852243, 19, 33.96004365302609, -6.914515714524896),
(23, 20, 'D44', 93.378, '2017-10-01', '2017-10-31', 1, '2017-10-31 14:24:37', '2017-10-31 14:24:37', '{lat:33.95978670006763,lng : -6.914731632350595},{lat:33.95968436361312,lng : -6.91485501396528},{lat:33.95971995891561,lng : -6.914683352588327},{lat:33.95976445302277,lng : -6.914632390617044},{lat:33.9597889247718,lng : -6.914696763633401},{lat:33.95974220597211,lng : -6.914731632350595},', 33.959770291002, -6.9144772251054, 19, 33.95973664419246, -6.914743702291162),
(24, 20, 'D33', 93.378, '2017-10-31', NULL, 6, '2017-10-31 14:24:37', '2017-10-31 14:24:37', '{lat:33.95978670006763,lng : -6.914731632350595},{lat:33.95968436361312,lng : -6.91485501396528},{lat:33.95971995891561,lng : -6.914683352588327},{lat:33.95976445302277,lng : -6.914632390617044},{lat:33.9597889247718,lng : -6.914696763633401},{lat:33.95974220597211,lng : -6.914731632350595},', 33.959770291002, -6.9144772251054, 19, 33.95973664419246, -6.914743702291162),
(25, 17, 'qqq1', 1650.5619, '2017-10-25', NULL, 6, '2017-10-31 16:23:17', '2017-10-31 16:23:17', '{lat:33.96030558548267,lng : -6.915355009550694},{lat:33.95977165805695,lng : -6.915419382567052},{lat:33.95954918730722,lng : -6.915183348173741},{lat:33.95994073543766,lng : -6.915033144468907},', 33.960078993499, -6.9149785735824, 17, 33.959927386394945, -6.9152262635179795),
(26, 17, 'qqqq', 1650.5619, '2017-10-31', NULL, 1, '2017-10-31 16:23:17', '2017-10-31 16:23:17', '{lat:33.96030558548267,lng : -6.915355009550694},{lat:33.95977165805695,lng : -6.915419382567052},{lat:33.95954918730722,lng : -6.915183348173741},{lat:33.95994073543766,lng : -6.915033144468907},', 33.960078993499, -6.9149785735824, 17, 33.959927386394945, -6.9152262635179795),
(28, 17, 'mama11', 1334.0857, '2017-10-31', NULL, 1, '2017-10-31 16:24:21', '2017-10-31 16:24:21', '{lat:33.960207699038826,lng : -6.915344280714635},{lat:33.95956698498861,lng : -6.915161890501622},{lat:33.96016320516347,lng : -6.914925856108312},{lat:33.960198800265616,lng : -6.915269178862218},', 33.960078993499, -6.9149785735824, 17, 33.959887342013715, -6.915135068411473),
(29, 19, 'cropb block test 1', 233.7316, '2017-11-15', '2017-11-15', 1, '2017-11-15 17:39:19', '2017-11-15 17:39:19', '{lat:33.96038274177584,lng : -6.915670889429748},{lat:33.96070977037267,lng : -6.915644067339599},{lat:33.960767612034445,lng : -6.915751355700195},{lat:33.96058073883156,lng : -6.9157352624461055},', 33.960490561365, -6.9157205396511, 19, 33.96057517690514, -6.915697711519897),
(30, 19, 'crop block test 2', 200.7348, '2017-11-15', '2017-11-15', 5, '2017-11-15 17:40:06', '2017-11-15 17:40:06', '{lat:33.9609300134131,lng : -6.915692347101867},{lat:33.96076983671296,lng : -6.915732580237091},{lat:33.96070977037267,lng : -6.9156360207125545},{lat:33.96079208349491,lng : -6.9155609188601375},', 33.960543953807, -6.9155730181553, 19, 33.96081989189288, -6.915646749548614),
(31, 19, 'cropSouf', 49.3099, '2017-11-17', NULL, 1, '2017-11-17 17:14:04', '2017-11-17 17:14:04', '{lat:33.96071199505268,lng : -6.915602954104543},{lat:33.960625232488525,lng : -6.915635140612721},{lat:33.96049397613352,lng : -6.915643187239766},{lat:33.960691972930334,lng : -6.915578814223409},', 33.961342611755, -6.9167009289556, 19, 33.9606029855931, -6.915611000731587);

-- --------------------------------------------------------

--
-- Structure de la table `bt_crop_type`
--

CREATE TABLE `bt_crop_type` (
  `id` bigint(20) NOT NULL,
  `crop_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `enabled` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `bt_crop_type`
--

INSERT INTO `bt_crop_type` (`id`, `crop_type`, `updated_at`, `created_at`, `enabled`) VALUES
(1, 'CropType1', '2017-09-11 12:36:36', '0000-00-00 00:00:00', 0),
(2, 'CropType2', '2017-09-11 12:30:02', '0000-00-00 00:00:00', 1),
(3, 'CropType3', '2017-09-11 12:30:09', '0000-00-00 00:00:00', 1),
(4, 'CropType4', '2017-09-11 12:30:24', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Structure de la table `bt_farms`
--

CREATE TABLE `bt_farms` (
  `id` bigint(20) NOT NULL,
  `owner_id` bigint(20) DEFAULT NULL,
  `farm_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `postal_code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone1` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `polygoninformation` longtext COLLATE utf8_unicode_ci NOT NULL,
  `positionlat` double NOT NULL,
  `positionlang` double NOT NULL,
  `positionzoom` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `bt_farms`
--

INSERT INTO `bt_farms` (`id`, `owner_id`, `farm_name`, `address1`, `address2`, `city`, `postal_code`, `country`, `phone1`, `phone2`, `email`, `updated_at`, `created_at`, `polygoninformation`, `positionlat`, `positionlang`, `positionzoom`) VALUES
(5, 18, 'skhiratp', 'adress 11111', 'adress2', 'rabat', '1100', 'morroco', '060606', '060606', 'nb@adelphatech.com', '2017-10-24 12:57:59', '2017-09-12 17:42:56', '{lat:33.961194749242466,lng : -6.916097402572632},{lat:33.96082990301087,lng : -6.9163978099823},{lat:33.96053624515131,lng : -6.915979385375977},{lat:33.96029597887588,lng : -6.915678977966309},{lat:33.95984214072637,lng : -6.91614031791687},{lat:33.95913023286924,lng : -6.915121078491211},{lat:33.96002011675985,lng : -6.914241313934326},{lat:33.96126593856862,lng : -6.915990114212036},', 33.960096791069, -6.9154819487774, 17),
(6, 17, 'aaa', 'aaa', 'aaaa', 'rabat', '101010', 'morroco', '03030303030', '030303', 'iraouf@adelphatech.com', '2017-10-12 15:37:34', '2017-09-13 12:32:31', '{lat:33.98584938824883,lng : -6.842958927154541},{lat:33.984586134129884,lng : -6.846424341201782},{lat:33.983767677776264,lng : -6.843913793563843},{lat:33.98422138825015,lng : -6.842604875564575},{lat:33.9848797089449,lng : -6.842604875564575},{lat:33.985315621193465,lng : -6.8432700634002686},{lat:33.98532451733852,lng : -6.844182014465332},', 33.984298040741, -6.8427627780654, 17);

-- --------------------------------------------------------

--
-- Structure de la table `bt_production_departement`
--

CREATE TABLE `bt_production_departement` (
  `id` bigint(20) NOT NULL,
  `farm_id` bigint(20) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `polygoninformation` longtext COLLATE utf8_unicode_ci,
  `positionlat` double NOT NULL,
  `positionlang` double NOT NULL,
  `positionzoom` int(11) NOT NULL,
  `centerOfPolyLat` double NOT NULL,
  `centerOfPolyLang` double NOT NULL,
  `area` double NOT NULL,
  `grower_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `bt_production_departement`
--

INSERT INTO `bt_production_departement` (`id`, `farm_id`, `name`, `updated_at`, `created_at`, `polygoninformation`, `positionlat`, `positionlang`, `positionzoom`, `centerOfPolyLat`, `centerOfPolyLang`, `area`, `grower_id`) VALUES
(17, 5, '321', '2017-10-19 11:54:05', '2017-10-17 11:23:31', '{lat:33.96048285270429,lng : -6.915523409843445},{lat:33.95988663476967,lng : -6.9156306982040405},{lat:33.959450592142176,lng : -6.915158629417419},{lat:33.96008240828357,lng : -6.914826035499573},', 33.960078993499, -6.9149785735824, 17, 33.959966722423, -6.9152283668518, 4635.6956, 31),
(18, 6, '36363', '2017-10-19 10:28:56', '2017-10-17 16:49:11', '{lat:33.985102113432696,lng : -6.844026446342468},{lat:33.984568341684266,lng : -6.844680905342102},{lat:33.98417690693822,lng : -6.844273209571838},{lat:33.98411463306242,lng : -6.843608021736145},{lat:33.98426586953881,lng : -6.843103766441345},{lat:33.98474626597288,lng : -6.8429213762283325},{lat:33.98501315170742,lng : -6.842942833900452},{lat:33.98502204788412,lng : -6.843790411949158},{lat:33.98468399251421,lng : -6.843822598457336},{lat:33.9845505492349,lng : -6.843629479408264},{lat:33.984826331781235,lng : -6.8432968854904175},{lat:33.98475516217752,lng : -6.84323251247406},{lat:33.98442600198519,lng : -6.843253970146179},{lat:33.98436372829188,lng : -6.843575835227966},{lat:33.98436372829188,lng : -6.843683123588562},{lat:33.98430145455295,lng : -6.843768954277039},{lat:33.98426586953881,lng : -6.843940615653992},{lat:33.98435483204625,lng : -6.844037175178528},{lat:33.98441710574607,lng : -6.844037175178528},{lat:33.98451496432503,lng : -6.844165921211243},{lat:33.984595030351294,lng : -6.844251751899719},{lat:33.98481743558402,lng : -6.844101548194885},', 33.984298040741, -6.8427627780654, 17, 33.984608373248, -6.8438011407852, 8577.0589, 31),
(19, 5, 'proddepttt  232', '2017-11-02 17:41:10', '2017-10-19 15:58:40', '{lat:33.96098118090655,lng : -6.915710996836424},{lat:33.96059853629714,lng : -6.915775369852781},{lat:33.960322675162196,lng : -6.915668081492186},{lat:33.960625232488525,lng : -6.915592979639769},{lat:33.96081210559368,lng : -6.91555006429553},', 33.960096791069, -6.9154819487774, 17, 33.960651928034, -6.9156627170742, 814.9622, 34),
(20, 5, 'pdt1', '2017-10-30 12:09:01', '2017-10-30 12:09:01', '{lat:33.96016320516347,lng : -6.9146039910265245},{lat:33.95963817567694,lng : -6.914904398436192},{lat:33.959753860418374,lng : -6.914593262190465},{lat:33.960074217342964,lng : -6.914410871977452},', 33.960096791069, -6.9154819487774, 17, 33.959900690420206, -6.914657635206822, 1062.2675, 34);

-- --------------------------------------------------------

--
-- Structure de la table `bt_recording`
--

CREATE TABLE `bt_recording` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `cardlocation_id` bigint(20) DEFAULT NULL,
  `scouting_date` date NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `bt_recording`
--

INSERT INTO `bt_recording` (`id`, `user_id`, `cardlocation_id`, `scouting_date`, `updated_at`, `created_at`) VALUES
(1, NULL, 19, '2017-10-20', '2017-10-20 15:27:47', '2017-10-20 15:27:47'),
(2, NULL, 17, '2017-10-20', '2017-10-20 15:27:47', '2017-10-20 15:27:47'),
(3, NULL, 18, '2017-10-20', '2017-10-20 15:27:47', '2017-10-20 15:27:47'),
(4, 36, 19, '2017-10-20', '2017-10-20 16:03:46', '2017-10-20 16:03:46'),
(5, 36, 17, '2017-10-20', '2017-10-20 16:03:46', '2017-10-20 16:03:46'),
(6, 36, 18, '2017-10-20', '2017-10-20 16:03:46', '2017-10-20 16:03:46'),
(7, 36, 16, '2017-10-23', '2017-10-23 16:43:33', '2017-10-23 16:43:33'),
(8, 36, 20, '2017-10-24', '2017-10-24 12:11:30', '2017-10-24 12:11:30'),
(9, 36, 17, '2017-10-24', '2017-10-24 12:11:30', '2017-10-24 12:11:30'),
(10, 36, 18, '2017-10-24', '2017-10-24 12:11:30', '2017-10-24 12:11:30'),
(11, 36, 19, '2017-10-24', '2017-10-24 12:11:30', '2017-10-24 12:11:30'),
(12, 36, 20, '2017-11-15', '2017-11-15 18:59:26', '2017-11-15 18:59:26'),
(13, 36, 17, '2017-11-15', '2017-11-15 18:59:26', '2017-11-15 18:59:26'),
(14, 36, 18, '2017-11-15', '2017-11-15 18:59:26', '2017-11-15 18:59:26'),
(15, 36, 19, '2017-11-15', '2017-11-15 18:59:26', '2017-11-15 18:59:26'),
(16, 36, 20, '2017-11-17', '2017-11-17 15:52:15', '2017-11-17 15:52:15'),
(17, 36, 17, '2017-11-17', '2017-11-17 15:52:15', '2017-11-17 15:52:15'),
(18, 36, 18, '2017-11-17', '2017-11-17 15:52:15', '2017-11-17 15:52:15'),
(19, 36, 19, '2017-11-17', '2017-11-17 15:52:15', '2017-11-17 15:52:15'),
(20, 33, 20, '2017-11-17', '2017-11-17 16:05:00', '2017-11-17 16:05:00'),
(21, 33, 17, '2017-11-17', '2017-11-17 16:05:00', '2017-11-17 16:05:00'),
(22, 33, 18, '2017-11-17', '2017-11-17 16:05:00', '2017-11-17 16:05:00'),
(23, 33, 19, '2017-11-17', '2017-11-17 16:05:00', '2017-11-17 16:05:00'),
(24, 36, 20, '2017-11-17', '2017-11-17 16:34:02', '2017-11-17 16:34:02'),
(25, 36, 17, '2017-11-17', '2017-11-17 16:34:02', '2017-11-17 16:34:02'),
(26, 36, 18, '2017-11-17', '2017-11-17 16:34:02', '2017-11-17 16:34:02'),
(27, 36, 19, '2017-11-17', '2017-11-17 16:34:02', '2017-11-17 16:34:02'),
(28, 33, 23, '2017-11-18', '2017-11-17 17:17:34', '2017-11-17 17:17:34'),
(29, 33, 17, '2017-11-18', '2017-11-17 17:17:34', '2017-11-17 17:17:34'),
(30, 33, 18, '2017-11-18', '2017-11-17 17:17:34', '2017-11-17 17:17:34'),
(31, 33, 19, '2017-11-18', '2017-11-17 17:17:34', '2017-11-17 17:17:34'),
(32, 33, 20, '2017-11-18', '2017-11-17 17:17:34', '2017-11-17 17:17:34');

-- --------------------------------------------------------

--
-- Structure de la table `bt_recording_bugs`
--

CREATE TABLE `bt_recording_bugs` (
  `id` bigint(20) NOT NULL,
  `recording_id` bigint(20) NOT NULL,
  `bugs_id` bigint(20) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `bt_recording_bugs`
--

INSERT INTO `bt_recording_bugs` (`id`, `recording_id`, `bugs_id`, `amount`) VALUES
(1, 1, 6, 1),
(2, 1, 8, 1),
(3, 1, 7, 1),
(4, 1, 10, 1),
(5, 1, 3, 1),
(6, 1, 9, 1),
(7, 1, 1, 1),
(8, 1, 2, 1),
(9, 1, 12, 1),
(10, 1, 5, 1),
(11, 2, 6, 3216),
(12, 2, 8, 3216),
(13, 2, 7, 321),
(14, 2, 10, 3216),
(15, 2, 3, 3216),
(16, 2, 9, 3216),
(17, 2, 1, 3216),
(18, 2, 2, 3216),
(19, 2, 12, 321),
(20, 2, 5, 321),
(21, 3, 6, 1),
(22, 3, 8, 1),
(23, 3, 7, 1),
(24, 3, 10, 1),
(25, 3, 3, 1),
(26, 3, 9, 1),
(27, 3, 1, 1),
(28, 3, 2, 1),
(29, 3, 12, 1),
(30, 3, 5, 1),
(31, 4, 6, 44),
(32, 4, 8, 77),
(33, 5, 6, 22),
(34, 5, 8, 55),
(35, 6, 6, 33),
(36, 6, 8, 66),
(37, 7, 6, 3216),
(38, 7, 8, 3216),
(39, 7, 7, 3216),
(40, 7, 10, 3214),
(41, 8, 6, 3216),
(42, 8, 8, 3216),
(43, 8, 7, 3213),
(44, 8, 10, 2211),
(45, 9, 6, 321),
(46, 9, 8, 321),
(47, 9, 7, 321),
(48, 9, 10, 321),
(49, 10, 6, 321),
(50, 10, 8, 321),
(51, 10, 7, 321),
(52, 10, 10, 321),
(53, 11, 6, 321),
(54, 11, 8, 321),
(55, 11, 7, 321),
(56, 11, 10, 3216),
(57, 12, 6, 4),
(58, 13, 6, 1),
(59, 14, 6, 2),
(60, 15, 6, 3),
(61, 16, 6, 87),
(62, 16, 7, 66),
(63, 17, 6, 50),
(64, 17, 7, 33),
(65, 18, 6, 37),
(66, 18, 7, 90),
(67, 19, 6, 49),
(68, 19, 7, 54),
(69, 20, 6, 80),
(70, 20, 7, 100),
(71, 21, 6, 45),
(72, 21, 7, 180),
(73, 22, 6, 30),
(74, 22, 7, 100),
(75, 23, 6, 47),
(76, 23, 7, 100),
(77, 24, 6, 80),
(78, 24, 7, 100),
(79, 25, 6, 30),
(80, 25, 7, 120),
(81, 26, 6, 30),
(82, 26, 7, 100),
(83, 27, 6, 47),
(84, 27, 7, 100),
(85, 28, 6, 70),
(86, 28, 7, 100),
(87, 29, 6, 45),
(88, 29, 7, 180),
(89, 30, 6, 30),
(90, 30, 7, 100),
(91, 31, 6, 47),
(92, 31, 7, 100),
(93, 32, 6, 80),
(94, 32, 7, 100);

-- --------------------------------------------------------

--
-- Structure de la table `bt_reporting`
--

CREATE TABLE `bt_reporting` (
  `id` bigint(20) NOT NULL,
  `moyenneamount` double NOT NULL,
  `cropBlockId` int(11) NOT NULL,
  `bugId` int(11) NOT NULL,
  `bugName` varchar(110) COLLATE utf8_unicode_ci NOT NULL,
  `cropId` int(11) NOT NULL,
  `cropName` varchar(110) COLLATE utf8_unicode_ci NOT NULL,
  `keyIntervalDanger` int(11) NOT NULL,
  `dangerMin` double NOT NULL,
  `dangerMax` double NOT NULL,
  `listChekDanger` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `prodDeptId` int(11) NOT NULL,
  `prodDeptName` varchar(110) COLLATE utf8_unicode_ci NOT NULL,
  `cropBlockName` varchar(110) COLLATE utf8_unicode_ci NOT NULL,
  `dateScouting` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `bt_reporting`
--

INSERT INTO `bt_reporting` (`id`, `moyenneamount`, `cropBlockId`, `bugId`, `bugName`, `cropId`, `cropName`, `keyIntervalDanger`, `dangerMin`, `dangerMax`, `listChekDanger`, `prodDeptId`, `prodDeptName`, `cropBlockName`, `dateScouting`) VALUES
(9, 45, 30, 6, 'alaa', 5, 'Cyclamen', 3, 35, 47.5, 'a:4:{i:1;a:2:{s:3:\"min\";i:10;s:3:\"max\";d:22.5;}i:2;a:2:{s:3:\"min\";d:22.5;s:3:\"max\";d:35;}i:3;a:2:{s:3:\"min\";d:35;s:3:\"max\";d:47.5;}i:4;a:2:{s:3:\"min\";d:47.5;s:3:\"max\";d:60;}}', 19, 'proddepttt  232', 'crop block test 2', '2017-11-16 00:00:00'),
(10, 180, 30, 7, 'ayaaa', 5, 'Cyclamen', 4, 175, 200, 'a:4:{i:1;a:2:{s:3:\"min\";i:100;s:3:\"max\";d:125;}i:2;a:2:{s:3:\"min\";d:125;s:3:\"max\";d:150;}i:3;a:2:{s:3:\"min\";d:150;s:3:\"max\";d:175;}i:4;a:2:{s:3:\"min\";d:175;s:3:\"max\";d:200;}}', 19, 'proddepttt  232', 'crop block test 2', '2017-11-16 00:00:00'),
(11, 52.333333333333, 29, 6, 'alaa', 1, 'African Violet', 1, 50, 62.5, 'a:4:{i:1;a:2:{s:3:\"min\";i:50;s:3:\"max\";d:62.5;}i:2;a:2:{s:3:\"min\";d:62.5;s:3:\"max\";d:75;}i:3;a:2:{s:3:\"min\";d:75;s:3:\"max\";d:87.5;}i:4;a:2:{s:3:\"min\";d:87.5;s:3:\"max\";d:100;}}', 19, 'proddepttt  232', 'cropb block test 1', '2017-11-16 00:00:00'),
(12, 100, 29, 7, 'ayaaa', 1, 'African Violet', 3, 90, 105, 'a:4:{i:1;a:2:{s:3:\"min\";i:60;s:3:\"max\";d:75;}i:2;a:2:{s:3:\"min\";d:75;s:3:\"max\";d:90;}i:3;a:2:{s:3:\"min\";d:90;s:3:\"max\";d:105;}i:4;a:2:{s:3:\"min\";d:105;s:3:\"max\";d:120;}}', 19, 'proddepttt  232', 'cropb block test 1', '2017-11-16 00:00:00'),
(13, 30, 30, 6, 'alaa', 5, 'Cyclamen', 2, 22.5, 35, 'a:4:{i:1;a:2:{s:3:\"min\";i:10;s:3:\"max\";d:22.5;}i:2;a:2:{s:3:\"min\";d:22.5;s:3:\"max\";d:35;}i:3;a:2:{s:3:\"min\";d:35;s:3:\"max\";d:47.5;}i:4;a:2:{s:3:\"min\";d:47.5;s:3:\"max\";d:60;}}', 19, 'proddepttt  232', 'crop block test 2', '2017-11-17 00:00:00'),
(14, 120, 30, 7, 'ayaaa', 5, 'Cyclamen', 1, 100, 125, 'a:4:{i:1;a:2:{s:3:\"min\";i:100;s:3:\"max\";d:125;}i:2;a:2:{s:3:\"min\";d:125;s:3:\"max\";d:150;}i:3;a:2:{s:3:\"min\";d:150;s:3:\"max\";d:175;}i:4;a:2:{s:3:\"min\";d:175;s:3:\"max\";d:200;}}', 19, 'proddepttt  232', 'crop block test 2', '2017-11-17 00:00:00'),
(15, 52.333333333333, 29, 6, 'alaa', 1, 'African Violet', 1, 50, 62.5, 'a:4:{i:1;a:2:{s:3:\"min\";i:50;s:3:\"max\";d:62.5;}i:2;a:2:{s:3:\"min\";d:62.5;s:3:\"max\";d:75;}i:3;a:2:{s:3:\"min\";d:75;s:3:\"max\";d:87.5;}i:4;a:2:{s:3:\"min\";d:87.5;s:3:\"max\";d:100;}}', 19, 'proddepttt  232', 'cropb block test 1', '2017-11-17 00:00:00'),
(16, 100, 29, 7, 'ayaaa', 1, 'African Violet', 3, 90, 105, 'a:4:{i:1;a:2:{s:3:\"min\";i:60;s:3:\"max\";d:75;}i:2;a:2:{s:3:\"min\";d:75;s:3:\"max\";d:90;}i:3;a:2:{s:3:\"min\";d:90;s:3:\"max\";d:105;}i:4;a:2:{s:3:\"min\";d:105;s:3:\"max\";d:120;}}', 19, 'proddepttt  232', 'cropb block test 1', '2017-11-17 00:00:00'),
(17, 45, 30, 6, 'alaa', 5, 'Cyclamen', 3, 35, 47.5, 'a:4:{i:1;a:2:{s:3:\"min\";i:10;s:3:\"max\";d:22.5;}i:2;a:2:{s:3:\"min\";d:22.5;s:3:\"max\";d:35;}i:3;a:2:{s:3:\"min\";d:35;s:3:\"max\";d:47.5;}i:4;a:2:{s:3:\"min\";d:47.5;s:3:\"max\";d:60;}}', 19, 'proddepttt  232', 'crop block test 2', '2017-11-18 00:00:00'),
(18, 180, 30, 7, 'ayaaa', 5, 'Cyclamen', 4, 175, 200, 'a:4:{i:1;a:2:{s:3:\"min\";i:100;s:3:\"max\";d:125;}i:2;a:2:{s:3:\"min\";d:125;s:3:\"max\";d:150;}i:3;a:2:{s:3:\"min\";d:150;s:3:\"max\";d:175;}i:4;a:2:{s:3:\"min\";d:175;s:3:\"max\";d:200;}}', 19, 'proddepttt  232', 'crop block test 2', '2017-11-18 00:00:00'),
(19, 52.333333333333, 29, 6, 'alaa', 1, 'African Violet', 1, 50, 62.5, 'a:4:{i:1;a:2:{s:3:\"min\";i:50;s:3:\"max\";d:62.5;}i:2;a:2:{s:3:\"min\";d:62.5;s:3:\"max\";d:75;}i:3;a:2:{s:3:\"min\";d:75;s:3:\"max\";d:87.5;}i:4;a:2:{s:3:\"min\";d:87.5;s:3:\"max\";d:100;}}', 19, 'proddepttt  232', 'cropb block test 1', '2017-11-18 00:00:00'),
(20, 100, 29, 7, 'ayaaa', 1, 'African Violet', 3, 90, 105, 'a:4:{i:1;a:2:{s:3:\"min\";i:60;s:3:\"max\";d:75;}i:2;a:2:{s:3:\"min\";d:75;s:3:\"max\";d:90;}i:3;a:2:{s:3:\"min\";d:90;s:3:\"max\";d:105;}i:4;a:2:{s:3:\"min\";d:105;s:3:\"max\";d:120;}}', 19, 'proddepttt  232', 'cropb block test 1', '2017-11-18 00:00:00'),
(21, 70, 31, 6, 'alaa', 1, 'African Violet', 2, 62.5, 75, 'a:4:{i:1;a:2:{s:3:\"min\";i:50;s:3:\"max\";d:62.5;}i:2;a:2:{s:3:\"min\";d:62.5;s:3:\"max\";d:75;}i:3;a:2:{s:3:\"min\";d:75;s:3:\"max\";d:87.5;}i:4;a:2:{s:3:\"min\";d:87.5;s:3:\"max\";d:100;}}', 19, 'proddepttt  232', 'cropSouf', '2017-11-18 00:00:00'),
(22, 100, 31, 7, 'ayaaa', 1, 'African Violet', 3, 90, 105, 'a:4:{i:1;a:2:{s:3:\"min\";i:60;s:3:\"max\";d:75;}i:2;a:2:{s:3:\"min\";d:75;s:3:\"max\";d:90;}i:3;a:2:{s:3:\"min\";d:90;s:3:\"max\";d:105;}i:4;a:2:{s:3:\"min\";d:105;s:3:\"max\";d:120;}}', 19, 'proddepttt  232', 'cropSouf', '2017-11-18 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `bt_user`
--

CREATE TABLE `bt_user` (
  `id` bigint(20) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `firstname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manager_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `bt_user`
--

INSERT INTO `bt_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `firstname`, `lastname`, `phone`, `manager_id`, `created_at`, `updated_at`) VALUES
(2, 'iraouf+1@adelphatech.com', 'iraouf+1@adelphatech.com', 'iraouf+1@adelphatech.com', 'iraouf+1@adelphatech.com', 1, '75v66alxfuccs08w8w8g4s4w4sgsc0w', '$2y$13$75v66alxfuccs08w8w8g4eiIgK980TKdLvezTXbtSK.RtvFPkeyeW', '2017-11-17 17:38:18', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:17:\"ROLE_GLOBAL_ADMIN\";}', 0, NULL, 'issam', 'raouf', '06666666', 0, '0000-00-00 00:00:00', '2017-11-17 17:38:18'),
(17, 'iraouf+owner@adelphatech.com', 'iraouf+owner@adelphatech.com', 'iraouf+owner@adelphatech.com', 'iraouf+owner@adelphatech.com', 0, '56cyi80e7v0oo484oggosc80og0gs8s', '$2y$13$56cyi80e7v0oo484oggosOmkqlq/9IFnK0GuUpSR43/.YHg209xyC', '2017-08-17 17:23:57', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:15:\"ROLE_OWNER_FARM\";}', 0, NULL, 'Issam', 'Raouf', '+212624713701', 2, '2017-08-17 17:21:42', '2017-10-24 17:25:09'),
(18, 'nboudass+owner@adelphatech.com', 'nboudass+owner@adelphatech.com', 'nboudass+owner@adelphatech.com', 'nboudass+owner@adelphatech.com', 1, '7ljo8qpnjuo0kwws8k0k8ssg4k840kk', '$2y$13$7ljo8qpnjuo0kwws8k0k8eHiGxFwuSZ4k0hUYGDA8GbIPYIm3vB8y', '2017-10-19 10:59:19', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:15:\"ROLE_OWNER_FARM\";}', 0, NULL, 'Noureddine', 'Boudass', '+2126767676', 2, '2017-08-17 17:22:50', '2017-10-19 10:59:19'),
(31, 'aaa@gmail.com', 'aaa@gmail.com', 'aaa@gmail.com', 'aaa@gmail.com', 1, '51yhddq4o00040ko0ggcwo0swk0ggo0', '$2y$13$51yhddq4o00040ko0ggcwevMkcMAvtGx4Me98iEqccDu6NdhaJaGa', '2017-10-17 12:34:37', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_GROWER\";}', 0, NULL, 'aaa', 'aaa', 'aaa', 2, '2017-10-17 11:23:17', '2017-10-17 12:34:37'),
(33, 'test@adelphatech.com', 'test@adelphatech.com', 'test@adelphatech.com', 'test@adelphatech.com', 1, 'h022f2acxr4goo08os80kscg0wooscw', '$2y$13$h022f2acxr4goo08os80keWftmZtIh7xdKzcKRCasYdZzsoueqjEq', NULL, 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:13:\"ROLE_OPERATOR\";}', 0, NULL, 'test', 'test', 'test', 2, '2017-10-19 16:06:05', '2017-10-19 16:06:05'),
(34, 'grower@adelphatech.com', 'grower@adelphatech.com', 'grower@adelphatech.com', 'grower@adelphatech.com', 1, 'oamqxv1tou800sgw4gwsgsww08wsgow', '$2y$13$oamqxv1tou800sgw4gwsge/3Yk.FrMCYHBM7Wy.g0JXsvn0vJyBoq', '2017-10-19 17:11:31', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_GROWER\";}', 0, NULL, 'grower', 'issam', '06060606', 18, '2017-10-19 16:43:59', '2017-10-19 17:11:31'),
(36, 'testOperator@adelphatech.com', 'testoperator@adelphatech.com', 'testOperator@adelphatech.com', 'testoperator@adelphatech.com', 1, 'n7wxif9fmz48wg80c8s8c8o8owks40w', '$2y$13$n7wxif9fmz48wg80c8s8cuqRvlagKZSlkt59vZLJHFp60drJt0LNO', '2017-11-01 15:28:36', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:13:\"ROLE_OPERATOR\";}', 0, NULL, 'testOperator', 'testOperator', '06060606', 34, '2017-10-19 17:14:05', '2017-11-01 15:28:36'),
(37, 'iraouf@adelphatech.com', 'iraouf@adelphatech.com', 'iraouf@adelphatech.com', 'iraouf@adelphatech.com', 1, 'tkm0qw1bus08go84g84wgkskogwcg00', '$2y$13$tkm0qw1bus08go84g84wgevHcE3HmFwfiMWBA7udMCEbNIq/aB4qC', NULL, 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:13:\"ROLE_OPERATOR\";}', 0, NULL, 'issam', 'mamam', '060606', 2, '2017-11-02 15:47:45', '2017-11-02 15:47:45');

-- --------------------------------------------------------

--
-- Structure de la table `growers_productiondepartement`
--

CREATE TABLE `growers_productiondepartement` (
  `grower_id` bigint(20) NOT NULL,
  `productionDepartement_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `growers_productiondepartement`
--

INSERT INTO `growers_productiondepartement` (`grower_id`, `productionDepartement_id`) VALUES
(2, 17),
(2, 18);

-- --------------------------------------------------------

--
-- Structure de la table `operator_productiondepartement`
--

CREATE TABLE `operator_productiondepartement` (
  `Operator_id` bigint(20) NOT NULL,
  `productionDepartement_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `operator_productiondepartement`
--

INSERT INTO `operator_productiondepartement` (`Operator_id`, `productionDepartement_id`) VALUES
(33, 17),
(36, 17),
(36, 19),
(37, 17),
(37, 18);

-- --------------------------------------------------------

--
-- Structure de la table `recording`
--

CREATE TABLE `recording` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sylius_email`
--

CREATE TABLE `sylius_email` (
  `id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sender_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sylius_settings`
--

CREATE TABLE `sylius_settings` (
  `id` int(11) NOT NULL,
  `schema_alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `namespace` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `acl_classes`
--
ALTER TABLE `acl_classes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`);

--
-- Index pour la table `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`),
  ADD KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`),
  ADD KEY `IDX_46C8B806EA000B10` (`class_id`),
  ADD KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_46C8B806DF9183C9` (`security_identity_id`);

--
-- Index pour la table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`),
  ADD KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`);

--
-- Index pour la table `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD PRIMARY KEY (`object_identity_id`,`ancestor_id`),
  ADD KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`),
  ADD KEY `IDX_825DE299C671CEA1` (`ancestor_id`);

--
-- Index pour la table `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`);

--
-- Index pour la table `bt_bug`
--
ALTER TABLE `bt_bug`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_A10F300FEA2C0EE` (`bugtype_id`);

--
-- Index pour la table `bt_bugs_crops_config`
--
ALTER TABLE `bt_bugs_crops_config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C102E62DFA3DB3D5` (`bug_id`),
  ADD KEY `IDX_C102E62D888579EE` (`crop_id`);

--
-- Index pour la table `bt_bug_type`
--
ALTER TABLE `bt_bug_type`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `bt_card_location`
--
ALTER TABLE `bt_card_location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_473E22DFCAE79B76` (`ProductionDepartement_id`);

--
-- Index pour la table `bt_crop`
--
ALTER TABLE `bt_crop`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_673377F827A79878` (`croptype_id`);

--
-- Index pour la table `bt_crop_block`
--
ALTER TABLE `bt_crop_block`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_141656FFF9551DD4` (`productiondepartement_id`),
  ADD KEY `IDX_141656FF888579EE` (`crop_id`);

--
-- Index pour la table `bt_crop_type`
--
ALTER TABLE `bt_crop_type`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `bt_farms`
--
ALTER TABLE `bt_farms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_C9D1AD6C7E3C61F9` (`owner_id`);

--
-- Index pour la table `bt_production_departement`
--
ALTER TABLE `bt_production_departement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C0915D4565FCFA0D` (`farm_id`),
  ADD KEY `IDX_C0915D455243E353` (`grower_id`);

--
-- Index pour la table `bt_recording`
--
ALTER TABLE `bt_recording`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E402F7B1A76ED395` (`user_id`),
  ADD KEY `IDX_E402F7B19744CF8` (`cardlocation_id`);

--
-- Index pour la table `bt_recording_bugs`
--
ALTER TABLE `bt_recording_bugs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_53440AEE8CA9A845` (`recording_id`),
  ADD KEY `IDX_53440AEE1132A6CD` (`bugs_id`);

--
-- Index pour la table `bt_reporting`
--
ALTER TABLE `bt_reporting`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `bt_user`
--
ALTER TABLE `bt_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_7629C2A92FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_7629C2AA0D96FBF` (`email_canonical`);

--
-- Index pour la table `growers_productiondepartement`
--
ALTER TABLE `growers_productiondepartement`
  ADD PRIMARY KEY (`grower_id`,`productionDepartement_id`),
  ADD KEY `IDX_8BCCA40F5243E353` (`grower_id`),
  ADD KEY `IDX_8BCCA40F3431650C` (`productionDepartement_id`);

--
-- Index pour la table `operator_productiondepartement`
--
ALTER TABLE `operator_productiondepartement`
  ADD PRIMARY KEY (`Operator_id`,`productionDepartement_id`),
  ADD KEY `IDX_FB1C971DDED91774` (`Operator_id`),
  ADD KEY `IDX_FB1C971D3431650C` (`productionDepartement_id`);

--
-- Index pour la table `recording`
--
ALTER TABLE `recording`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `sylius_email`
--
ALTER TABLE `sylius_email`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_732D4E1577153098` (`code`);

--
-- Index pour la table `sylius_settings`
--
ALTER TABLE `sylius_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1AFEFB2A894A31AD33E16B56` (`schema_alias`,`namespace`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `acl_classes`
--
ALTER TABLE `acl_classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `acl_entries`
--
ALTER TABLE `acl_entries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `acl_security_identities`
--
ALTER TABLE `acl_security_identities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `bt_bug`
--
ALTER TABLE `bt_bug`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `bt_bugs_crops_config`
--
ALTER TABLE `bt_bugs_crops_config`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `bt_bug_type`
--
ALTER TABLE `bt_bug_type`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `bt_card_location`
--
ALTER TABLE `bt_card_location`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pour la table `bt_crop`
--
ALTER TABLE `bt_crop`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `bt_crop_block`
--
ALTER TABLE `bt_crop_block`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT pour la table `bt_crop_type`
--
ALTER TABLE `bt_crop_type`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `bt_farms`
--
ALTER TABLE `bt_farms`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `bt_production_departement`
--
ALTER TABLE `bt_production_departement`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT pour la table `bt_recording`
--
ALTER TABLE `bt_recording`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT pour la table `bt_recording_bugs`
--
ALTER TABLE `bt_recording_bugs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT pour la table `bt_reporting`
--
ALTER TABLE `bt_reporting`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT pour la table `bt_user`
--
ALTER TABLE `bt_user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT pour la table `recording`
--
ALTER TABLE `recording`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sylius_email`
--
ALTER TABLE `sylius_email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sylius_settings`
--
ALTER TABLE `sylius_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`);

--
-- Contraintes pour la table `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `bt_bug`
--
ALTER TABLE `bt_bug`
  ADD CONSTRAINT `FK_A10F300FEA2C0EE` FOREIGN KEY (`bugtype_id`) REFERENCES `bt_bug_type` (`id`);

--
-- Contraintes pour la table `bt_bugs_crops_config`
--
ALTER TABLE `bt_bugs_crops_config`
  ADD CONSTRAINT `FK_C102E62D888579EE` FOREIGN KEY (`crop_id`) REFERENCES `bt_crop` (`id`),
  ADD CONSTRAINT `FK_C102E62DFA3DB3D5` FOREIGN KEY (`bug_id`) REFERENCES `bt_bug` (`id`);

--
-- Contraintes pour la table `bt_card_location`
--
ALTER TABLE `bt_card_location`
  ADD CONSTRAINT `FK_473E22DFCAE79B76` FOREIGN KEY (`ProductionDepartement_id`) REFERENCES `bt_production_departement` (`id`);

--
-- Contraintes pour la table `bt_crop`
--
ALTER TABLE `bt_crop`
  ADD CONSTRAINT `FK_673377F827A79878` FOREIGN KEY (`croptype_id`) REFERENCES `bt_crop_type` (`id`);

--
-- Contraintes pour la table `bt_crop_block`
--
ALTER TABLE `bt_crop_block`
  ADD CONSTRAINT `FK_141656FF888579EE` FOREIGN KEY (`crop_id`) REFERENCES `bt_crop` (`id`),
  ADD CONSTRAINT `FK_141656FFF9551DD4` FOREIGN KEY (`productiondepartement_id`) REFERENCES `bt_production_departement` (`id`);

--
-- Contraintes pour la table `bt_farms`
--
ALTER TABLE `bt_farms`
  ADD CONSTRAINT `FK_C9D1AD6C7E3C61F9` FOREIGN KEY (`owner_id`) REFERENCES `bt_user` (`id`);

--
-- Contraintes pour la table `bt_production_departement`
--
ALTER TABLE `bt_production_departement`
  ADD CONSTRAINT `FK_C0915D455243E353` FOREIGN KEY (`grower_id`) REFERENCES `bt_user` (`id`),
  ADD CONSTRAINT `FK_C0915D4565FCFA0D` FOREIGN KEY (`farm_id`) REFERENCES `bt_farms` (`id`);

--
-- Contraintes pour la table `bt_recording`
--
ALTER TABLE `bt_recording`
  ADD CONSTRAINT `FK_E402F7B19744CF8` FOREIGN KEY (`cardlocation_id`) REFERENCES `bt_card_location` (`id`),
  ADD CONSTRAINT `FK_E402F7B1A76ED395` FOREIGN KEY (`user_id`) REFERENCES `bt_user` (`id`);

--
-- Contraintes pour la table `bt_recording_bugs`
--
ALTER TABLE `bt_recording_bugs`
  ADD CONSTRAINT `FK_53440AEE1132A6CD` FOREIGN KEY (`bugs_id`) REFERENCES `bt_bug` (`id`),
  ADD CONSTRAINT `FK_53440AEE8CA9A845` FOREIGN KEY (`recording_id`) REFERENCES `bt_recording` (`id`);

--
-- Contraintes pour la table `growers_productiondepartement`
--
ALTER TABLE `growers_productiondepartement`
  ADD CONSTRAINT `FK_8BCCA40F3431650C` FOREIGN KEY (`productionDepartement_id`) REFERENCES `bt_production_departement` (`id`),
  ADD CONSTRAINT `FK_8BCCA40F5243E353` FOREIGN KEY (`grower_id`) REFERENCES `bt_user` (`id`);

--
-- Contraintes pour la table `operator_productiondepartement`
--
ALTER TABLE `operator_productiondepartement`
  ADD CONSTRAINT `FK_FB1C971D3431650C` FOREIGN KEY (`productionDepartement_id`) REFERENCES `bt_production_departement` (`id`),
  ADD CONSTRAINT `FK_FB1C971DDED91774` FOREIGN KEY (`Operator_id`) REFERENCES `bt_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
