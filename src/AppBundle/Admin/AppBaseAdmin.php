<?php

namespace AppBundle\Admin;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bundle\FrameworkBundle\Routing\Router;


class AppBaseAdmin extends AbstractAdmin
{
    protected $translationDomain = 'admin';
    protected $router;
  

    protected $labelTranslatorStrategy = 'sonata.admin.label.strategy.underscore';

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);
       
    }

    public function setSecurityTokenStorage(TokenStorage $securityTokenStorage)
    {
        $this->securityTokenStorage = $securityTokenStorage;
    }

    public function getUser()
    {
        return $this->securityTokenStorage->getToken()->getUser();
    }

    public function getCurrentUser()
    {
        return $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
    }
   public function getCurrentRoute()
    {
        $request = $this->getConfigurationPool()->getContainer()->get('request');
        $this->router = $request->get('_route');
        /* get All Route*/ //$this->router = $this->getConfigurationPool()->getContainer()->get('router')->getRouteCollection();
          return $this->router;

    }

    public function getEM()
    {
        return $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
    }
    public function getContainers(){

           return $this->getConfigurationPool()->getContainer();
    }

    /**
     * {@inheritdoc}
     */
    public function getExportFormats()
    {
        return [
            'csv', 'xls',
        ];
    }
}
