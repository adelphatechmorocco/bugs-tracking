<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\CoreBundle\Validator\ErrorElement;




class ReportingAdmin extends AppBaseAdmin
{

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create'); 
        $collection->remove('edit'); 
    }

     public function prePersist($object){
          
       
    }
     /* public function getBatchActions()
        {
            $actions = parent::getBatchActions();
            unset($actions['delete']);

            return $actions;
        }*/
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
        {
            $em = $this->getEM();
     

             $datagridMapper
                  ->add('dateScouting', 'doctrine_orm_date_range', array(
                       'field_type' => 'sonata_type_date_range_picker',
                         'datepicker_use_button' => false,
                         'field_options' => array(
                          'field_options_start' => array(
                            'format' => 'd.MM.y',
                            'datepicker_use_button' => true,
                            'widget' => 'single_text'
                        ),
                        'field_options_end' => array(
                           'format' => 'd.MM.y',
                            'widget' => 'single_text',
                            'datepicker_use_button' => true
                            )
                   
                    )))
                ->add('farmName',null,array('label' => "farm"))   
                ->add('prodDeptName',null,array('label' => "Prod Dept"))
                ->add('cropBlockName',null,array('label' => "Crop Block"))
                ->add('cropName',null,array('label' => "Crop"))
                ->add('bugName',null,array('label' =>"Bug" ));

           
        }
      public function createQuery($context = 'list')
        {
            $query = parent::createQuery($context);
            $alias = $query->getRootAliases()[0];
            $user = $this->getCurrentUser();
            if($user->hasRole('ROLE_OWNER_FARM')){
                if($user->getUserFarm())
                {
                   $query 
                     ->where($alias.'.farmId = :fId')
                     ->setParameter('fId',$user->getUserFarm()->getId());
                   }else{
                     $query 
                     ->where($alias.'.farmId = :fId')
                     ->setParameter('fId',0);
                     
                   }
           
            }else{
                 $query
                 ->select($alias)
                /* ->orderBy($alias.'.createdAt','DESC')*/;
                
            }
            return $query;
        }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
      
        $listMapper
            ->addIdentifier('dateScouting')
            ->add('farmName',null,array('label' => "farm"))   
            ->add('prodDeptName',null,array('label' => "Prod Dept"))
            ->add('cropBlockName',null,array('label' => "Crop Block"))
            ->add('cropName',null,array('label' => "Crop"))
            ->add('bugName',null,array('label' =>"Bug" ))
            ->add('moyenneamount',null,array('label' => 'Moyenne Amount'))
            ->add('variation',null,array('label' => "Change (%) "))       
            ->add('_action', 'actions', [
                'actions' => [
                    'show' => [
                        'template' => 'Admin/Button/list__action_show.html.twig',
                    ],
                    'delete' => [
                        'template' => 'Admin/Button/list__action_delete.html.twig',
                    ],
                ]
            ])
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('dateScouting')
            ->add('farmName',null,array('label' => "farm"))   
            ->add('prodDeptName',null,array('label' => "Prod Dept"))
            ->add('cropBlockName',null,array('label' => "Crop Block"))
            ->add('cropName',null,array('label' => "Crop"))
            ->add('bugName',null,array('label' =>"Bug" ))
            ->add('moyenneamount',null,array('label' => 'Moyenne Amount'))
            ->add('variation',null,array('label' => "Change (%) "))       
                 ;
      
    }
}