<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;



class CropTypeAdmin extends AppBaseAdmin
{

    public function prePersist($object){
         $Enabled = !$user->hasRole("ROLE_GLOBAL_ADMIN") ? 0 : 1;
         $object->setEnabled($Enabled);
    }
      public function getBatchActions()
        {
            $actions = parent::getBatchActions();
            unset($actions['delete']);

            return $actions;
        }
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
         $datagridMapper
            ->add('Type')
            ->add('enabled');
    }
    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('Type')
            ->add('enabled',null,array('editable' => true))
            ->add('_action', 'actions', [
                'actions' => [
                    'show' => [
                        'template' => 'Admin/Button/list__action_show.html.twig',
                    ],
                    'edit' => [
                        'template' => 'Admin/Button/list__action_edit.html.twig',
                    ],
                    'delete' => [
                        'template' => 'Admin/Button/list__action_delete.html.twig',
                    ],
                ]
            ])
        ;
    }
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {   

        $formMapper
            ->with('Crop Type',array('class' => 'col-lg-7'))
                 ->add('Type',null,array('attr' => ['required' => true,'placeholder' => 'Crop Type'] ))
                  ->add('enabled')
           
            ->end();

        
    }
    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Details',array('class' => 'col-lg-7'))
                ->add('Type ')
            ->end();
      
    }
}