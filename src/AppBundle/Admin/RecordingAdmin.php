<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\RecordingBugs;
use AppBundle\Entity\Recording;
use AppBundle\Entity\CardLocation;
use AppBundle\Entity\ProductionDepartement;


class RecordingAdmin extends AppBaseAdmin
{

       protected $parentAssociationMapping = 'recordingCardlocation';

       protected function configureRoutes(RouteCollection $collection)
        {
            $collection->add('all_bugs');
            $collection->add('recording_show_others');
            $collection->remove('edit');

        }

        protected $perPageOptions = array(1000000,2000000,3000000);
            
            public function getTemplate($name)
            {
                switch ($name) {
                    case 'list':
                        return 'Admin/recording_list.html.twig';;
                        break;
         
                    default:
                        return parent::getTemplate($name);
                        break;
                }
             }
    /* public function getBatchActions()
        {
            $actions = parent::getBatchActions();
            unset($actions['delete']);

            return $actions;
        }*/
        public function prePersist($object){

                  $user = $this->getCurrentUser();
                  $em =$this->getEm();
                  $varCard ="";
                if($user->hasRole('ROLE_OPERATOR') && !$object->getrecordingUser()){
                    $object->setrecordingUser($user);
                }

                $fieldListRecording = $this->getForm()->get('listRecording')->getData();
                $arrayGlobalRecord  =  json_decode($fieldListRecording,true);

                $this->getContainers()->get('sonata.admin.checkBugCrop')->listerAndExec($arrayGlobalRecord,$object->getscoutingDate());
                
                $arrayGlobalRecordD = array_filter($arrayGlobalRecord);
                $arrlast   = end($arrayGlobalRecordD);
        
                 foreach ($arrlast as $listL) {
                     $record = new  RecordingBugs();
                     $bug = $em->getRepository('AppBundle:Bug')->findById($listL['bugId']);
                     $varCard = (int) $listL['cardId'];
                     $record->setRecordingbugsRecording($object);
                     $record->setRecordingbugsBugs($bug[0]);
                     $record->setAmount($listL['amount']);
                     $em->persist($record);
                 }
                
                 $cardLocation = $em->getRepository('AppBundle:CardLocation')->findById($varCard);

                 $object->setrecordingCardlocation($cardLocation[0]);
                  
                    unset ($arrayGlobalRecordD[count($arrayGlobalRecordD)-1]);
                    $countRecord = count($arrayGlobalRecord);  
                   
                    foreach ($arrayGlobalRecordD as $listReco) {
                             $Recording = new Recording();
                             $cardLocationN = $em->getRepository('AppBundle:CardLocation')->findById($listReco[0]['cardId']);
                             $Recording->setrecordingCardlocation($cardLocationN[0]);
                             $Recording->setrecordingUser($object->getrecordingUser());
                             $Recording->setscoutingDate($object->getscoutingDate());
                             $em->persist($Recording);
                        foreach ($listReco as $list) {
                             $recordd = new  RecordingBugs();
                             $bug = $em->getRepository('AppBundle:Bug')->findById($list['bugId']);
                             $recordd->setRecordingbugsRecording($Recording);
                             $recordd->setRecordingbugsBugs($bug[0]);
                             $recordd->setAmount($list['amount']);
                             $em->persist($recordd);
                               
                        }
                      }  
                 $em->flush();
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $em = $this->getEM();
        $user = $this->getCurrentUser();
        if($user->hasRole('ROLE_GROWER'))
        {
              $queryCard = $em->createQueryBuilder('cardLoc')
                ->select('cardLoc')
                ->from('AppBundle:CardLocation','cardLoc')
                ->leftJoin('cardLoc.cardlocationProductiondepartement','prodDept')
                ->where('prodDept.ProductionDepartementUser=:user')
                ->setParameter('user',$user);

        }else if($user->hasRole('ROLE_OWNER_FARM')){
               
                 $queryCard = $em->createQueryBuilder('cardLoc')
                ->select('cardLoc')
                ->from('AppBundle:CardLocation','cardLoc')
                ->leftJoin('cardLoc.cardlocationProductiondepartement','prodDept')
                ->leftJoin('prodDept.productiondepartementFarm','farm')
                ->where('farm.farmUser=:user')
                ->setParameter('user',$user);
        }else{
             $queryCard = $em->createQueryBuilder('cardLoc')
                ->select('cardLoc')
                ->from('AppBundle:CardLocation','cardLoc');
        }
         $datagridMapper
             ->add('recordingCardlocation',null,['label' => 'Card Location'],null,array('query_builder' => $queryCard))
             ->add('recordingCardlocation.cardlocationProductiondepartement')
             ->add('scoutingDate', 'doctrine_orm_date_range', array(
                   'field_type' => 'sonata_type_date_range_picker',
                     'datepicker_use_button' => false,
                     'field_options' => array(
                      'field_options_start' => array(
                        'format' => 'd.MM.y',
                        'datepicker_use_button' => true,
                        'widget' => 'single_text'
                    ),
                    'field_options_end' => array(
                       'format' => 'd.MM.y',
                        'widget' => 'single_text',
                        'datepicker_use_button' => true
                        )
               
                )));
       
    }
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $alias = $query->getRootAliases()[0];
        $user = $this->getCurrentUser();
        if($user->hasRole('ROLE_OPERATOR'))
        {
            $query
                ->select($alias)
                ->where($alias.'.recordingUser=:user')
                ->setParameter('user',$user);
        }
        else if($user->hasRole('ROLE_GROWER'))
        {
            $query
            ->leftJoin($alias.'.recordingCardlocation','cardL')
            ->leftJoin('cardL.cardlocationProductiondepartement','Prod')
            ->where('Prod.ProductionDepartementUser=:user')
            ->setParameter('user',$user);
        }else if($user->hasRole('ROLE_OWNER_FARM')){
                $query
                ->leftJoin($alias.'.recordingCardlocation','cardL')
                ->leftJoin('cardL.cardlocationProductiondepartement','Prod')
                ->leftJoin('Prod.productiondepartementFarm','Farm')
                ->where('Farm.farmUser=:user')
                ->setParameter('user',$user);
        }else{
             $query
             ->select($alias)
            /* ->orderBy($alias.'.createdAt','DESC')*/;
            
        }
        return $query;
    }
    
    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {

        $user = $this->getCurrentUser();
        $listMapper
            ->addIdentifier('recordingCardlocation',null,array('label' => 'Card location'))
            ->add('recordingCardlocation.cardlocationProductiondepartement',null,['label'=>'Production Deparetemnt']);
              if(!$user->hasRole('ROLE_OPERATOR')){
                $listMapper
                 ->add('recordingUser',null,['label'=>'Operator']);
             }
               $listMapper
            ->add('scoutingDate',null,array('pattern'  => 'd-MM-y'))
                /*  'test' => [
                        'template' => 'Admin/Button/show_all_recording.html.twig',
                    ],*/
            ->add('_action', 'actions', [
                'actions' => [

                    'recordingRecordingbugs' => [
                        'template' => 'Admin/recording_bugs_recording_list.html.twig',
                    ],
                   
                    'show' => [
                        'template' => 'Admin/Button/list__action_show.html.twig',
                    ],
                    
                    'delete' => [
                        'template' => 'Admin/Button/list__action_delete.html.twig',
                    ],
                ]
            ])
        ;
          /*dump($listMapper);
        die;*/
    }
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {   

         $user = $this->getCurrentUser();
         $em = $this->getEM();
         $Role ="ROLE_OPERATOR";
        
         if($user->hasRole('ROLE_OPERATOR')){
                        $query = $em->createQueryBuilder('Prod')
                             ->select('Prod')
                             ->from('AppBundle:ProductionDepartement', 'Prod')
                             ->innerJoin('Prod.ProductionDepartementOperator', 'u')
                             ->where('u.id=:usr')
                             ->setParameter('usr',$user->getId());

         }
           else if($user->hasRole('ROLE_GROWER')){
                        $query = $em->createQueryBuilder('Prod')
                             ->select('Prod')
                             ->from('AppBundle:ProductionDepartement', 'Prod')
                             ->where('Prod.ProductionDepartementUser = :user')
                             ->orderBy('Prod.createdAt','DESC')
                             ->setParameter('user', $user);

                   $queryTow = $em->createQueryBuilder('user')
                         ->select('user')
                         ->from('AppBundle:User', 'user')
                         ->where('user.managerId=:user')
                         ->andWhere('user.enabled=1')
                         ->andWhere('user.roles LIKE :Op')
                        ->setParameters(array('Op' => '%'.$Role.'%','user' => $user->getId()));


         }else if($user->hasRole('ROLE_OWNER_FARM')){
                        $query = $em->createQueryBuilder('Prod')
                           ->select('Prod')
                           ->from('AppBundle:ProductionDepartement','Prod')
                           ->leftJoin('Prod.productiondepartementFarm','Farm')
                           ->where('Farm.farmUser =:user')
                            ->orderBy('Prod.createdAt','DESC')
                           ->setParameter('user',$user);
                   $Allgrowers = $em->getRepository('AppBundle:User')->findBy(array('managerId' => $user->getId()));
                   $listManagerId = []; 
                   foreach($Allgrowers as $grower)
                   {
                        $listManagerId[] =$grower->getId(); 
                   }
                  $listManagerId = array_unique($listManagerId);

                           $queryTow = $em->createQueryBuilder('user')
                             ->select('user')
                             ->from('AppBundle:User', 'user')
                             ->where('user.managerId in (:listManagerId)')
                             ->andWhere('user.enabled=1')
                             ->andWhere('user.roles LIKE :Op')
                            ->setParameters(array('Op' => '%'.$Role.'%','listManagerId' => $listManagerId));
         }else{
                   $query = $em->createQueryBuilder('Prod')
                           ->select('Prod')
                           ->from('AppBundle:ProductionDepartement','Prod')
                            ->orderBy('Prod.createdAt','DESC');
                             $queryTow = $em->createQueryBuilder('user')
                             ->select('user')
                             ->from('AppBundle:User', 'user')
                             ->where('user.enabled=1')
                             ->andWhere('user.roles LIKE :Op')
                            ->setParameter('Op','%'.$Role.'%');
                         
         }
      
          
        $formMapper
             ->with('Recording',array('class' => 'col-lg-12 record notPrint'));
               if(!$user->hasRole('ROLE_OPERATOR'))
                    {
                    $formMapper
                       ->add('recordingUser', 'sonata_type_model',array(
                            'class' => 'AppBundle:User',
                            'required' => true,
                            'empty_value' => 'Choose a Operator',   
                            'query' => $queryTow,
                            'label' => 'Operator'
                        ));
                     
                   }/*else{
                     $formMapper
                     ->add('recordingUser', 'entity', [
                            'required' => false,
                            'class' => 'AppBundle\Entity\user', 
                            'attr' =>['class' => 'a'],
                             'empty_data'  => null,
                            'query_builder' => function (EntityRepository $er) use ($user) {
                              return $er
                                ->createQueryBuilder('u')
                                ->where('u.id ='. $user->getId())
                                ;
                             },                       
                        ])
                                                                                                                                                                                                        
                     ;
                   }*/
                    $formMapper
                      ->add('scoutingDate','sonata_type_datetime_picker',array('label' => 'Scouting date','required' => true,'dp_side_by_side' => false,'dp_use_current' => true, 'dp_use_seconds' => false, 'format' => 'y-MM-d'))
                  
                    
                     ->add('DepartementProduction', 'entity', [
                                            'label' => 'Production Departement',
                                            'class' => 'AppBundle\Entity\ProductionDepartement',
                                            'empty_value' => 'Choose a Production Departement',   
                                            'attr' => ['class' => 'Production-departement-cordonner-recording'],   
                                            'query_builder' => $query,
                                            'multiple' => false,
                                            'expanded' => false,
                                            'required' => false,
                                            'mapped' => false,                   
                                        ])
                     ->add('recordingCardlocation','hidden',array('required' => false ))
                     ->add('listRecording','hidden',['mapped' => false ,'label' => ' ' ,'required' => false,'attr'=> ['class' => 'listRecording '] ])
             
                ->end()
                ->with('Table recording',array('class' => 'col-lg-12 tableRecording'))

                ->end()
                ->with('Map',array('class' => 'col-lg-12 mapRecording modalMy'))

                ->end();
             
                
    }
    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General',array('class' => 'col-lg-5'))
                ->add('recordingCardlocation',null,array('label' => 'Card location'))
                ->add('recordingUser',null,['label'=>'User'])
                ->add('scoutingDate',null,array('label' => 'Scouting Date'))
              ->end();
    }
}