<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityRepository;



class BugsCropsConfigAdmin extends AppBaseAdmin
{


   public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        unset($actions['delete']);

        return $actions;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('config_crop');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {   
        $formMapper
                    ->add('bug', 'entity', [
                                            'label' => 'Bugs',
                                            'required' => true,
                                            'class' => 'AppBundle\Entity\Bug',
                                             'attr' => ['class' => 'get-bugs-config'],
                                             'empty_value' => 'Choose a bug', 
                                             'query_builder' => function (EntityRepository $er) {
                                               return $er
                                                  ->createQueryBuilder('u')
                                                  ->where('u.enabled = 1')
                                                  ->orderBy('u.priority', 'ASC')
                                                  ;
                                                },
                                                             
                                        ])
                    ->add('Crop', 'entity', [
                                            'label' => 'Crops',
                                            'required' => true,
                                            'class' => 'AppBundle\Entity\Crop',
                                             'attr' => ['class' => 'get-crops-config'],
                                             'empty_value' => 'Choose a Crop', 
                                             'query_builder' => function (EntityRepository $er) {
                                               return $er
                                                  ->createQueryBuilder('u')
                                                  ->where('u.enabled = 1')
                                                  ;
                                                },
                                                             
                                        ])

                    ->add('min','text',array('label' =>' Min'))
                    ->add('max','text',array('label' =>' Max'))
        ;
    }
      /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
           
     
         $datagridMapper
            ->add('crop')
            ->add('bug');
          
    }
   
    
    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $user= $this->getCurrentUser();
        $listMapper
            ->addIdentifier('bug',null,array('label' => 'Bug'))
            ->add('crop',null,array('label' => 'Crop'))
            ->add('min',null,array('label' =>' Min'))
            ->add('max',null,array('label' =>' Max'));
            if($user->hasRole('ROLE_GLOBAL_ADMIN'))
            {
              $listMapper
              ->add('_action', 'actions', [
                'actions' => [
                    'edit' => [
                        'template' => 'Admin/Button/list__action_edit.html.twig',
                    ],
                    'delete' => [
                        'template' => 'Admin/Button/list__action_delete.html.twig',
                    ],
                ]
            ])
            ;

            }
            
    }
   
  
}