<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Farms;




class ProductionDepartementAdmin extends AppBaseAdmin
{
    
   protected $parentAssociationMapping = 'productiondepartementFarm';

   protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('coordinates_polygon');
        $collection->add('coordinates_polygon_production_departemnet');

    }
     public function getBatchActions()
        {
            $actions = parent::getBatchActions();
            unset($actions['delete']);

            return $actions;
        }



    public function prePersist($object){
        /*dump($object);
        die;*/
    }
    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $alias=$query->getRootAliases()[0];
        $user = $this->getCurrentUser();
        
        if ($user->hasRole('ROLE_GROWER')) {
            $query
                ->andWhere(
                    $query->expr()->eq($alias . '.ProductionDepartementUser', ':growerId')
                )
                /*->orderBy($alias . '.createdAt', 'ASC')*/
                ->setParameter('growerId',$this->getCurrentUser());
                
        }else if($user->hasRole('ROLE_OWNER_FARM')){
              $query
                ->leftJoin($alias.'.productiondepartementFarm','u')
                ->where($query->expr()->in('u.farmUser',":id"))
                ->setParameter('id', $user->getId())
                /*->orderBy($alias . '.createdAt', 'ASC')*/;
                
        }else if($user->hasRole('ROLE_OPERATOR')){
              $query
               ->innerJoin($alias . '.ProductionDepartementOperator', 'u')
               ->where('u.id=:usr')
               ->setParameter('usr',$user->getId());
        }else{
            $query
                ->select($alias)
               /* ->orderBy($alias . '.createdAt', 'ASC')*/;
               
        }

        return $query;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {

          $Role = "ROLE_GROWER";
          $em = $this->getEM();
          $user= $this->getCurrentUser();
           if($user->hasRole('ROLE_OWNER_FARM'))
             {
                 $query = $em->createQueryBuilder('Farm')
                      ->select('Farm')
                      ->from('AppBundle:Farms', 'Farm')
                      ->where('Farm.farmUser = :user')
                      ->setParameter('user', $user);

                 $queryTow = $em->createQueryBuilder('user')
                      ->select('user')
                      ->from('AppBundle:User', 'user')
                      ->where('user.roles LIKE :grower')
                      ->andWhere('user.enabled=1')
                      ->andWhere('user.managerId=:MnId')
                       ->setParameters(array('grower' => '%'.$Role.'%','MnId' => $user->getId()));

             }else if($user->hasRole('ROLE_GROWER'))
             {
                 $query = $em->createQueryBuilder('Farm')
                      ->select('Farm')
                      ->from('AppBundle:Farms', 'Farm')
                      ->leftJoin('Farm.farmProductiondepartement' ,'Pdt')
                      ->where('Pdt.ProductionDepartementUser = :user')
                      ->setParameter('user', $user);

             }
             else {
                  $query = $em->createQueryBuilder('Farm')
                    ->select('Farm')
                    ->from('AppBundle:Farms','Farm')
                    ->orderBy('Farm.createdAt','ASC');

                     $queryTow = $em->createQueryBuilder('user')
                             ->select('user')
                             ->from('AppBundle:User', 'user')
                             ->where('user.roles LIKE :grower')
                             ->andWhere('user.enabled=1')
                             ->setParameter('grower','%'.$Role.'%');
              }
         $datagridMapper
             ->add('name')
             ->add('area');
            /* ->add('productiondepartementFarm', null, array('label' => 'Farm'), null, array('required'=> false,
                    'query_builder' => $query ));*/
             if($user ->hasRole('ROLE_OWNER_FARM') || $user->hasRole('ROLE_GLOBAL_ADMIN')){
                $datagridMapper
                  ->add('ProductionDepartementUser', null, array('label' => 'Grower'), null, array('required'=> false,
                    'query_builder' => $queryTow ));
             }
    }
    
    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
      $user = $this->getCurrentUser();
      if(!$user->hasRole('ROLE_GROWER')){
        $listMapper
            ->add('_action', 'actions', [
                'actions' => [
                  'productiondepartementCropblock' => [
                     'template' => 'Admin/cropblock_production_departement_list.html.twig',

                   ],
                   'ProductiondepartementCardlocation' => [
                     'template' => 'Admin/cardlocation_productiondepratment_list.html.twig',

                   ],
                   
                    /*'show' => [
                        'template' => 'Admin/Button/list__action_show.html.twig',
                    ],
                    'edit' => [
                        'template' => 'Admin/Button/list__action_edit.html.twig',
                    ],
                    'delete' => [
                        'template' => 'Admin/Button/list__action_delete.html.twig',
                    ],*/
                ]
            ]);
          }

          $listMapper
            ->addIdentifier('name',null,['label'=>'Production departement name'])
           
           /* ->add('productiondepartementFarm', null, array('label' => 'Farm',
                    'sortable' => true,
                    'sort_field_mapping'=> array('fieldName'=>'farmName'),
                    'sort_parent_association_mappings' => array(array('fieldName'=>'productiondepartementFarm'))
             ))*/
              ->add('ProductionDepartementUser',null,array('label' => 'Grower' ))
             ->add('area',null,array('label' => 'Area (m)'));
      
      
    }
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {   
            $user = $this->getCurrentUser();
            $em = $this->getEM();
            $Role = "ROLE_GROWER";

            if($user->hasRole('ROLE_OWNER_FARM'))
             {
                   $query = $em->createQueryBuilder('Farm')
                    ->select('Farm')
                    ->from('AppBundle:Farms', 'Farm')
                    ->where('Farm.farmUser = :user')
                    ->setParameter('user', $user);
                     $queryTow = $em->createQueryBuilder('user')
                        ->select('user')
                        ->from('AppBundle:User', 'user')
                        ->where('user.roles LIKE :grower')
                        ->andWhere('user.enabled=1')
                        ->andWhere('user.managerId=:MnId')
                        ->setParameters(array('grower' => '%'.$Role.'%','MnId' => $user->getId()));

             }
             else {
                  $query = $em->createQueryBuilder('Farm')
                    ->select('Farm')
                    ->from('AppBundle:Farms','Farm')
                    ->orderBy('Farm.createdAt','ASC');

                 $queryTow = $em->createQueryBuilder('user')
                                ->select('user')
                                ->from('AppBundle:User', 'user')
                                 ->where('user.roles LIKE :grower')
                                 ->andWhere('user.enabled=1')
                                ->setParameter('grower','%'.$Role.'%');
        
              }

        $formMapper
          ->tab(' ')
                 ->with('Production Departement',array('class' => 'col-lg-4'))


                       ->add('ProductionDepartementUser', 'sonata_type_model',array(
                            'class' => 'AppBundle:User',
                            'required' => true,
                            'empty_value' => 'Choose a Grower',  
                            'query' => $queryTow,
                            'label' => 'Grower'
                        ))
                          ->add('productiondepartementFarm', 'entity', [
                                                'label' => 'Farm',
                                                'required' => true,
                                                'class' => 'AppBundle\Entity\Farms',
                                                'empty_value' => 'Choose a Farm',   
                                                'attr' => ['class' => 'farm-cordonner '],
                                                'query_builder' => $query,                  
                                    ])
                       ->add('name',null,array('label' => 'Production departement name'))
                       ->add('area',null,array('label' => 'Area (m²)','attr'=>['class' => 'area btn-toggle-info']))
                       ->add('polygoninformation','textarea',array('label' => 'polygon production departement info','attr'=>array("class" => "polygoninformation hideToggle",'required' => true)))
                            ->add('positionlat',null,array('label' => 'Position production departement latitude','attr'=>array("class" => "positionlat hideToggle" )))
                            ->add('positionlang',null,array('label' => 'Position production departement longitude','attr'=>array("class" => "positionlang hideToggle")))
                            ->add('positionzoom',null,array('label' => 'Position production departement zoom','attr'=>array("class" => "positionzoom hideToggle")))
                            ->add('centerOfPolyLat','hidden',['attr'=>["class" => "centerOfPolyLat"]])
                            ->add('centerOfPolyLang','hidden',['attr' =>["class" => "centerOfPolyLang"]])
                            
                
                  ->end()
                   
                 ->with('Map',array('class' => 'col-lg-8 MapMap'))
                 ->end()
                 
                ->end();       
    }
    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General',array('class' => 'col-lg-5'))
                 ->add('productiondepartementFarm',null,['label' => 'Farm'])
                 ->add('name',null,array('label' => 'Production departement name'))
                 ->add('ProductionDepartementUser',null,array('label' => 'Grower'))
                  ->add('area',null,array('label' => 'Area (m²)'))
              ->end()
              ->with('Map',array('class' => 'col-lg-7'))
                     ->add('polygoninformation','string',['Farm' =>'Border of the farm','template' => 'Admin/production_map_show.html.twig', 
                              
                             ])
             ->end();
    }
}