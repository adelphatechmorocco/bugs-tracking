<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityRepository;



class CardLocationAdmin extends AppBaseAdmin
{

     protected $parentAssociationMapping = 'cardlocationProductiondepartement';

     public function getBatchActions(){
            $actions = parent::getBatchActions();
            unset($actions['delete']);

            return $actions;
        }
     public function configure(){
                parent::configure();
                $this->classnameLabel = "Counting Device";
        }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('coordinates_polygon_cardlocation');
        $collection->add('cardlocation_markers');
        $collection->add('cardlocation_show_production_departement');
    }

    public function prePersist($object){
         $date = new \DateTime('now');
         $em = $this->getEM();

         $LastInsert = $em->getRepository('AppBundle:Cardlocation')->getLastInsert();
         if(!empty($LastInsert)){
             $LastId       = (Int) $LastInsert->getId() +1;
         }else{
             $LastId       = 1;
         }
            
         $ProdDeptName = $object->getCardlocationProductiondepartement()->getName();
         $FarmName     = $object->getCardlocationProductiondepartement()->getProductiondepartementFarm()->getFarmName();
         $NameOfCard   = $FarmName .'-'.$ProdDeptName.'-C'.$LastId;
        // $object->setStartDate($date);
         $name = $object->getName() ? $object->getName() : $NameOfCard;
         $object->setName($name);
        // $object->setEnabled(1);
    }

    public function createQuery($context='list') 
    {
      $query = parent::createQuery($context);
      $alias = $query->getRootAliases()[0];
      $user  = $this->getCurrentUser();
       if($user->hasRole('ROLE_GROWER'))
        {
           $query 
                ->leftJoin($alias.'.cardlocationProductiondepartement' ,'prod')
                ->where('prod.ProductionDepartementUser=:user')
              /*  ->orderBy($alias . '.createdAt', 'DESC')*/
                ->setParameter('user',$user);

        }else if($user->hasRole('ROLE_OWNER_FARM'))
        {
          $query
             ->leftJoin($alias.'.cardlocationProductiondepartement' ,'prod')
             ->leftJoin('prod.productiondepartementFarm','farms')
             ->where('farms.farmUser=:user')
            /* ->orderBy($alias . '.createdAt', 'DESC')*/
             ->setParameter('user',$user);

        }
        else{
              $query
                 ->select($alias)
                /* ->orderBy($alias . '.createdAt', 'DESC')*/;

        }
        return $query;

    } 

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
           $em    = $this->getEM();
           $user  = $this->getCurrentUser();
           $query = $em->createQueryBuilder('crop')
                            ->select('crop')
                            ->from('AppBundle:Crop','crop')
                            ->where('crop.enabled = 1');
                    if($user->hasRole('ROLE_GROWER'))
                    {
                        $queryProdDept = $em->createQueryBuilder('ProdDept')
                                    ->select('ProdDept')
                                    ->from('AppBundle:ProductionDepartement','ProdDept')
                                    ->where('ProdDept.ProductionDepartementUser =:grower')
                                    ->setParameter('grower',$user);
                                  

                    }elseif ($user->hasRole('ROLE_OWNER_FARM')) {
                       $queryProdDept = $em->createQueryBuilder('ProdDept')
                                            ->select('ProdDept')
                                            ->from('AppBundle:ProductionDepartement','ProdDept')
                                            ->leftjoin('ProdDept.productiondepartementFarm' ,'farm')
                                             ->where('farm.farmUser =:Owner')
                                             ->setParameter('Owner',$user);
                       
                    }else{
                        $queryProdDept = $em->createQueryBuilder('ProdDept')
                                    ->select('ProdDept')
                                    ->from('AppBundle:ProductionDepartement','ProdDept');
                    }
         $datagridMapper
            ->add('name')
          /*  ->add('cardlocationProductiondepartement',null,array("label" => "Production Departement"),null,array('query_builder' => $queryProdDept))*/
            ->add('startDate', 'doctrine_orm_date_range', array(
                   'field_type' => 'sonata_type_date_range_picker',
                     'datepicker_use_button' => false,
                     'field_options' => array(
                      'field_options_start' => array(
                        'format' => 'd.MM.y',
                        'datepicker_use_button' => true,
                        'widget' => 'single_text'
                    ),
                    'field_options_end' => array(
                       'format' => 'd.MM.y',
                        'widget' => 'single_text',
                        'datepicker_use_button' => true
                        )
               
                )))
             ->add('endDate', 'doctrine_orm_date_range', array(
                   'field_type' => 'sonata_type_date_range_picker',
                     'datepicker_use_button' => false,
                     'field_options' => array(
                      'field_options_start' => array(
                        'format' => 'd.MM.y',
                        'datepicker_use_button' => true,
                        'widget' => 'single_text'
                    ),
                    'field_options_end' => array(
                       'format' => 'd.MM.y',
                        'widget' => 'single_text',
                        'datepicker_use_button' => true
                        )
               
                )))
        ;;
    }
    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name',null,array('label' => 'Counting Device'))
           /* ->add('cardlocationProductiondepartement',null,['label' => 'Production departement'])*/
             
            ->add('startDate')
            ->add('endDate')
           
           /* ->add('_action', 'actions', [
                'actions' => [
                    'cardlocationRecording' =>[ 
                        'template' => 'Admin/recording_cardlocation_list.html.twig'
                        ],
                    'show' => [
                        'template' => 'Admin/Button/list__action_show.html.twig',
                    ],
                    'edit' => [
                        'template' => 'Admin/Button/list__action_edit.html.twig',
                    ],
                    'delete' => [
                        'template' => 'Admin/Button/list__action_delete.html.twig',
                    ],
                ]
            ])*/
        ;
    }
    /**
     * @param FormMapper $formMapper
     */
      protected function configureFormFields(FormMapper $formMapper)
      {   

            $user = $this->getCurrentUser();
            $em = $this->getEM();
            $Role ="ROLE_OPERATOR";
            /*$PriorityFieldOptions = array('required' => true);
            $PriorityFieldOptions['help'] = '<input  type="hidden" id="priority-cardlocation" value="1">';*/
            $cardlocation = $em->getRepository('AppBundle:CardLocation')->findOneBy(array(),array('id' => 'DESC'));
            $priority     = ($cardlocation ? $cardlocation->getPriority() + 1 : 1 );
            if($user->hasRole('ROLE_OWNER_FARM'))
             {
                $query = $em->createQueryBuilder('Farm')
                    ->select('Farm')
                    ->from('AppBundle:Farms', 'Farm')
                    ->where('Farm.farmUser = :user')
                    ->setParameter('user', $user);

                $AllGrowers = $em->getRepository('AppBundle:User')->findBy(array('managerId' => $user->getId()));
                    $listUser = [];
                    foreach ($AllGrowers as $Grower) {
                         $listUser[]=(int)$Grower->getId();
                    }
                    $listUser[]= (int)$user->getId();  
                    $managerList = array_unique($listUser); 

                $queryProdDept = $em->createQueryBuilder('ProdDept')
                                            ->select('ProdDept')
                                            ->from('AppBundle:ProductionDepartement','ProdDept')
                                            ->leftjoin('ProdDept.productiondepartementFarm' ,'farm')
                                             ->where('farm.farmUser =:Owner')
                                             ->setParameter('Owner',$user);

             }
             else if($user->hasRole('ROLE_GROWER'))
             {
               $query = $em->createQueryBuilder('Farm')
                    ->select('Farm')
                    ->from('AppBundle:Farms', 'Farm')
                    ->leftJoin('Farm.farmProductiondepartement','pd')
                    ->leftJoin('pd.ProductiondepartementCardlocation','cl')
                    ->where('pd.ProductionDepartementUser = :user')
                    ->setParameter('user',$user);
               $queryProdDept = $em->createQueryBuilder('ProdDept')
                                ->select('ProdDept')
                                ->from('AppBundle:ProductionDepartement','ProdDept')
                                ->where('ProdDept.ProductionDepartementUser =:grower')
                                ->setParameter('grower',$user);
                                  

             }
              else
              {
                    $query = $em->createQueryBuilder('Farm')
                        ->select('Farm')
                        ->from('AppBundle:Farms','Farm')
                        ->orderBy('Farm.createdAt','ASC');
                         $queryProdDept = $em->createQueryBuilder('ProdDept')
                                    ->select('ProdDept')
                                    ->from('AppBundle:ProductionDepartement','ProdDept');
                  
               }
            $checkId = $this->subject->getId() ? true : false ;

            $formMapper
            ->tab('General')
                ->with('General',array('class' => 'col-lg-4'))
                ->add('name',null,array('required' => false,'attr' =>['class' => 'name-cardLocation']))
              
                ->add('Farms', 'entity', array(
                          'class' => 'AppBundle\Entity\Farms',
                           'attr' => ['class' => 'farms-departement-cordonner-cardLocation'],
                          'label'=> "Farms:",
                          'query_builder' => $query,
                          'empty_value' => 'Choose a Farms',    
                              'multiple' => false,
                              'expanded' => false,
                              'required' => false,
                              'mapped' => false,
                                 
                    ))
                 ->add('cardlocationProductiondepartement', 'entity', [
                                    'label' => 'Production Departement',
                                    'required' => true,
                                    'class' => 'AppBundle\Entity\ProductionDepartement',
                                    'empty_value' => 'Choose a Production Departement',   
                                    'attr' => ['class' => 'Production-departement-cordonner-cardLocation'],
                                    'multiple' => false,
                                    'expanded' => false,
                                    'required' => true,
                                    'query_builder' => $queryProdDept
                                                    
                                ])

                  ->add('priority',null,array('attr' => [ 'class' => 'priorityVal' ])) 

                  ->add('startDate','sonata_type_datetime_picker',array('label' => 'Start Date','attr' => ['class' =>'StartDateMyDate'], 'required' => true,'dp_side_by_side' => false,'dp_use_current' => true, 'dp_use_seconds' => false, 'format' => 'y-MM-d'))
                 ->add('endDate','sonata_type_datetime_picker',array('label' => 'End Date','attr' => ['class' => ' btn-toggle-info'],'required' => false,'dp_side_by_side' => false,'dp_use_current' => true, 'dp_use_seconds' => false, 'format' => 'y-MM-d'))
              
                  ->add('latitude','text',array('attr'=>array("class" => "latitude hideToggle")))
                  ->add('longitude','text',array('attr'=>array("class" => "longitude hideToggle")))
                  ->add('positionlat',null,array('label' => 'Position card latitude','attr'=>array("class" => "positionlat hideToggle" )))
                  ->add('positionlang',null,array('label' => 'Position card longitude','attr'=>array("class" => "positionlang hideToggle")))
                  ->add('positionzoom',null,array('label' => 'Position card zoom','attr'=>array("class" => "positionzoom hideToggle")))
                
                ->end()
               
                ->with('Coordinates Map',array('class' => 'col-lg-8 MapMap'))

                ->end()
               

            ->end();
      }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Details',array('class' => 'col-lg-5'))
                  ->add('name')
                  ->add('cardlocationProductiondepartement',null,array('label' => 'Production Departement'))
                   ->add('priority')
                  ->add('startDate')
                  ->add('endDate')
            ->end()
            ->with('Coordinates map',array('class' => 'col-lg-7'))
                    ->add('Card location','string',['Farm' =>'Border of the farm','template' => 'Admin/cradlocation_map_show.html.twig', 
                              
                             ])
                 
            ->end();
      
    }
}