<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\CropBlock;
use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\ORM\Query\Expr\Join;



class CropBlockAdmin extends AppBaseAdmin
{


  protected $parentAssociationMapping = 'cropblockProductiondepartement';
    public function getBatchActions()
        {
            $actions = parent::getBatchActions();
            unset($actions['delete']);

            return $actions;
        }

   protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('coordinates_polygon_cropblock');
        $collection->add('farm_production_departement_cropblock');
        $collection->add('departement_production_cropblock');
        $collection->add('cropblock_recording_date');
    }
     

    public function prePersist($object)
    {
          $fieldCropBlockNewObject = $this->getForm()->get('cloneAndChange')->getData();
          $fieldCropBlockNewObject = json_decode($fieldCropBlockNewObject,true);
          if(!empty($fieldCropBlockNewObject))
          {
           
            $em= $this->getEM();
            $crop  = $em->getRepository('AppBundle:Crop')->find(intVal($fieldCropBlockNewObject['crop']));
            $cropBlock = new CropBlock();
            $cropBlock->setName($fieldCropBlockNewObject['name']);
            $cropBlock->setArea($object->getArea());
            $cropBlock->setPositionlat($object->getPositionlat());
            $cropBlock->setPositionlang($object->getPositionlang());
            $cropBlock->setPositionzoom($object->getPositionzoom());
            // $cropBlock->setStartDate(new \DateTime($fieldCropBlockNewObject['startDate']));
             $cropBlock->setStartDate($object->getEndDate());
           
            if(!empty($fieldCropBlockNewObject['endDate']))
            {     
                    $cropBlock->setEndDate(new \DateTime($fieldCropBlockNewObject['endDate']));
            }

            $cropBlock->setPolygoninformation($object->getPolygoninformation());
            $cropBlock->setCrop($crop);
           
            $cropBlock->setCenterOfPolyLat($object->getCenterOfPolyLat());
            $cropBlock->setCenterOfPolyLang($object->getCenterOfPolyLang());
            $cropBlock->setCropblockProductiondepartement($object->getCropblockProductiondepartement());
            $em->persist($cropBlock);
            $em->flush();
            $this->getRequest()->getSession()->getFlashBag()->add("sonata_flash_success", "Item ".$fieldCropBlockNewObject['name']." has been successfully created.");
          }
          
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
           $em    = $this->getEM();
           $user  = $this->getCurrentUser();
           $query = $em->createQueryBuilder('crop')
                            ->select('crop')
                            ->from('AppBundle:Crop','crop')
                            ->where('crop.enabled = 1');
                    if($user->hasRole('ROLE_GROWER'))
                    {
                        $queryProdDept = $em->createQueryBuilder('ProdDept')
                                            ->select('ProdDept')
                                            ->from('AppBundle:ProductionDepartement','ProdDept')
                                            ->where('ProdDept.ProductionDepartementUser =:grower')
                                            ->setParameter('grower',$user);
                                  

                    }elseif ($user->hasRole('ROLE_OWNER_FARM')) {
                       $queryProdDept = $em->createQueryBuilder('ProdDept')
                                            ->select('ProdDept')
                                            ->from('AppBundle:ProductionDepartement','ProdDept')
                                            ->leftjoin('ProdDept.productiondepartementFarm' ,'farm')
                                             ->where('farm.farmUser =:Owner')
                                             ->setParameter('Owner',$user);
                       
                    }else{
                        $queryProdDept = $em->createQueryBuilder('ProdDept')
                                    ->select('ProdDept')
                                    ->from('AppBundle:ProductionDepartement','ProdDept');
                    }
                     $now  = new \DateTime();

     
         $datagridMapper
            ->add('name')
            ->add('area')
           /* ->add('cropblockProductiondepartement',null,array("label" => "Production Departement"),null,array('query_builder' => $queryProdDept))*/
            ->add('Crop',null,array(),null,array('required' => false, 'query_builder' => $query))
            ->add('startDate', 'doctrine_orm_date_range', array(
                   'field_type' => 'sonata_type_date_range_picker',
                     'datepicker_use_button' => false,
                     'field_options' => array(
                      'field_options_start' => array(
                        'format' => 'd.MM.y',
                        'datepicker_use_button' => true,
                        'widget' => 'single_text'
                    ),
                    'field_options_end' => array(
                       'format' => 'd.MM.y',
                        'widget' => 'single_text',
                        'datepicker_use_button' => true
                        )
               
                )))
             ->add('endDate', 'doctrine_orm_date_range', array(
                   'field_type' => 'sonata_type_date_range_picker',
                     'datepicker_use_button' => false,
                     'field_options' => array(
                      'field_options_start' => array(
                        'format' => 'd.MM.y',
                        'datepicker_use_button' => true,
                        'widget' => 'single_text'
                    ),
                    'field_options_end' => array(
                       'format' => 'd.MM.y',
                        'widget' => 'single_text',
                        'datepicker_use_button' => true
                        )
               
                )))
        ;
          
    }
    
    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            /*->add('_action', 'actions', [
                'actions' => [
                    
                     'show' => [
                        'template' => 'Admin/Button/list__action_show.html.twig',
                    ],
                    'edit' => [
                        'template' => 'Admin/Button/list__action_edit.html.twig',
                    ],
                    'delete' => [
                        'template' => 'Admin/Button/list__action_delete.html.twig',
                    ],
                ]
            ])*/
                 ->addIdentifier('name',null,array('label' => 'Crop block name'))
              /*   ->add('cropblockProductiondepartement',null,array('label' => 'Production Departement'))*/
                 ->add('Crop',null,array('label'=> 'Crop name'))
                 ->add('area',null,array('label' => 'Area (m)'))
                 ->add('startDate')
                 ->add('endDate')
               
           
        ;
    }
 
      public function createQuery($context = 'list')
        {
            $query = parent::createQuery($context);
            $alias=$query->getRootAliases()[0];
            $user = $this->getCurrentUser();
            
            if($user->hasRole('ROLE_OWNER_FARM'))
            {
                 $query
                ->leftJoin($alias.'.cropblockProductiondepartement','pd')
                ->leftJoin('pd.productiondepartementFarm','fr')
                ->andWhere($query->expr()->in('fr.farmUser',":id"))
                ->setParameter('id', $user->getId())
               /* ->orderBy($alias . '.cropblockProductiondepartement', 'DESC')*/;

            }else if($user->hasRole('ROLE_GROWER')){
                 $query
                ->leftJoin($alias.'.cropblockProductiondepartement','pd')
                ->andWhere($query->expr()->in('pd.ProductionDepartementUser',":id"))
                ->setParameter('id', $user->getId())
                /*->orderBy($alias . '.cropblockProductiondepartement', 'DESC')*/;
            }
            else{
                $query
                ->select($alias)
               /* ->orderBy($alias . '.cropblockProductiondepartement', 'DESC')*/;
            }

            return $query;
            
        }
    protected function configureFormFields(FormMapper $formMapper)
    {   
            $user = $this->getCurrentUser();
            $em = $this->getEM();
             $now  = new \DateTime();
         if($user->hasRole('ROLE_OWNER_FARM'))
             {
                $query = $em->createQueryBuilder('Farm')
                    ->select('Farm')
                    ->from('AppBundle:Farms', 'Farm')
                    ->where('Farm.farmUser = :user')
                    ->setParameter('user', $user);
                $queryCrop = $em->createQueryBuilder('crop')
                                ->select('crop')
                                ->from('AppBundle:Crop','crop')
                                ->join('AppBundle:ConfigCropFarm','ccf',Join::WITH, 'crop.id = ccf.CropId')
                                ->where('ccf.farmId =  :fId')
                                ->andWhere('crop.enabled=1')
                                ->setParameter('fId',$user->getuserFarm());
                      
             }
             else if($user->hasRole('ROLE_GROWER'))
             {
                $query = $em->createQueryBuilder('Farm')
                    ->select('Farm')
                    ->from('AppBundle:Farms', 'Farm')
                    ->leftJoin('Farm.farmProductiondepartement','pd')
                    ->where('pd.ProductionDepartementUser = :user')
                    ->setParameter('user',$user);
                   
                $ownerFarm = $em->getRepository('AppBundle:User')->find(array('id' =>$user->getManagerId()));
                $queryCrop = $em->createQueryBuilder('crop')
                            ->select('crop')
                            ->from('AppBundle:Crop','crop')
                            ->join('AppBundle:ConfigCropFarm','ccf',Join::WITH, 'crop.id = ccf.CropId')
                            ->where('ccf.farmId =  :fId')
                            ->andWhere('crop.enabled=1')
                            ->setParameter('fId',$ownerFarm->getuserFarm());
                   
             }
             else
              {
                 $query = $em->createQueryBuilder('Farm')
                    ->select('Farm')
                    ->from('AppBundle:Farms','Farm')
                    ->orderBy('Farm.createdAt','ASC');
                  $queryCrop = $em->createQueryBuilder('crop')
                                    ->select('crop')
                                    ->from('AppBundle:Crop','crop')
                                    ->where('crop.enabled=1');
                                   
              }
             $disabled = $this->subject->getId() ? true : false;
             $hidden = $this->subject->getId() ? 'hidden' : 'null';
            
        $formMapper
        ->tab(' ')
                ->with('Crop Block',array('class' => 'col-lg-4'))
                     ->add('name',null,array('attr' => ['required' => true,'placeholder' => 'Name'] ))
                     ->add('Farms', 'entity', array(
                                      'class' => 'AppBundle\Entity\Farms',
                                       'attr' => ['class' => 'farms-departement-cordonner','hidden' => true],
                                      'label'=> "Farms:",
                                      'empty_value' => 'Choose a Farms',   
                                      'query_builder' => $query,
          
                                          'multiple' => false,
                                          'expanded' => false,
                                          'required' => false,
                                          'mapped' => false,
                                         
                            ))
                     ->add('cropblockProductiondepartement', 'entity', [
                                            'label' => 'Production Departement',
                                            'required' => true,
                                            'class' => 'AppBundle\Entity\ProductionDepartement',
                                            'empty_value' => 'Choose a Production Departement',   
                                            'attr' => ['class' => 'Production-departement-cordonner'],                      
                                        ])
                  

                       ->add('Crop', 'entity', [
                                            'label' => 'Crop',
                                            'required' => true,
                                            'class' => 'AppBundle\Entity\Crop',
                                            'empty_value' => 'Choose a Crop',   
                                            'attr' => ['class' => 'cropBlockClone'],
                                            'query_builder' => $queryCrop                    
                                        ])

                       ->add('cloneAndChange','hidden',array('mapped' => false,'attr' => ['class' =>'cloneAndChange']))
                       ->add('startDate','sonata_type_datetime_picker',array('label' => 'Start Date','required' => true,'attr' => ['class' =>'StartDateMyDate'],'dp_side_by_side' => false,'dp_use_current' => true, 'dp_use_seconds' => false,'format' => 'y-MM-d'))
                       ->add('endDate','sonata_type_datetime_picker',array('label' => 'End Date ','attr' => ['class' => ' btn-toggle-info'],'required' => true,'dp_side_by_side' => false,'dp_use_current' => true, 'dp_use_seconds' => false, 'format' => 'y-MM-d'))

                            ->add('polygoninformation','textarea',array('label' => 'polygon Crop info','attr'=>array("class" => "polygoninformation hideToggle",'required' => true)))
                            ->add('positionlat',null,array('label' => 'Position crop block latitude','attr'=>array("class" => "positionlat hideToggle" )))
                            ->add('positionlang',null,array('label' => 'Position crop block longitude','attr'=>array("class" => "positionlang hideToggle")))
                            ->add('positionzoom',null,array('label' => 'Position crop block zoom','attr'=>array("class" => "positionzoom hideToggle")))
                            ->add('area',null,array('label' => 'Area (m²)','attr'=>['class' => 'area hideToggle']))
                            ->add('centerOfPolyLat','hidden',['attr'=>["class" => "centerOfPolyLat"]])
                            ->add('centerOfPolyLang','hidden',['attr' =>["class" => "centerOfPolyLang"]])

                       
                  ->end()
                  
                 ->with('Map',array('class' => 'col-lg-8 MapMap'))
                 ->end()
               
            ->end();
    }
    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
             ->with('Details',array('class' => 'col-lg-5'))
                ->add('name ')
                 ->add('cropblockProductiondepartement',null,array('label' => 'Production Departement'))
                 ->add('Crop')
                 ->add('area',null,array('label' => 'Area (m²)'))
                ->add('startDate')
                ->add('endDate')
            ->end()
            ->with('Map',array('class' => 'col-lg-7'))
                     ->add('polygoninformation','string',['Farm' =>'Border of the farm','template' => 'Admin/cropblock_map_show.html.twig', 
                              
                             ])
             ->end();
      
    }
}