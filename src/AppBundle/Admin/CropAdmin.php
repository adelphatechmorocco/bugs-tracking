<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\CoreBundle\Validator\ErrorElement;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\File\File;


class CropAdmin extends AppBaseAdmin
{
    public function prePersist($object){
       $Enabled = !$user->hasRole("ROLE_GLOBAL_ADMIN") ? 0 : 1;
        $object->setEnabled($Enabled);
       
    }
     protected function configureRoutes(RouteCollection $collection)
      {
          $collection->add('add_conf_to_farm','{id}/AddConfigCrop');
          $collection->add('remove_conf_to_farm','{id}/removeConfigCrop');
          $collection->add('check_list_of_crop');
      }
     public function createQuery($context='list') 
    {
      $query = parent::createQuery($context);
      $alias = $query->getRootAliases()[0];
      $user = $this->getCurrentUser();
      if($user->hasRole('ROLE_GLOBAL_ADMIN'))
      {
         $query
             ->select($alias);
            
      }else{
         $query
             ->select($alias)
             ->where($alias.'.enabled = 1');
           
      }
              
        return $query;
    } 

      public function getBatchActions()
        {
            $actions = parent::getBatchActions();
            unset($actions['delete']);

            return $actions;
        }
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
         $em = $this->getEM();
         $query = $em->createQueryBuilder()
                   ->select('Crop')
                   ->from('AppBundle:CropType','Crop')
                   ->where('Crop.enabled=1');
          $user = $this->getCurrentUser();
         $datagridMapper
            ->add('name')
            ->add('cropCorptype',null,array('label' => 'Crop Type'),null,array('query_builder' => $query));
             if($user->hasRole('ROLE_GLOBAL_ADMIN'))
             {
                $datagridMapper
                ->add('enabled');
             }
       
    }
    
    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $FieldOptions = array('required' => false,'data_class' => null,'label'=> 'Image','mapped' =>false);
           $user = $this->getCurrentUser();
        $listMapper
            ->addIdentifier('name')
            ->add('cropCorptype',null,['label' => 'Crop Type'])
            ->add('description', null, array(
            'header_style' => 'width: 35%; text-align: center',
            'row_align' => 'left'
            ))
            ->add('pathFile','string',['label'=>'Image','template' => 'Admin/image.html.twig', 
            'header_style' => 'width: 15%; text-align: center',
            'row_align' => 'center'
            ]);
              if($user->hasRole('ROLE_GLOBAL_ADMIN'))
               {

                  $listMapper
                     ->add('enabled',null,array('editable' => true));
                }
            $listMapper
            ->add('_action', 'actions', [
                'actions' => [
                  'addConf' => [
                      'template' => 'Admin/Button/list_add_config_crop.html.twig'
                    ],
                    'show' => [
                        'template' => 'Admin/Button/list__action_show.html.twig',
                    ],
                    'edit' => [
                        'template' => 'Admin/Button/list__action_edit.html.twig',
                    ],
                    'delete' => [
                        'template' => 'Admin/Button/list__action_delete.html.twig',
                    ],
                ]
            ])
        ;
    }
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {   
        $check = $this->subject->getId() ? true : false;

        $image = $this->getSubject();
        $fileFieldOptions = array('required' => false,'data_class' => null,'label'=> 'Image');
        $webPath = $image->getPath() ? $image->getPath() : "default.png" ;
        $urlImages = "/images/bugs";
        $fullPath = $urlImages.'/'.$webPath;
        $fileFieldOptions['help'] = '<img id="image-path" src="'.$fullPath.'" class="admin-preview" />';
  

        $formMapper
            ->with('General',array('class' => 'col-lg-7'))
              ->add('name')
              ->add('cropCorptype','entity',[
                            'label' => 'Crop Type',
                            'required' => true,
                            'class' => 'AppBundle\Entity\CropType',
                            'empty_value'=> 'Choose a Crop Type',
                            'query_builder' => function (EntityRepository $er) {
                              return $er
                                ->createQueryBuilder('u')
                                ->where("u.enabled=1")
                                ;
                             },                       
                    ])
             
               ->add('pathFile', 'file', $fileFieldOptions)
              
                    /*->add('pathFile','file',['label'=> 'Image','required' => false]);*/
               ->add('description','textarea', ['required' => false ,
                    'attr' => ['cols' => '5', 'rows' => '4','placeholder' => 'Description']
                      ]);

                if(!$check)
                {
                 $formMapper
                 ->add('enabled','hidden',array());
                }else{
                   $formMapper
                 ->add('enabled');
                }
           $formMapper
            ->end();

        
    }
    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Details',array('class' => 'col-lg-7'))
                  ->add('name')
                  ->add('cropCorptype',null,['label' => 'Crop Type'])
                  ->add('description')
                  ->add('pathFile','string',['label'=>'Image','template' => 'Admin/image.html.twig' ])
                ->end();
      
    }
}