<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityRepository;



class RecordingBugsAdmin extends AppBaseAdmin
{

    protected $parentAssociationMapping = 'recordingbugsRecording';
     public function getBatchActions()
        {
            $actions = parent::getBatchActions();
            unset($actions['delete']);

            return $actions;
        }
    protected function configureFormFields(FormMapper $formMapper)
    {   
        $formMapper
                    ->add('recordingbugsBugs', 'entity', [
                                            'label' => 'Bugs',
                                            'required' => true,
                                            'class' => 'AppBundle\Entity\Bug',
                                             'attr' => ['class' => 'get-bugs-recording'],
                                            'empty_value' => 'Choose a bug', 
                                            'query_builder' => function (EntityRepository $er) {
                                             return $er
                                                ->createQueryBuilder('u')
                                                ->orderBy('u.priority', 'ASC')
                                                ;
                                              },
                                                             
                                        ])
                    ->add('amount','text',array('label' =>' Amount'));

        ;
    }
     protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
         $datagridMapper
            ->add('recordingbugsBugs' ,null ,array('label' => 'Bug'));
       
    }
    
    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('recordingbugsBugs',null,array('label' => 'Bug'))
            ->add('amount')
            ->add('_action', 'actions', [
                'actions' => [
                    'show' => [
                        'template' => 'Admin/Button/list__action_show.html.twig',
                    ],
                    'edit' => [
                        'template' => 'Admin/Button/list__action_edit.html.twig',
                    ],
                    'delete' => [
                        'template' => 'Admin/Button/list__action_delete.html.twig',
                    ],
                ]
            ])
        ;
    }
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General',array('class' => 'col-lg-5'))
                ->add('recordingbugsBugs','null',array('label' => 'Bug'))
                ->add('amount')
              ->end();
    }
  
}