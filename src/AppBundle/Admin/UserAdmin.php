<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\CoreBundle\Validator\ErrorElement;




class UserAdmin extends AppBaseAdmin
{

/**
 * @param DatagridMapper $datagridMapper
 */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
         $user = $this->getCurrentUser();
        $datagridMapper
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add('enabled');
            if($user->hasRole('ROLE_OWNER_FARM'))
            {
                 $datagridMapper
                ->add('roles',null, array(), 'choice' , array('choices' => array('grower' => 'Grower','Operator' =>'Operator')));
            }
            if($user->hasRole('ROLE_GLOBAL_ADMIN')){
                 $datagridMapper
                 ->add('roles',null, array(), 'choice' , array('choices' => array('owner' =>'Owner','Grower' => 'grower','Operator' => 'Operator')));
                
            }
        
    }
     /* public function getBatchActions()
        {
            $actions = parent::getBatchActions();
            unset($actions['delete']);

            return $actions;
        }*/
     public function prePersist($object){

         $object->setManagerId($this->getCurrentUser()->getId());
         $object->setEnabled(1);
   
    }

  
     public function preUpdate($object){

             $em = $this->getEM();
             $repository = $em->getRepository('AppBundle:User')->find($object->getId());
             $Password = $object->getPlainPassword();
            
            if (!empty($Password)) {
                $salt = $object->getSalt();
                $encoderservice = $this->getConfigurationPool()->getContainer()->get('security.encoder_factory');
                $encoder = $encoderservice->getEncoder($object);
                $encoded_pass = $encoder->encodePassword($Password, $salt);
                $object->setPassword($encoded_pass);

            } else {
                $object->setPassword($repository->getPassword());
            }
        }
       
       public function createQuery($context = 'list')
        {
            $query = parent::createQuery($context);
            $alias = $query->getRootAliases()[0];
            $user = $this->getCurrentUser();
            $em = $this->getEM();
            if($user->hasRole('ROLE_OWNER_FARM'))
            {
                $AllGrowers = $em->getRepository('AppBundle:User')->findBy(array('managerId' => $user->getId()));
                $listUser = [];
                foreach ($AllGrowers as $Grower) {
                     $listUser[]=(int)$Grower->getId();
                }
                $listUser[]= (int)$user->getId();  
                $managerList = array_unique($listUser); 

                 $query 
                ->andWhere($query->expr()->in($alias.'.managerId',":id"))
                /*->orderBy($alias . '.createdAt', 'DESC')*/
                ->setParameter('id', $managerList);

            }
            else if($user->hasRole('ROLE_GROWER')){
                  $query
                    ->andWhere(
                        $query->expr()->eq($alias . '.managerId', ':managerId')
                    )
                    /*->orderBy($alias . '.createdAt', 'DESC')*/
                    ->setParameter('managerId',$user->getId());
            }
           else{
                 $query
                 ->select($alias);
                /* ->orderBy($alias . '.createdAt', 'DESC');*/
            }
            return $query;
            
       }

    
    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $user = $this->getCurrentuser();
        $listMapper
            ->addIdentifier('firstname')
            ->add('lastname')
            ->add('email')
            ->add('phone')
            ->add('lastLogin')
            ->add('enabled',null,array('editable' => true));
             if($user->hasRole('ROLE_GLOBAL_ADMIN') || $user->hasRole('ROLE_OWNER_FARM') )
             {
                $listMapper
                 ->add('rolesAsString',null,array('label' => 'Roles'));
             }
               $listMapper
            ->add('_action', 'actions', [
                'actions' => [
                    'show' => [
                        'template' => 'Admin/Button/list__action_show.html.twig',
                    ],
                    'edit' => [
                        'template' => 'Admin/Button/list__action_edit.html.twig',
                    ],
                    'delete' => [
                        'template' => 'Admin/Button/list__action_delete.html.twig',
                    ],
                ]
            ])
        ;
    }
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $user = $this->getCurrentuser();
        $passwordRequired = (preg_match('/_edit$/', $this->getRequest()->get('_route'))) ? false : true;
        $subjectId = $this->subject->getId();
        $em = $this->getEm();
        $roleUpd = $this->subject->getRoles()[0];
          
        if($user->hasRole("ROLE_OWNER_FARM")){
                $queryDept = $em->createQueryBuilder('dpt')
                ->select('dpt')
                ->from('AppBundle:ProductionDepartement','dpt')
                ->leftJoin('dpt.productiondepartementFarm','farm')
                ->Where('farm.farmUser=:usr')
                ->setParameter('usr',$user);
        }else if($user->hasRole("ROLE_GROWER")){
             $queryDept = $em->createQueryBuilder('dpt')
                ->select('dpt')
                ->from('AppBundle:ProductionDepartement','dpt')
                ->Where('dpt.ProductionDepartementUser=:usr')
                ->setParameter('usr',$user);
        }else if($user->hasRole("ROLE_GLOBAL_ADMIN")){
               $queryDept = $em->createQueryBuilder('dpt')
                ->select('dpt')
                ->from('AppBundle:ProductionDepartement','dpt');
                 $queryFarms = $em->createQueryBuilder('fr')
                                 ->select('fr')
                                 ->from('AppBundle:Farms','fr')
                                 ->orderBy('fr.id','ASC');
        }
        $formMapper
            ->with('Users', array('class' => 'col-lg-7'))
            ->add('firstname')
            ->add('lastname')
            ->add('phone', null, array('required' => false))
            ->add('email');
           if($user->hasRole('ROLE_GLOBAL_ADMIN'))
           {
             $formMapper
                 ->add('roles', 'choice', array(
                    'choices' => array(
                        'ROLE_GLOBAL_ADMIN' => 'Global admin',
                        'ROLE_OWNER_FARM' => 'Owner farm',
                        'ROLE_GROWER' => 'Grower',
                        'ROLE_OPERATOR' => 'Operator',
                        'ROLE_TECHNICIEN' => 'Viewer'
                    ),
                    'multiple' => true
            
                ))
                ->add('enabled');      
           }
           elseif($user->hasRole('ROLE_OWNER_FARM') && !isset($subjectId))
           {
             $formMapper
                 ->add('roles', 'choice', array(
                    'choices' => array(
                        'ROLE_GROWER' => 'Grower',
                        'ROLE_OPERATOR' => 'Operator',
                    ),
                    'multiple' => true
            
                ))
                ->add('enabled');      
             } elseif ($user->hasRole('ROLE_OWNER_FARM') && isset($subjectId) && $subjectId != $user->getId()) {
                  $formMapper
                 ->add('roles', 'choice', array(
                    'choices' => array(
                        'ROLE_GROWER' => 'Grower',
                        'ROLE_OPERATOR' => 'Operator',
                    ),
                    'multiple' => true
            
                ))
                ->add('enabled');      
             }
             elseif($user->hasRole('ROLE_GROWER' ) &&  !isset($subjectId)){
                    $formMapper
                     ->add('roles', 'choice', array(
                    'choices' => array(
                        'ROLE_OPERATOR' => 'Operator',
                    ),
                    'multiple' => true
            
                 ))
                ->add('enabled');      
             }
             elseif ($user->hasRole('ROLE_GROWER') && isset($subjectId) && $subjectId != $user->getId()) {
                $formMapper
                     ->add('roles', 'choice', array(
                    'choices' => array(
                        'ROLE_OPERATOR' => 'Operator',
                    ),
                    'multiple' => true
            
                 ))
                ->add('enabled');       
             }      
           $formMapper
              ->add('plainPassword', 'repeated', [
                        'type' => 'password',
                        'invalid_message' => 'The password fields must match.',
                        'required' => $passwordRequired,
                        'first_options'  => ['label' => 'Password'],
                        'second_options' => ['label' => 'Repeat Password'],
                    ])
            ->end();
            if(!$user->hasRole('ROLE_GLOBAL_ADMIN'))
            {
                  if(!$user->hasRole('ROLE_OPERATOR')  && isset($subjectId) && $subjectId != $user->getId() && $roleUpd ==="ROLE_OPERATOR")
                   {
                     $formMapper
                    ->with('Production Deparetement To Operator', array('class' => 'col-lg-5'))
                      ->add("operatorProductionDepartement", null, array('label' => 'Operator', 'expanded' => true, 'by_reference' => false, 'multiple' => true,'query_builder' => $queryDept))
                    ->end();
                    }
                  else if(!isset($subjectId)&& !$user->hasRole('ROLE_OPERATOR')){
                    $formMapper
                        ->with('Production Deparetement To Operator', array('class' => 'col-lg-5'))
                          ->add("operatorProductionDepartement", null, array('label' => 'Operator', 'expanded' => true, 'by_reference' => false, 'multiple' => true,
                                                                                                                                            'query_builder' => $queryDept))
                       ->end();
                }
               ;

            }else{
                
                     $formMapper
                    ->with('Add Viewer to Farms', array('class' => 'col-lg-5'))
                      ->add("technicienFarms", null, array('label' => 'Farms', 'expanded' => true, 'by_reference' => false, 'multiple' => true,'query_builder' => $queryFarms))
                    ->end();
                  

            }
          
        
    }
    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
       $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $showMapper
            ->with('details',array('class' => 'col-lg-7'))
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add ('phone');

             if (!$user->hasRole('')) {
             $showMapper
                ->add('enabled')
                ->add('lastLogin')
                ->add('rolesAsString',null,array('label' => 'Roles'));
                 }
        $showMapper
            ->end();

        ;
    }
}