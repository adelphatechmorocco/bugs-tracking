<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Farms;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\HttpFoundation\RedirectResponse;



class FarmsAdmin extends AppBaseAdmin
{
         public function getBatchActions()
        {
            $actions = parent::getBatchActions();
            unset($actions['delete']);

            return $actions;
        }
        public function prePersist($object){
            
            if(!$object->getFarmUser())
            {
                $object->setFarmUser($this->getCurrentUser()); 
            }
        }
        

    /**
     * {@inheritdoc}
     */
    public function validate(ErrorElement $errorElement, $object=null)
    {
         $user = $this->getCurrentUser();
         $em = $this->getEM();
         $farm = $em->getRepository('AppBundle:Farms')->findByfarmUser($user->getId());
        if(!$object->getId())
        {
         if(!empty($farm[0]))
         {
             
            $errorElement
            ->with('name')
            ->addViolation('Recall,You have no right to create more than one farm')
            ->end();
         }
     }
    }
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {    
          $Role = "ROLE_OWNER_FARM";
          $em = $this->getEM();
          $user= $this->getCurrentUser();
          $query = $em->createQueryBuilder('user')
                     ->select('user')
                     ->from('AppBundle:User', 'user')
                     ->where('user.roles LIKE :owner')
                     ->andWhere('user.enabled=1')
                     ->setParameter('owner','%'.$Role.'%');
          $datagridMapper
            ->add('farmName');
            if($user->hasRole('ROLE_GLOBAL_ADMIN'))
            {
                $datagridMapper
                ->add('country')
                ->add('city')
                ->add('farmUser', null, array(), null, array('required'=> false,
                    'query_builder' => $query ));
          
            }
            
       
    }
   public function createQuery($context = 'list')
    {
      
        $query = parent::createQuery($context);
        $alias=$query->getRootAliases()[0];
        $user = $this->getCurrentUser();
     
        if ($user->hasRole('ROLE_OWNER_FARM')) {
            $query
                ->andWhere(
                    $query->expr()->eq($alias . '.farmUser', ':OwnerId')
                )
                /*->orderBy($alias . '.createdAt', 'DESC')*/
                ->setParameter('OwnerId',$this->getCurrentUser());
        }else{
            $query
                ->select($alias)
                /*->orderBy($alias . '.createdAt', 'DESC')*/;
            
        }
        return $query;
    }
    
    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        
        $listMapper
            ->add('_action', 'actions', [
                'actions' => [
                    'farmProductiondepartement' => [
                                    'template'=> 'Admin/production_departement.html.twig', 
                    ],
                    /*'show' => [
                        'template' => 'Admin/Button/list__action_show.html.twig',
                    ],
                    'edit' => [
                        'template' => 'Admin/Button/list__action_edit.html.twig',
                    ],
                    'delete' => [
                        'template' => 'Admin/Button/list__action_delete.html.twig',
                    ],*/
                ]
            ])
            ->addIdentifier('farmName')
            ->add('farmUser', null, array('label' => 'Owner email',
                    'sortable' => true,
                    'sort_field_mapping'=> array('fieldName'=>'id'),
                    'sort_parent_association_mappings' => array(array('fieldName'=>'farmUser'))
                    ))
           /* ->add('address1',null,array('label' => 'First Address'))
            ->add('phone1',null,array('label' => 'First Phone'))
            ->add('city')
            ->add('postalCode')
            ->add('country')*/
            
      
        ;
    }
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {   

        $user = $this->getCurrentUser();
        $Role = "ROLE_OWNER_FARM";
        $em = $this->getEM();
        $check = $this->subject->getId() ? true : false;
      
         $farm = $em->getRepository('AppBundle:Farms')->findByfarmUser($user->getId());
         if(!$check)
         {
             if(!empty($farm[0]))
             {
                  $this->getRequest()->getSession()->getFlashBag()->add("sonata_flash_error", "Recall,You have no right to create more than one farm");
                  $redirection = new RedirectResponse($this->getConfigurationPool()->getContainer()->get('router')->generate('admin_app_farms_list'));
                  $redirection->send();
             }
         }
        /*
        $query = $em->createQueryBuilder('user')
                     ->select('user')
                     ->from('AppBundle:User', 'user')
                     ->leftJoin('user.userFarm','farm')
                     ->where('user.roles LIKE :owner')
                     ->andWhere('user.enabled=1')
                     ->andWhere('farm.farmUser IS NULL')
                     ->setParameter('owner','%'.$Role.'%');*/
                       
        $query = $em->createQueryBuilder('user')
                     ->select('user')
                     ->from('AppBundle:User', 'user')
                     ->where('user.roles LIKE :owner')
                     ->andWhere('user.enabled=1')
                     ->setParameter('owner','%'.$Role.'%');
              
              
    
        $formMapper
        ->tab(' ')
               ->with('Farm',array('class' => 'col-lg-4'));
              
                if (!$user->hasRole('ROLE_GLOBAL_ADMIN')) {
                    $formMapper
                         ->add('farmUser', 'entity', [
                                'required' => false,
                                'class' => 'AppBundle\Entity\user', 
                                'attr' =>['class' => 'hiddenUser'],
                                'query_builder' => function (EntityRepository $er) use ($user) {
                                  return $er
                                    ->createQueryBuilder('u')
                                    ->where('u.id ='. $user->getId())
                                    ;
                                 },                       
                            ]);
                }else{
                     $formMapper
                         ->add('farmUser', 'sonata_type_model',array(
                                'class' => 'AppBundle:User',
                                'required' => true,
                                'query' => $query,
                                 'attr' =>['class' => 'btn-add-new-swtich'],
                                'empty_value' => 'Choose an Owner',  
                                                ));
                }
            
             $formMapper
                   ->add('farmName',null,array('label' => 'Farm Name'))
                   ->add('address1','textarea', [ 'label' => 'First address ',
                        'attr' => ['cols' => '5', 'rows' => '4','placeholder' => 'First address']
                          ])
                      ->add('address2','textarea', [ 'required'=> false,'label' => 'Second address ',
                                'attr' => ['cols' => '5', 'rows' => '4','placeholder' => 'Second address',]
                                  ])
                     ->add('city',null,array('label' => 'City' ,'attr'=>[ 'class' => 'citysAuto']))
                     ->add('postalCode',null,array('label' => 'Postal Code'))
                      ->add('country',null,array('label' => 'Country'))


                     ->add('phone1',null,array('label' => 'First Phone'))
                     ->add('phone2',null,array('label' => 'Second Phone'))  
                     ->add('email',null,array('label' => 'Email' ,'attr'=>['class' => 'btn-toggle-info']))
                    
                     ->add('polygoninformation','textarea',array('label' => 'polygon farm info','attr'=>array("class" => "polygoninformation hideToggle",'required' => true)))
                     ->add('positionlat',null,array('label' => 'Position farm latitude','attr'=>array("class" => "positionlat hideToggle" )))
                     ->add('positionlang',null,array('label' => 'Position farm longitude','attr'=>array("class" => "positionlang hideToggle")))
                     ->add('positionzoom',null,array('label' => 'Position farm zoom','attr'=>array("class" => "positionzoom hideToggle")))

                    ->end()
                       
                    ->with('Map',array('class' => 'col-lg-8 MapMap'))
                    ->end()
                       
                 ->end();
    }
    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('General',array('class' => 'col-lg-5'))
                 ->add('farmUser',null,array('label' => 'Owner farm'))
                 ->add('farmName')
                 ->add('email',null,array('label' => 'Email' ,'attr' =>[ 'placeholder' => 'Email']))
                 ->add('country',null,array('label' => 'Country'))
                 ->add('city',null,array('label' => 'City' ))
                 ->add('postalCode',null,array('label' => 'Postal Code'))   
                 ->add('address1',null,array('label' => 'First Adress'))
                 ->add('address2',null,array('label' => 'Scond Address'))
                 ->add('phone1',null,array('label' => 'First Phone'))
                 ->add('phone2',null,array('label' => 'Second Phone'))  
                 ->end()
                 ->with('Map',array('class' => 'col-lg-7'))
                     ->add('positionzoom','string',['Farm' =>'Border of the farm','template' => 'Admin/farm_map_show.html.twig', 
                              
                             ])
                 ->end();
    }
}