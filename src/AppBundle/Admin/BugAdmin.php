<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityRepository;



class BugAdmin extends AppBaseAdmin
{
     protected $datagridValues = array(
          '_page' => 1,
          '_sort_order' => 'ASC',
          '_sort_by' => 'enabled',
      );

       public function prePersist($object){
          $user = $this->getCurrentUser();
          $Enabled = !$user->hasRole("ROLE_GLOBAL_ADMIN") ? 0 : 1;
          $object->setNameType($object->getBugBugType()->getBugType());
          $object->setEnabled($Enabled);
        }
    
        public function getBatchActions()
        {
            $actions = parent::getBatchActions();
            unset($actions['delete']);

            return $actions;
        }

        protected function configureRoutes(RouteCollection $collection)
        {
            $collection->add('add_conf_to_farm','{id}/AddConfigBug');
            $collection->add('remove_conf_to_farm','{id}/removeConfigBug');
            $collection->add('check_list_of_bug');
        }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
       $em = $this->getEM();
       $query = $em->createQueryBuilder()
                   ->select('bug')
                   ->from('AppBundle:bugType','bug')
                   ->where('bug.enabled=1');
                   $user = $this->getCurrentUser();

         $datagridMapper
            ->add('name')
            ->add('bugBugType' ,null,array("label" => "Bug Type"),null,array('query_builder' => $query));
             if($user->hasRole('ROLE_GLOBAL_ADMIN'))
             {
               $datagridMapper
                   ->add('enabled')  ;
              }

          
       
    }
     public function createQuery($context='list') 
      {
        $query = parent::createQuery($context);
        $alias = $query->getRootAliases()[0];
        $user = $this->getCurrentUser();
        if($user->hasRole('ROLE_GLOBAL_ADMIN'))
        {
           $query
               ->select($alias)
               ->orderBy($alias . '.bugBugType', 'ASC');
        }else{
           $query
               ->select($alias)
               ->where($alias.'.enabled = 1')
               ->orderBy($alias . '.bugBugType', 'ASC');
        }
                
          return $query;
      } 
  
    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    { 
      $em = $this->getEM();
      $query = $em->getRepository('AppBundle:BugType')->findByEnabled(1);
      $user = $this->getCurrentUser();
      
        $listMapper
            ->addIdentifier('name')
            ->add('bugBugType','entity',array('label' => 'Bug Type',
                'query' => $query))
          
            ->add('description',null, array(
                'header_style' => 'width: 35%; text-align: center',
                'row_align' => 'left'
                ))
             ->add('pathFile','string',['label'=>'Image','template' => 'Admin/image.html.twig', 
              'header_style' => 'width: 15%; text-align: center',
              'row_align' => 'center'
              ]);
             if($user->hasRole('ROLE_GLOBAL_ADMIN'))
             {
               $listMapper
                   ->add('enabled',null,array('editable' => true));

             }
             
          $listMapper
            ->add('_action', 'actions', [
                'actions' => [
                  'addconf' => [
                        'template' => 'Admin/Button/list_add_config_bug.html.twig',
                    ],
                  
                    'show' => [
                        'template' => 'Admin/Button/list__action_show.html.twig',
                    ],
                    'edit' => [
                        'template' => 'Admin/Button/list__action_edit.html.twig',
                    ],
                    'delete' => [
                        'template' => 'Admin/Button/list__action_delete.html.twig',
                    ],
                    
                ]
            ])
        ;
    }
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    { 
        $required = $this->subject->getId() ? false : true;
        $image = $this->getSubject();
        $fileFieldOptions = array('required' => false,'data_class' => null,'label'=> 'Image');
        $webPath = $image->getPath() ? $image->getPath() : "default.png" ;
        $urlImages = "/images/bugs";
        $fullPath = $urlImages.'/'.$webPath;
        $fileFieldOptions['help'] = '<img id="image-path" src="'.$fullPath.'" class="admin-preview" />';
        $formMapper
            ->with('Bug',array('class' => 'col-lg-7'))
                  ->add('bugBugType', 'entity', [
                                        'label' => 'Bug Type',
                                        'required' => true,
                                        'class' => 'AppBundle\Entity\BugType',
                                        'empty_value' => 'Choose a Type',
                                        'query_builder' => function(EntityRepository $er){
                                                  return $er
                                                       ->createQueryBuilder('u')
                                                       ->where("u.enabled=1");
                                        }
                                       
                    ])
                  ->add('priority')
                  ->add('name',null,array('attr' => ['required' => true,'placeholder' => 'Name'] ))
                  ->add('pathFile', 'file', $fileFieldOptions)
                  ->add('description','textarea', [ 'required'=>false,
                      'attr' => ['cols' => '5', 'rows' => '10','placeholder' => 'Bug Description']
                        ]);
                  if($required)
                  {
                   $formMapper
                   ->add('enabled','hidden',array());
                  }else{
                     $formMapper
                   ->add('enabled');
                  }
                   $formMapper
            ->end();
    }
    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Details',array('class' => 'col-lg-7'))
                ->add('name ')
                 ->add('priority')
                ->add('bugBugType',null,array('label' => 'Bug Type')) 
                ->add('description')

                ->add('pathFile','string',['label'=>'Image','template' => 'Admin/image.html.twig' ])
            ->end();
      
    }
}