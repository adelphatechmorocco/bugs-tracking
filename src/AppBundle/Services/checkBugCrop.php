<?php

namespace AppBundle\Services;

use Symfony\Component\Security\Core\SecurityContextInterface;
use AppBundle\Entity\Reporting;
use AppBundle\Entity\AmountPositionCard;


class checkBugCrop 
{

		private $em ;
		private $securityContext;


		public function __construct($securityContext,$entityManager)
		{
			$this->em = $entityManager;
			$this->securityContext = $securityContext;

		}
		public function countBugCropBlock($recordsLists)
		{
	       $listResults   = [];
	      
		   foreach ($recordsLists as  $recordList) {
		   			foreach ($recordList as  $List) {
		   				
		   					if(isset($listResults[$List['CropBlockId']."-".$List['bugId']])){
   						    	$CardInfo =array('amount' => $List['amount'],'cardLat' => $List['cardLat'],
										     'cardLong' => $List['cardLong'],'cardId' => $List['cardId']);
								$listResults[$List['CropBlockId']."-".$List['bugId']]['card-info'][] = $CardInfo;
		   						$listResults[$List['CropBlockId']."-".$List['bugId']]['totalamountbugs']   += $List['amount'];
		   						$listResults[$List['CropBlockId']."-".$List['bugId']]['bug']                = $List['bugId'];
		   						$listResults[$List['CropBlockId']."-".$List['bugId']]['cropBlock']          = $List['CropBlockId'];
		   						$listResults[$List['CropBlockId']."-".$List['bugId']]['numbreBug']         += 1;

		   					}else{
		   						$CardInfo =array('amount' => $List['amount'],'cardLat' => $List['cardLat'],
												     'cardLong' => $List['cardLong'],'cardId' => $List['cardId']);
		   						$listResults[$List['CropBlockId']."-".$List['bugId']]['card-info'][] = $CardInfo;
		   						$listResults[$List['CropBlockId']."-".$List['bugId']]['totalamountbugs'] = $List['amount'];
		   						$listResults[$List['CropBlockId']."-".$List['bugId']]['bug']             = $List['bugId'];
		   						$listResults[$List['CropBlockId']."-".$List['bugId']]['cropBlock']       = $List['CropBlockId'];
		   						$listResults[$List['CropBlockId']."-".$List['bugId']]['numbreBug']       = 1;
		   					}
		   			}
		   }
		 

		 
		   return $listResults;
	      
		}
		public function listerAndExec($list,$date)
		{
			  $listReporting = [];
		      $MainList = $this->countBugCropBlock($list);
		      $l=1;
		      foreach ($MainList as $lis) {
		      			$listReporting[$l]['moyenneAmount']           =  $this->moyenneBugCropBlock($lis['totalamountbugs'],$lis['numbreBug']);
		      			$listReporting[$l]['cropBlockId']             =  intval($lis['cropBlock']);
		      			$listReporting[$l]['bugId']                   =  intval($lis['bug']);

		         		$ListAll                                      =  $this->detectedDangerous($listReporting[$l]['bugId'],$listReporting[$l]['cropBlockId'],$listReporting[$l]['moyenneAmount']);
		      		
		      			if($ListAll['keyDanger'] != null)
		      			{	
			      			$Reporting = new Reporting();
			      			$Reporting->setMoyenneamount($listReporting[$l]['moyenneAmount']);  
							$Reporting->setCropBlockId($listReporting[$l]['cropBlockId']);
							$Reporting->setBugId($listReporting[$l]['bugId']);
							$Reporting->setBugName($ListAll['bugName']);
							$Reporting->setCropId($ListAll['crop']['id']);
							$Reporting->setCropName($ListAll['crop']['name']); 
							$Reporting->setKeyIntervalDanger($ListAll['keyDanger']);
							$Reporting->setDangerMin($ListAll['danger']['min']);
							$Reporting->setDangerMax($ListAll['danger']['max']);
							$Reporting->setListChekDanger($ListAll['listcheckDenger']); 
							$Reporting->setProdDeptId($ListAll['productionDept']['id']);
							$Reporting->setProdDeptName($ListAll['productionDept']['name']);
							$Reporting->setCropBlockName($ListAll['cropBloc']['name']);
							$Reporting->setDateScouting($date);
							$Reporting->setVariation($this->Variation($listReporting[$l]['bugId'],$ListAll['crop']['id'],$ListAll['productionDept']['id'],$listReporting[$l]['cropBlockId'],$listReporting[$l]['moyenneAmount']));
							$Reporting->setFarmId($ListAll['farmId']);
							$Reporting->setFarmName($ListAll['farmName']);
							$this->em->persist($Reporting);

							foreach ($lis['card-info'] as $listCardAmount) {
								$amountpositionCard = new AmountPositionCard();
								$amountpositionCard->setAmount($listCardAmount['amount']);
	                            $amountpositionCard->setLatitude($listCardAmount['cardLat']);
	                            $amountpositionCard->setLongitude($listCardAmount['cardLong']); 
	                            $amountpositionCard->setCardId($listCardAmount['cardId']); 
	                            $amountpositionCard->setReporting($Reporting);
	                            $this->em->persist($amountpositionCard);	
							}
					
		      			}
		      			$l++;
		      }
		      $this->em->flush();
		}

		public function moyenneBugCropBlock($total,$divs){
			  return  round(floatval($total) / floatval($divs),2);
		}

		public function intervalDangerous($min,$max){

			$Interval = [] ;
			$fact = floatval( ($max - $min) / 4);
			$Interval[0] = $min;
			$Interval[1] = $min + $fact;
			$IntervalDangerous = [];
			for ($i=2;$i<= 4;$i++)
			{
				$Interval[$i] = $Interval[$i-1] + $fact;
			}
			for($j= 1;$j < count($Interval);$j++)
			{
					 $IntervalDangerous[$j]['min'] = $Interval[$j-1];
					 $IntervalDangerous[$j]['max'] = $Interval[$j];
			}

				return $IntervalDangerous;
		}

		public function detectedDangerous($bugId,$cropBlockId,$amount){

			     $crop = $this->em->createQueryBuilder('c')
			  						        ->select('c')
									        ->from('AppBundle:Crop','c')
									        ->join('AppBundle:CropBlock', 'co', 'WITH', 'co.Crop = c.id')
									        ->where('co.id = :CropBlocId')
									        ->setParameter('CropBlocId',$cropBlockId)
									        ->getQuery()->getResult();

			     $bug = $this->em->createQueryBuilder('bug')
										->select('bug')
										->from('AppBundle:Bug','bug')
										->where('bug.id =:id')
										->setParameter('id',$bugId)
										->getQuery()->getResult();

			      $config = $this->em->createQueryBuilder('conf')
										->select('conf')
										->from('AppBundle:BugsCropsConfig','conf')
										->where('conf.bug =:bug')
										->andWhere('conf.crop =:crop')
										->setParameters(array('bug' =>$bug ,'crop' =>$crop))
										->getQuery()->getResult();

				  $prod         = $this->FindProdutionDepartement($cropBlockId);
			      

				  $cropBlock   = $this->FindCropBlock($cropBlockId);
				 

				 

           
				
				  if(!empty($config))
				  {
				  	    $listresult =[];
				  	    $listcheckDenger = $this->intervalDangerous($config[0]->getMin(),$config[0]->getMax());
  	 	                $listresult['bugName']                    = $bug[0]->getName()  ;
              			$listresult['farmId']                     = $prod[0]->getProductiondepartementFarm()->getId();
              			$listresult['farmName']                   = $prod[0]->getProductiondepartementFarm()->getFarmName();
              			$listresult['crop']['name']               = $crop[0]->getName() ;
              			$listresult['crop']['id']                 = intval($crop[0]->getId());
              			$listresult['listcheckDenger']            = $listcheckDenger;
              			$listresult['productionDept']['name']     = $prod[0]->getName();
              			$listresult['productionDept']['id']       = intval($prod[0]->getId());
              			$listresult['cropBloc']['id']             = intval($cropBlock->getId());
              			$listresult['cropBloc']['name']           = $cropBlock->getName();
				  	    
				  	    if($listcheckDenger[4]['max'] < $amount){
				  	   	          
				  	   		      	$listresult['keyDanger']       	          = 4; 
		                  			$listresult['danger']                     = $listcheckDenger[4];
		                  			return $listresult;  			
		                  			
				  	     }else{
			                  foreach ($listcheckDenger as $key => $danger) {
			                  		if($danger['min'] >= $amount || $danger['max'] >= $amount)
			                  		{
			                  			$listresult['keyDanger']       	          = $key; 
			                  			$listresult['danger']                     = $danger;
			                  		    return $listresult;		                
			                  			break;
			                  		}
			                  }
				  	  }
				  	 
				  }
				  else{
				  	return null;
				  }

		}
		
		public function Variation($bugId,$cropId,$prodDeptId,$cropBlockId,$thisAmount){
				 
				 $reporting = $this->em->getRepository('AppBundle:Reporting')->findBy(array('cropBlockId' => $cropBlockId,'bugId' => $bugId ,'cropId'=> $cropId,'prodDeptId'=> $prodDeptId),array('dateScouting' =>'DESC'),1,0);
				 if(!$reporting)
				 	return null;
				 if($reporting[0]->getMoyenneamount() !== null)
				 { 	
				   $result    = round((( $thisAmount -  $reporting[0]->getMoyenneamount()) /  $reporting[0]->getMoyenneamount() ) * 100,2);
				   return $result;
				 }else{
				 	return null;
				 }
		}

		public function FindProdutionDepartement($cropBlockId){
			$prod = $this->em->createQueryBuilder('prod')
			  						        ->select('prod')
									        ->from('AppBundle:ProductionDepartement','prod')
									        ->join('AppBundle:CropBlock', 'co', 'WITH', 'co.cropblockProductiondepartement = prod.id')
									        ->where('co.id = :CropBlocId')
									        ->setParameter('CropBlocId',$cropBlockId)
									        ->getQuery()->getResult();
			return $prod;
		}

		public function FindCropBlock($cropBlockId){
     		$cropBlock = $this->em->getRepository('AppBundle:CropBlock')->find($cropBlockId);
			return $cropBlock;
		}
	
}