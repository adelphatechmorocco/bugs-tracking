<?php

namespace AppBundle\Security\Authentication\Handler;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use AppBundle\Entity\User;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    private $container;
    private $router;
    private $tokenStorage;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->router = $container->get('router');

        if ($this->container->has('security.token_storage')) {
            $this->tokenStorage = $this->container->get('security.token_storage');
        } else {
            $this->tokenStorage = $this->container->get('security.context');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $user = $token->getUser();
        
        if($user->hasRole('ROLE_GLOBAL_ADMIN') || $user->hasRole('ROLE_OWNER_FARM') || $user->hasRole('ROLE_TECHNICIEN')) 
           $url = $this->router->generate('app_home_page');
        else
          $url = $this->router->generate('sonata_admin_dashboard');

        return new RedirectResponse($url);
    }
}
