<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\Bug;
use AppBundle\Entity\ConfigBugFarm;


class BugAdminController extends AdminCRUDController
{     
	 public function preDelete(Request $request, $object){
	 	
	     $em = $this->getDoctrine()->getManager();
   		 $IdBug = $object->getId();
   		 $query= $em->createQuery('SELECT RB FROM 
   								AppBundle:RecordingBugs RB where RB.recordingbugsBugs= :IdBug
   								')
   				   ->setParameter('IdBug',$IdBug)
   				   ->getResult(); 
   				 
	    if(!empty($query)){
	    	$this->addFlash('sonata_flash_error','You can not delete this item because it links with Recording Bugs item !');
	   	     return $this->redirect($this->generateUrl('admin_app_bug_list'));
	   	 }
	   	}

	   	public function addConfToFarmAction(Request $request,$id)
	   	{
	   		$em    = $this->getDoctrine()->getManager();
	   		$user = $this->getUser();
	   		if($user->hasRole('ROLE_GROWER'))
	   		{
   			    $manager       =    $this->getUser()->getManagerId();
			    $queryFarm     =   "SELECT f.id from bt_farms f where f.owner_id=".$manager;
	            $stmt          =    $em->getConnection()->prepare($queryFarm);
	            $stmt->execute();
	            $FarmId        = $stmt->fetch();

	   			$farm     = $em->getRepository('AppBundle:Farms')->find($FarmId['id']);
	   			$bug      = $em->getRepository('AppBundle:Bug')->find(array('id' => $id));

	   		}else{
	   			$FarmId  = $this->getUser()->getUserFarm()->getId();
	   			$farm    = $em->getRepository('AppBundle:Farms')->find($FarmId);
	   			$bug     = $em->getRepository('AppBundle:Bug')->find(array('id' => $id));
	   		}	
	   		$ConfigBugFarm = new ConfigBugFarm();
	   		$ConfigBugFarm->setFarmId($farm);
	   		$ConfigBugFarm->setBugId($bug);
	   		$em->persist($ConfigBugFarm);
            $em->flush();
	   		

	   		$this->addFlash('sonata_flash_success',$bug.' has been added from your farm with success');
	   		return $this->redirect($this->generateUrl('admin_app_bug_list'));
	   	   }
	   		public function removeConfToFarmAction(Request $request,$id)
		   	{
		   		$em    = $this->getDoctrine()->getManager();
		   		$user = $this->getUser();
		   		if($user->hasRole('ROLE_GROWER'))
		   		{
		   			$manager       =    $this->getUser()->getManagerId();
				    $queryFarm     =   "SELECT f.id from bt_farms f where f.owner_id=".$manager;
		            $stmt          =    $em->getConnection()->prepare($queryFarm);
		            $stmt->execute();
		            $FarmId        = $stmt->fetch();

	   			     $farm     = $em->getRepository('AppBundle:Farms')->find($FarmId['id']);
		   			 $bug      = $em->getRepository('AppBundle:Bug')->find(array('id' => $id));

		   		}else{
		   			$FarmId  =  $this->getUser()->getUserFarm()->getId();
		   			$farm    =  $em->getRepository('AppBundle:Farms')->find($FarmId);
		   			$bug     =  $em->getRepository('AppBundle:Bug')->find(array('id' => $id));
		   		}		   		
		   		 $qb = $em->createQueryBuilder('cbf');
				 $q = $qb->delete('AppBundle:ConfigBugFarm','cbf')
							->where('cbf.farmId =:farm')
							->andWhere('cbf.BugId=:bug')
							->setParameters(array('farm' => $farm,'bug' => $bug))
				            ->getQuery();
			
				 $q->execute();
		   		$this->addFlash('sonata_flash_success',$bug.' was removed from your farm with success');
		   		return $this->redirect($this->generateUrl('admin_app_bug_list'));
		   	}

		   	public function checkListOfBugAction(Request $request){

		   	     	$request = $this->container->get('request');
		   	     	$user = $this->getUser();
		   	     	$em = $this->getDoctrine()->getManager();

			   		if($request->isXmlHttpRequest())
				         {
					         	if($user->hasRole('ROLE_GLOBAL_ADMIN'))
					         	{
					         		$data["check"] = true;
					         	}
					         	else if($user->hasRole('ROLE_OWNER_FARM'))
					         	{
					         		    $FarmId  =  $this->getUser()->getUserFarm()->getId();
				                        $query =   "SELECT cbf.bug_id from bt_ConfigBugFarm cbf where cbf.farm_id=".$FarmId;
							            $stmt = $em->getConnection()->prepare($query);
							            $stmt->execute();
							            $result     = $stmt->fetchAll();
								        $data = $result;

					         	}else{
					         			    $manager       =   $this->getUser()->getManagerId();
			   								$queryFarm     =   "SELECT f.id from bt_farms f where f.owner_id=".$manager;
								            $stmt          =    $em->getConnection()->prepare($queryFarm);
								            $stmt->execute();
								            $FarmId        = $stmt->fetch();

							                $query =   "SELECT cbf.bug_id from bt_ConfigBugFarm cbf where cbf.farm_id=".$FarmId['id'];
								            $stmt = $em->getConnection()->prepare($query);
								            $stmt->execute();
								            $result     = $stmt->fetchAll();

							                $data = $result;
					         	}
				         }
				         else{
				        	   $data = ["success"=>false];
				             }

					    $response = new Response(json_encode($data));
					    $response->headers->set('Content-Type', 'application/json');
						return $response; 

		     	}

	   	
}
