<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use AppBundle\Entity\User;


class UserAdminController extends AdminCRUDController
{     
	public function preDelete(Request $request, $object)
     {
     		$em        = $this->getDoctrine()->getManager();
   			$idUser    = $object->getId();
   			$roleUser = $object->getRoles()[0];
   			if($roleUser == "ROLE_GROWER")
   			{

				 $query = $em->getRepository('AppBundle:ProductionDepartement')->findBy(array('ProductionDepartementUser' =>$idUser));

			 if(!empty($query)){	
				 		$names = " ";
				 	
				 for($i=0;$i<count($query);$i++){
				 			$names .= $query[$i]->getName().",";
				 	}	
				 	
  				        $this->addFlash('sonata_flash_error','You can not delete this Grower because it links with Productions Departement :'.$names);
	                    return $this->redirect($this->generateUrl('admin_app_user_list'));
			   }	
   			}
   		   	 
     }

  
}
