<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\CardLocation;
use Symfony\Component\HttpFoundation\Response;


class CardLocationAdminController extends AdminCRUDController
{     


	public function coordinatesPolygonCardlocationAction(Request $request)
	{
		$request = $this->container->get('request');

	    if($request->isXmlHttpRequest())
	     {
	          $productionDepartementId = $request->get('productionDepartementId');

				$em = $this->getDoctrine()->getManager();
			    $query = $em->createQuery(
			        "SELECT pd
			        FROM AppBundle:ProductionDepartement pd where pd.id=$productionDepartementId"
			    );
			    $data = $query->getArrayResult(); 

	        }else{
	        	$data = ["success"=>false];
	        }

		    $response = new Response(json_encode($data));
		    $response->headers->set('Content-Type', 'application/json');
			return $response; 
		
	}

	 public function cardlocationMarkersAction(Request $request)
		{
			$request = $this->container->get('request');

	        if($request->isXmlHttpRequest())
	         {
		          $productionDepartementId = $request->get('productionDepartementId');

					$em = $this->getDoctrine()->getManager();
				    $query = $em->createQuery(
				        "SELECT cl
				        FROM AppBundle:CardLocation cl where cl.cardlocationProductiondepartement=$productionDepartementId"
				    );
				    $data = $query->getArrayResult(); 

		        }else{
		        	$data = ["success"=>false];
		        }

			    $response = new Response(json_encode($data));
			    $response->headers->set('Content-Type', 'application/json');
				return $response; 
			


		}
		public function cardlocationShowProductionDepartementAction(Request $request)
			{
				$request = $this->container->get('request');

		        if($request->isXmlHttpRequest())
		         {
			          $cardId = $request->get('cardId');

						$em = $this->getDoctrine()->getManager();
					    $query = $em->createQuery(
					        "SELECT pd ,cl
					        FROM AppBundle:ProductionDepartement pd join pd.ProductiondepartementCardlocation cl where cl=$cardId"
					    );
					    $data = $query->getArrayResult(); 

			        }else{
			        	$data = ["success"=>false];
			        }

				    $response = new Response(json_encode($data));
				    $response->headers->set('Content-Type', 'application/json');
					return $response; 
			


		}
  
}
