<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\ProductionDepartement;
use AppBundle\Entity\Farms;
use Symfony\Component\HttpFoundation\Response;



class ProductionDepartementAdminController extends AdminCRUDController
{     

	public function coordinatesPolygonAction(Request $request)
	{
		$request = $this->container->get('request');
		
        if($request->isXmlHttpRequest())
         {
	          $FarmId = $request->get('FarmId');

				$em = $this->getDoctrine()->getManager();
			    $query = $em->createQuery(
			        "SELECT c
			        FROM AppBundle:Farms c where c.id=$FarmId"
			    );
			    $data = $query->getArrayResult(); 

	        }else{
	        	$data = ["success"=>false];
	        }

		    $response = new Response(json_encode($data));
		    $response->headers->set('Content-Type', 'application/json');
			return $response; 
	}


	public function coordinatesPolygonProductionDepartemnetAction(Request $request)
	{
		$request = $this->container->get('request');
		
		if($request->isXmlHttpRequest())
         {
	          $FarmId = $request->get('FarmId');

				$em = $this->getDoctrine()->getManager();
			    $query = $em->createQuery(
			        "SELECT Pd
			        FROM AppBundle:ProductionDepartement Pd where Pd.productiondepartementFarm=$FarmId"
			    );
			    $data = $query->getArrayResult(); 

	        }else{
	        	$data = ["success"=>false];
	        }

		    $response = new Response(json_encode($data));
		    $response->headers->set('Content-Type', 'application/json');
			return $response; 

	}

}
