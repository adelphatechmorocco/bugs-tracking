<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\CropType;
use AppBundle\Entity\Crop;



class CropTypeAdminController extends AdminCRUDController
{     

     public function preDelete(Request $request, $object)
     {
     		$em = $this->getDoctrine()->getManager();
   			$idCropType= $object->getId();
     	    $query = $em->createQuery(
	             'SELECT C
	       		  FROM AppBundle:Crop C
	      		  WHERE C.cropCorptype = :cropType'
	      		 )->setParameter('cropType', $idCropType)
     	           ->getResult();
			 if(!empty($query)){		
  				        $this->addFlash('sonata_flash_error','You can not delete this item because it links with crop item!');
	                    return $this->redirect($this->generateUrl('admin_app_croptype_list'));
			   }	
     }

  

}
