<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\CropBlock;
use AppBundle\Entity\ProductionDepartement;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;


class CropBlockAdminController extends AdminCRUDController
{     

// All production departement select...
  public function farmProductionDepartementCropblockAction(Request $request)
	{
		$request = $this->container->get('request');
		$user =  $this->get('security.context')->getToken()->getUser();
		$userId = $user->getId(); 

        if($request->isXmlHttpRequest())
         {
	          $FarmProductionId = $request->get('FarmProductionId');

				$em = $this->getDoctrine()->getManager();
				if($user->hasRole("ROLE_GROWER"))
				{
				    $query = $em->createQuery(
				        "SELECT pd
				         FROM AppBundle:ProductionDepartement pd where
				         pd.productiondepartementFarm=$FarmProductionId 
				         and pd.ProductionDepartementUser=$userId "
			   		 );
				}else{
					 $query = $em->createQuery(
				        "SELECT pd
				         FROM AppBundle:ProductionDepartement pd
				         where pd.productiondepartementFarm=$FarmProductionId"
			   		 );
				}
			    $data = $query->getArrayResult(); 

	        }else{
	        	$data = ["success"=>false];
	        }

		    $response = new Response(json_encode($data));
		    $response->headers->set('Content-Type', 'application/json');
			return $response; 
		


	}
	// get Prodction departement
	public function coordinatesPolygonCropblockAction(Request $request)
	{
		$request = $this->container->get('request');

        if($request->isXmlHttpRequest())
         {
	          $ProductionId = $request->get('ProductionId');

				$em = $this->getDoctrine()->getManager();
			    $query = $em->createQuery(
			        "SELECT pd
			        FROM AppBundle:ProductionDepartement pd where pd.id=$ProductionId"
			    );
			    $data = $query->getArrayResult(); 

	        }else{
	        	$data = ["success"=>false];
	        }

		    $response = new Response(json_encode($data));
		    $response->headers->set('Content-Type', 'application/json');
			return $response;

	}
	
	// get exist crop block

	public function departementProductionCropblockAction(Request $request)
	{
	 $request = $this->container->get('request');

        if($request->isXmlHttpRequest())
         {
	          $ProductionId = $request->get('ProductionId');

				$em = $this->getDoctrine()->getManager();
			    $query = $em->createQuery(
			        "SELECT cp
			        FROM AppBundle:CropBlock cp where cp.cropblockProductiondepartement=$ProductionId"
			    );
			    $data = $query->getArrayResult(); 

	        }else{
	        	$data = ["success"=>false];
	        }

		    $response = new Response(json_encode($data));
		    $response->headers->set('Content-Type', 'application/json');
			return $response; 

	}
   public function cropblockRecordingDateAction(Request $request){
   	       $request = $this->container->get('request');

          
        if($request->isXmlHttpRequest())
         {
	          $ProductionId = $request->get('ProductionId');
	          	$date = date('Y-m-d');
				$em = $this->getDoctrine()->getManager();
			    $query = $em->createQuery(
			        "SELECT cp
			        FROM AppBundle:CropBlock cp where cp.cropblockProductiondepartement=$ProductionId and cp.startDate <= '$date' and  cp.endDate >= '$date'"
			       
			    );
			    
			    $data = $query->getArrayResult(); 

	        }else{
	        	$data = ["success"=>false];
	        }

		    $response = new Response(json_encode($data));
		    $response->headers->set('Content-Type', 'application/json');
			return $response; 


   }
  
}
