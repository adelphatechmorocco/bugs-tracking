<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\Crop;
use AppBundle\Entity\ConfigCropFarm;


class CropAdminController extends AdminCRUDController
{     
   public function preDelete(Request $request, $object){

   		$em = $this->getDoctrine()->getManager();
   		$IdCrop = $object->getId();
   		$query= $em->createQuery('SELECT CP FROM 
   								AppBundle:CropBlock CP where CP.Crop= :idCrop
   								')
   				   ->setParameter('idCrop',$IdCrop)
   				   ->getResult(); 
	    if(!empty($query)){
	    	$this->addFlash('sonata_flash_error','You can not delete this item because it links with CropBlock item !');
	   	     return $this->redirect($this->generateUrl('admin_app_crop_list'));
	   	 }
     }
     public function addConfToFarmAction(Request $request,$id)
     {
        $em    = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        if($user->hasRole('ROLE_GROWER'))
        {
              $manager       =    $this->getUser()->getManagerId();
              $queryFarm     =   "SELECT f.id from bt_farms f where f.owner_id=".$manager;
              $stmt          =    $em->getConnection()->prepare($queryFarm);
              $stmt->execute();
              $FarmId        = $stmt->fetch();

               $farm     = $em->getRepository('AppBundle:Farms')->find($FarmId['id']);
               $crop      = $em->getRepository('AppBundle:Crop')->find(array('id' => $id));

        }else{
          $FarmId   = $this->getUser()->getUserFarm()->getId();
          $farm     = $em->getRepository('AppBundle:Farms')->find($FarmId);
          $crop     = $em->getRepository('AppBundle:Crop')->find(array('id' => $id));
        } 
        $ConfigCropFarm = new ConfigCropFarm();
        $ConfigCropFarm->setFarmId($farm);
        $ConfigCropFarm->setCropId($crop);
        $em->persist($ConfigCropFarm);
        $em->flush();
        

        $this->addFlash('sonata_flash_success',$crop.' has been added from your farm with success');
        return $this->redirect($this->generateUrl('admin_app_crop_list'));
       
     }
      public function removeConfToFarmAction(Request $request,$id)
      {
          $em    = $this->getDoctrine()->getManager();
          $user = $this->getUser();
          if($user->hasRole('ROLE_GROWER'))
          {
            $manager       =    $this->getUser()->getManagerId();
            $queryFarm     =   "SELECT f.id from bt_farms f where f.owner_id=".$manager;
                $stmt          =    $em->getConnection()->prepare($queryFarm);
                $stmt->execute();
                $FarmId        = $stmt->fetch();

               $farm     = $em->getRepository('AppBundle:Farms')->find($FarmId['id']);
               $crop      = $em->getRepository('AppBundle:Crop')->find(array('id' => $id));

          }else{
            $FarmId  =  $this->getUser()->getUserFarm()->getId();
            $farm    =  $em->getRepository('AppBundle:Farms')->find($FarmId);
            $crop     =  $em->getRepository('AppBundle:Crop')->find(array('id' => $id));
          }         
           $qb = $em->createQueryBuilder('ccf');
           $q = $qb->delete('AppBundle:ConfigCropFarm','ccf')
              ->where('ccf.farmId =:farm')
              ->andWhere('ccf.CropId=:crop')
              ->setParameters(array('farm' => $farm,'crop' => $crop))
              ->getQuery();
            $q->execute();

          $this->addFlash('sonata_flash_success',$crop.' was removed from your farm with success' );
          return $this->redirect($this->generateUrl('admin_app_crop_list'));
         
      }

       public function checkListOfCropAction(Request $request){
              $request = $this->container->get('request');
              $user = $this->getUser();
              $em = $this->getDoctrine()->getManager();

            if($request->isXmlHttpRequest())
                 {
                    if($user->hasRole('ROLE_GLOBAL_ADMIN')) 
                    {
                      $data["check"] = true;
                    }
                    else if($user->hasRole('ROLE_OWNER_FARM'))
                    {
                          $FarmId  =  $this->getUser()->getUserFarm()->getId();
                          $query =   "SELECT ccf.crop_id from bt_ConfigCropFarm ccf where ccf.farm_id=".$FarmId;
                          $stmt = $em->getConnection()->prepare($query);
                          $stmt->execute();
                          $result     = $stmt->fetchAll();
                        $data = $result;

                    }else{
                            $manager       =   $this->getUser()->getManagerId();
                            $queryFarm     =   "SELECT f.id from bt_farms f where f.owner_id=".$manager;
                            $stmt          =    $em->getConnection()->prepare($queryFarm);
                            $stmt->execute();
                            $FarmId        = $stmt->fetch();

                              $query =    "SELECT ccf.crop_id from bt_ConfigCropFarm ccf where ccf.farm_id=".$FarmId['id'];
                              $stmt = $em->getConnection()->prepare($query);
                              $stmt->execute();
                              $result     = $stmt->fetchAll();

                              $data = $result;
                    }
                 }
                 else{
                     $data = ["success"=>false];
                     }

              $response = new Response(json_encode($data));
              $response->headers->set('Content-Type', 'application/json');
            return $response; 

          

       }





}
