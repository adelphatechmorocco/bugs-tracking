<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\Bug;


class BugTypeAdminController extends AdminCRUDController
{     

	public function PreDelete(Request $request,$object)
	{
		$em = $this->getDoctrine()->getManager();
		$IdBugType = $object->getId();
		$query = $em
				->createQuery('SELECT b FROM AppBundle:Bug b
			           	       where b.bugBugType =:bugTypeId
			    ')->setParameter('bugTypeId',$IdBugType)
			      ->getResult();
	    if(!empty($query))
	    {
	    	$this->addFlash('sonata_flash_error','You can not delete this item because it links with Bug item !');
	    	return $this->redirect($this->generateUrl('admin_app_bugtype_list'));
	    }

	}

  
}
