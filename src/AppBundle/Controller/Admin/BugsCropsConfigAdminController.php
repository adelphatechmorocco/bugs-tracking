<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class BugsCropsConfigAdminController extends AdminCRUDController
{     

       public function configCropAction(Request $request)
		{
			$request = $this->container->get('request'); 

	        if($request->isXmlHttpRequest())
	         {
                 $BugId = $request->get('bugId');

			    $em = $this->getDoctrine()->getManager();
		
			    $queryBugInConfig =  $em->createQueryBuilder()
			    	->select('crop.id')
			    	->from('AppBundle:BugsCropsConfig','bbc')
			    	->join('bbc.crop','crop')
			    	->where('bbc.bug = :BugId')
			    	->setParameter('BugId',$BugId)
			    	->getQuery()->getArrayResult();
			    if(!empty($queryBugInConfig))
			    {
			    	    $queryCropConfig  =  $em->createQueryBuilder()
				    					->select('Crop.id,Crop.name')
				    					->from('AppBundle:Crop','Crop')
				    					->where('Crop.id  not in (:listCrop)')
				    					->setParameter('listCrop',$queryBugInConfig)
				    							->getQuery()->getArrayResult();
			    }else {
			    	 $queryCropConfig   = $em->createQuery('select m from AppBundle:Crop m')
        														->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

			     } 
	   		
				    $data = $queryCropConfig;
		        }else{
		        	$data = ["success"=>false];
		        }

			    $response = new Response(json_encode($data));
			    $response->headers->set('Content-Type', 'application/json');
				return $response; 
			


		   }

  
}
