<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Entity\Bug;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\ConfigBugFarm;
use Doctrine\ORM\Query\Expr\Join;



class RecordingAdminController extends AdminCRUDController
{     


	public function allBugsAction(Request $request)
	{
		$request = $this->container->get('request');
		$user = $this->getUser();
        if($request->isXmlHttpRequest())
         {
         	$em = $this->getDoctrine()->getManager();
   
	         if($user->hasRole('ROLE_OWNER_FARM')){
     	        $query = $em->createQueryBuilder('bug')
                                ->select('bug')
                                ->from('AppBundle:Bug','bug')
                                ->join('AppBundle:ConfigBugFarm','cbf',Join::WITH, 'bug.id = cbf.BugId')
                                ->where('cbf.farmId =  :fId')
                                ->andWhere('bug.enabled=1')
                                ->setParameter('fId',$user->getuserFarm())
                                ->getQuery()->getArrayResult();

	         	
			    $data = $query;


	         }
	         else if($user->hasRole('ROLE_GROWER')){
	         	$ownerFarm = $em->getRepository('AppBundle:User')->find(array('id' =>$user->getManagerId()));
	         	$query = $em->createQueryBuilder('bug')
                                ->select('bug')
                                ->from('AppBundle:Bug','bug')
                                ->join('AppBundle:ConfigBugFarm','cbf',Join::WITH, 'bug.id = cbf.BugId')
                                ->where('cbf.farmId =  :fId')
                                ->andWhere('bug.enabled=1')
                                ->setParameter('fId',$ownerFarm->getuserFarm())
                                ->getQuery()->getArrayResult();

	         	
			    $data = $query;


	         }else if ($user->hasRole('ROLE_OPERATOR')){
	         	 $growerFarm = $em->getRepository('AppBundle:User')->find(array('id' =>$user->getManagerId()));
	         	 $ownerFarm = $em->getRepository('AppBundle:User')->find(array('id' =>$growerFarm->getManagerId()));
	         	$query = $em->createQueryBuilder('bug')
                                ->select('bug')
                                ->from('AppBundle:Bug','bug')
                                ->join('AppBundle:ConfigBugFarm','cbf',Join::WITH, 'bug.id = cbf.BugId')
                                ->where('cbf.farmId =  :fId')
                                ->andWhere('bug.enabled=1')
                                ->setParameter('fId',$ownerFarm->getuserFarm())
                                ->getQuery()->getArrayResult();

	         	
			    $data = $query;

	         }
	         else{
	         		  $query = $em->createQuery(
					        "SELECT bugs
					        FROM AppBundle:Bug bugs where bugs.enabled=1 order by bugs.priority ASC"
						    );
				     $data = $query->getArrayResult(); 
	          }
				
			    
	        }else{
	        	$data = ["success"=>false];
	        }

		    $response = new Response(json_encode($data));
		    $response->headers->set('Content-Type', 'application/json');
			return $response; 
	}
	function unique_obj($obj) {
		static $idList = array();
		if(in_array($obj->id,$idList)) {
			return false;
		}
		$idList []= $obj->id;
		return true;
    }

	public function recordingShowOthersAction()
	{
		$request = $this->container->get('request');
		$user = $this->getUser();

	        if($request->isXmlHttpRequest())
	         {
	         	$em = $this->getDoctrine()->getManager();
	   
			         if($user->hasRole('ROLE_GLOBAL_ADMIN')){

		     	        $productionDeparetement = $em->createQueryBuilder('prod')
					                                ->select('prod.id,prod.name')
					                                ->from('AppBundle:ProductionDepartement','prod')
					                                ->getQuery()->getArrayResult();

					     $RecordingDate         = $em->createQueryBuilder('Record')
					                                ->select('Record.scoutingDate')
					                                ->from('AppBundle:Recording','Record')
					                                ->distinct()
					                                ->getQuery()->getArrayResult();

					    $recordingProd          = $em->createQueryBuilder('recordingProd')
					    										 ->select('Prod.id,record.scoutingDate')
					    										 ->from('AppBundle:ProductionDepartement','Prod')
					    										 ->join('Prod.ProductiondepartementCardlocation','card')
					    										 ->join('card.cardlocationRecording','record')
					    										 ->distinct()
					    										 ->getQuery()->getArrayResult();



			         	
		      		   }else if($user->hasRole('ROLE_OWNER_FARM')){

				     	        $productionDeparetement = $em->createQueryBuilder('prod')
								                                ->select('prod.id,prod.name')
								                                ->from('AppBundle:ProductionDepartement','prod')
								                                ->leftjoin('prod.productiondepartementFarm' ,'farm')
				                                                ->where('farm.farmUser =:Owner')
				                                                ->setParameter('Owner',$user)
								                                ->getQuery()->getArrayResult();
								                               

							     $RecordingDate         = $em->createQueryBuilder('Record')
							                                ->select('Record.scoutingDate')
							                                ->from('AppBundle:Recording','Record')
							                                ->leftJoin('Record.recordingCardlocation','cardL')
											                ->leftJoin('cardL.cardlocationProductiondepartement','Prod')
											                ->leftJoin('Prod.productiondepartementFarm','Farm')
											                ->where('Farm.farmUser=:user')
											                ->setParameter('user',$user)
							                                ->distinct()
							                                ->getQuery()->getArrayResult();
							                                   

							    $recordingProd          = $em->createQueryBuilder('recordingProd')
							    										 ->select('Prod.id,record.scoutingDate')
							    										 ->from('AppBundle:ProductionDepartement','Prod')
							    										 ->join('Prod.ProductiondepartementCardlocation','card')
							    										 ->join('card.cardlocationRecording','record')
							    										 ->leftJoin('Prod.productiondepartementFarm','Farm')
															             ->where('Farm.farmUser=:user')
															             ->setParameter('user',$user)
							    										 ->distinct()
							    										 ->getQuery()->getArrayResult();

		      		   }else if($user->hasRole('ROLE_GROWER')){

		      		   			 $productionDeparetement = $em->createQueryBuilder('prod')
								                                ->select('prod.id,prod.name')
								                                ->from('AppBundle:ProductionDepartement','prod')
								                                ->where('prod.ProductionDepartementUser =:grower')
				                                                ->setParameter('grower',$user)
								                                ->getQuery()->getArrayResult();
								                               

							     $RecordingDate         = $em->createQueryBuilder('Record')
							                                ->select('Record.scoutingDate')
							                                ->from('AppBundle:Recording','Record')
							                                ->leftJoin('Record.recordingCardlocation','cardL')
												            ->leftJoin('cardL.cardlocationProductiondepartement','Prod')
												            ->where('Prod.ProductionDepartementUser=:user')
												            ->setParameter('user',$user)
							                                ->distinct()
							                                ->getQuery()->getArrayResult();
							                                   

							    $recordingProd          = $em->createQueryBuilder('recordingProd')
							    										 ->select('Prod.id,record.scoutingDate')
							    										 ->from('AppBundle:ProductionDepartement','Prod')
							    										 ->join('Prod.ProductiondepartementCardlocation','card')
							    										 ->join('card.cardlocationRecording','record')
							    										 ->where('Prod.ProductionDepartementUser=:user')
												           				 ->setParameter('user',$user)
							    										 ->distinct()
							    										 ->getQuery()->getArrayResult();


		      		   }else if($user->hasRole('ROLE_OPERATOR')){

		      		   	        $productionDeparetement = $em->createQueryBuilder('prod')
								                                ->select('prod.id,prod.name')
								                                ->from('AppBundle:ProductionDepartement','prod')
								                                ->innerJoin('prod.ProductionDepartementOperator', 'u')
												                ->where('u.id=:usr')
												                ->setParameter('usr',$user->getId())

								                                ->getQuery()->getArrayResult();
								                               

							     $RecordingDate         = $em->createQueryBuilder('Record')
							                                ->select('Record.scoutingDate')
							                                ->from('AppBundle:Recording','Record')
						                                    ->where('Record.recordingUser=:user')
            												->setParameter('user',$user)
							                                ->distinct()
							                                ->getQuery()->getArrayResult();
							                                   

							    $recordingProd          = $em->createQueryBuilder('recordingProd')
							    										 ->select('Prod.id,record.scoutingDate')
							    										 ->from('AppBundle:ProductionDepartement','Prod')
							    										 ->join('Prod.ProductiondepartementCardlocation','card')
							    										 ->join('card.cardlocationRecording','record')
							    										 ->where('record.recordingUser=:user')
            															 ->setParameter('user',$user)
							    										 ->distinct()
							    										 ->getQuery()->getArrayResult();

		      		   }

      		    $data['recordingProd'] = $recordingProd;
			    $data['productionDeparetement'] = $productionDeparetement;
			    $data['RecordingDate'] = $RecordingDate;


			   }else{
	        	$data = ["success"=>false];
	          }
		    $response = new Response(json_encode($data));
		    $response->headers->set('Content-Type', 'application/json');
			return $response; 
	}
	
                  


  
}
