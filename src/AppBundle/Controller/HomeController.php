<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Reporting;
use AppBundle\Entity\AmountPositionCard;
use Symfony\Component\Validator\Constraints\DateTime;


class HomeController extends Controller
{
     /**
     * @Route("/website/" , name="app_website")
     */
     public function websiteAction()
     {
        return $this->render('Reporting/website.html.twig');
     }
    

    /**
     * @Route("/home" , name="app_home_page")
     */
    public function indexAction()
    {

            $listHotSpots   = $this->getwithSpecificateQueryHotSpots();
            $listVariations = $this->getwithSpecificateQueryVariations();
            $user = $this->getUser();
            $em = $this->getDoctrine()->getManager();

           if($user->hasRole('ROLE_TECHNICIEN')){
                        $allFarms =  $em->createQueryBuilder('Farm')
                                                        ->select('Farm')
                                                        ->from('AppBundle:Farms','Farm')
                                                        ->leftjoin('Farm.farmsTechnicien' ,'technicien')
                                                        ->where('technicien.id =  :techId')
                                                        ->setParameter('techId',$user->getId())
                                                        ->getQuery()->getResult();

                        $hotSpots = null;
                        $variations = null;

               }

            elseif($user->hasRole('ROLE_GLOBAL_ADMIN')){
                        $allFarms = $em->getRepository('AppBundle:Farms')->createQueryBuilder('Farms')->getQuery()->getArrayResult();
                        $hotSpots = null;
                        $variations = null;

               }
            elseif ($user->hasRole('ROLE_OWNER_FARM')){
                              $allFarms = null;
                             $prodDepart = $em->createQueryBuilder('ProdDept')
                                            ->select('ProdDept.id')
                                            ->from('AppBundle:ProductionDepartement','ProdDept')
                                            ->leftjoin('ProdDept.productiondepartementFarm' ,'farm')
                                             ->where('farm.farmUser =:Owner')
                                             ->setParameter('Owner',$user)
                                             ->getQuery()->getResult();
                             $listProd =[];
                             foreach ($prodDepart as $prod) {
                                array_push($listProd, (int) $prod['id']);
                            }

                           $hotSpots = $em->createQueryBuilder('Repor')
                                                ->select('Repor')
                                                ->from('AppBundle:Reporting','Repor')
                                                ->where('Repor.id in (:listIds)')
                                                ->andWhere('Repor.prodDeptId in (:listProd)')
                                                ->orderBy('Repor.dateScouting','DESC')
                                                ->setParameters(array('listIds'=> $listHotSpots,'listProd' =>$listProd ))
                                                ->getQuery()->getResult();
                           $variations= $em->createQueryBuilder('Repor')
                                                ->select('Repor')
                                                ->from('AppBundle:Reporting','Repor')
                                                ->where('Repor.id in (:listIds)')
                                                ->andWhere('Repor.prodDeptId in (:listProd)')
                                                ->orderBy('Repor.dateScouting','DESC')
                                                ->setParameters(array('listIds'=> $listVariations,'listProd' =>$listProd ))
                                                ->getQuery()->getResult();


            }else{
              return $this->redirectToRoute('sonata_admin_dashboard');
            }

              return $this->render('Reporting/Home.html.twig',array('hotSpots' => $hotSpots,'variations' => $variations,'farms' => $allFarms));
        }

     /**
     *  @Route("/Reporting/" , name="app_reporting")
     *  @Route("/Reporting/{Reporting}/details/" , name="app_reporting_details",defaults={"idReporting" = null})
     */
    public function ReportingAction(Reporting $Reporting=null)
    {
        $user  = $this->getUser();
        $em    = $this->getDoctrine()->getManager();
        $crops = $em->getRepository('AppBundle:Crop')->findBy(array('enabled' => 1));
        $bugs  = $em->getRepository('AppBundle:Bug')->findBy(array('enabled' => 1));
        $cropblock =( $Reporting  ? $em->getRepository('AppBundle:CropBlock')->find($Reporting->getCropBlockId()) : null );

        $cardamountpoistion = ( $Reporting  ? $em->createQueryBuilder('apc')
                                                    ->select('apc')
                                                    ->from('AppBundle:AmountPositionCard','apc')
                                                    ->where('apc.Reporting = :reporting')
                                                    ->setParameter('reporting',$Reporting)
                                                     ->getQuery()->getArrayResult() : null );

        $ReportingsBugCrop =( $Reporting  ?  $em->createQueryBuilder('Repor')
                                                    ->select('Repor')
                                                    ->from('AppBundle:Reporting','Repor')
                                                    ->where('Repor.farmId =:FarmId')
                                                    ->andWhere('Repor.cropId  =:cropId ')
                                                    ->andWhere('Repor.bugId =:bugId')
                                                    ->andWhere('Repor.dateScouting <= :date')
                                                    ->andWhere('Repor.cropBlockId = :cropBlockId')
                                                    ->andWhere('Repor.cropBlockId = :cropBlockId')
                                                    ->orderBy('Repor.dateScouting','asc')
                                                    ->setParameters(array('cropBlockId' => $Reporting->getCropBlockId(),'FarmId' => $Reporting->getFarmId(),'cropId' => $Reporting->getCropId(),'bugId' =>$Reporting->getBugId(),'date' =>$Reporting->getDateScouting()))
                                                    ->getQuery()->getArrayResult() : null) ;
        /*dump($ReportingsBugCrop);
        die;*/

        if($user->hasRole('ROLE_OWNER_FARM')){

            $farm = $user->getUserFarm();
            $allFarms = null;

             if(!$farm){

                   $this->addFlash('warning', 'You have to create the farm and make the records');
             }
             else{
                  $allDateRecording = $em->createQueryBuilder('Repor')
                                                ->select('Repor.dateScouting')
                                                ->from('AppBundle:Reporting','Repor')
                                                ->where('Repor.farmId =:farmId')
                                                ->distinct()
                                                ->orderBy('Repor.dateScouting','DESC')
                                                ->setParameter('farmId',$farm->getId())
                                                ->getQuery()->getArrayResult();
                   if(!$allDateRecording)
                      $allDateRecording = null;
             }

        }else{
               if($user->hasRole('ROLE_TECHNICIEN'))
               {
                    $allFarms =  $em->createQueryBuilder('Farm')
                                                        ->select('Farm')
                                                        ->from('AppBundle:Farms','Farm')
                                                        ->leftjoin('Farm.farmsTechnicien' ,'technicien')
                                                        ->where('technicien.id =  :techId')
                                                        ->setParameter('techId',$user->getId())
                                                        ->getQuery()->getArrayResult();

                }else{
                     $allFarms = $em->getRepository('AppBundle:Farms')->createQueryBuilder('Farms')->getQuery()->getArrayResult();
                }

               $farm = ( $Reporting  ? $em->getRepository('AppBundle:Farms')->findBy(array('id' =>$Reporting->getFarmId()))[0] : null ) ;
               $allDateRecording =( $Reporting  ? $em->createQueryBuilder('Repor')
                                                ->select('Repor.dateScouting')
                                                ->from('AppBundle:Reporting','Repor')
                                                ->where('Repor.farmId =:farmId')
                                                ->distinct()
                                                ->orderBy('Repor.dateScouting','DESC')
                                                ->setParameter('farmId',$Reporting->getFarmId())
                                                ->getQuery()->getArrayResult() : null);
         }

		    return $this->render('Reporting/Reporting.html.twig',array('farm' => $farm,'crops' => $crops,'bugs' => $bugs,'ReportDetails' => $Reporting,'cropblock' => $cropblock,'farms' =>$allFarms,'allDateRecording' => $allDateRecording,'ReportingsBugCrop' => $ReportingsBugCrop,'cardamountpoistion' => $cardamountpoistion ));
    }


    /**
    * @Route("/reporting/details/filter",name="reporting_details_filter")
    */
     public function ReportingWithFilterAction(Request $request){

          $em = $this->getDoctrine()->getManager();
          if($request->isXmlHttpRequest()) {

                     $FarmId     = $request->query->get('farmId');
                     $bugId      = $request->query->get('bugId') ;
                     $cropId     = $request->query->get('cropId');
                     $date       = $request->query->get('date') ;

                     $Reportings   = $em->createQueryBuilder('Repor')
                                                    ->select('Repor')
                                                    ->from('AppBundle:Reporting','Repor')
                                                    ->where('Repor.farmId =:FarmId')
                                                    ->andWhere('Repor.cropId  =:cropId ')
                                                    ->andWhere('Repor.bugId =:bugId')
                                                    ->andWhere('Repor.dateScouting =:date')
                                                    ->orderBy('Repor.dateScouting','DESC')
                                                    ->setParameters(array('FarmId' => $FarmId,'cropId' => $cropId,'bugId' =>$bugId,'date' => $date))
                                                    ->getQuery()->getArrayResult();
                    
                    $cropBlockId  = [];
                    $reportingIds = [];
                    foreach ($Reportings as $reporting) {
                              array_push($cropBlockId ,$reporting['cropBlockId']);
                              array_push($reportingIds, $reporting['id']);
                           }
                  $cropblocks = $em->createQueryBuilder('cropBlock')
                                    ->select('cropBlock')
                                    ->from('AppBundle:CropBlock','cropBlock')
                                    ->where('cropBlock.id in (:listIdCropBlock)')
                                    ->setParameter('listIdCropBlock',$cropBlockId)
                                    ->getQuery()->getArrayResult();

                 $ReportingsBugCrop = $em->createQueryBuilder('Repor')
                                                    ->select('Repor')
                                                    ->from('AppBundle:Reporting','Repor')
                                                    ->where('Repor.farmId =:FarmId')
                                                    ->andWhere('Repor.cropId  =:cropId ')
                                                    ->andWhere('Repor.bugId =:bugId')
                                                    ->andWhere('Repor.cropBlockId in (:listIdCropBlock)')
                                                    ->andWhere('Repor.dateScouting <= :date')
                                                    ->orderBy('Repor.dateScouting','DESC')
                                                    ->setParameters(array('listIdCropBlock' => $cropBlockId,'FarmId' => $FarmId,'cropId' => $cropId,'bugId' =>$bugId,'date' =>$date))
                                                    ->getQuery()->getArrayResult();

                  $cradamount = $em->createQueryBuilder('cardap')
                                    ->select('cardap')
                                    ->from('AppBundle:AmountPositionCard','cardap')
                                    ->join('cardap.Reporting','reporting')
                                    ->where('reporting.id in (:RepoIds)')
                                    ->setParameter('RepoIds',$reportingIds)
                                    ->getQuery()->getArrayResult();


                  $data["Reportings"]         = $Reportings;
                  $data["ReportingsBugCrop"]  = $ReportingsBugCrop;
                  $data["cropblocks"]         = $cropblocks;
                  $data["amoutpositioncard"]  = $cradamount;


              }else {
                     $data = ["success"=>false];
              }

         return new JsonResponse($data);
      }
     /**
    * @Route("/reporting/details/datesRecord/",name="reporting_details_filter_dateRecord")
    */
     public function ReportingGetDateRecordAction(Request $request){

          $em = $this->getDoctrine()->getManager();
          if($request->isXmlHttpRequest()) {
                     $FarmId           = $request->query->get('farmId');
                     $allDateRecording = $em->createQueryBuilder('Repor')
                                                ->select('Repor.dateScouting')
                                                ->from('AppBundle:Reporting','Repor')
                                                ->where('Repor.farmId =:farmId')
                                                ->distinct()
                                                ->orderBy('Repor.dateScouting','DESC')
                                                ->setParameter('farmId',$FarmId)
                                                ->getQuery()->getArrayResult();
                  $data = $allDateRecording;
              }else {
                     $data = ["success"=>false];
              }

          return new JsonResponse($data);
      }
    /**
    * @Route("/reporting/details/Detailsadmin/",name="reporting_details_filter_Detailsadmin")
    */
     public function ReportingGetDetailsadminAction(Request $request){

            $listHotSpots   = $this->getwithSpecificateQueryHotSpots();
            $listVariations = $this->getwithSpecificateQueryVariations();
            $em = $this->getDoctrine()->getManager();

          if($request->isXmlHttpRequest()) {
                     $FarmId      = $request->query->get('farmId');
                     $hotSpots = $em->createQueryBuilder('Repor')
                                            ->select('Repor')
                                            ->from('AppBundle:Reporting','Repor')
                                            ->where('Repor.id in (:listIds)')
                                            ->andWhere('Repor.farmId =:farmId')
                                             ->orderBy('Repor.dateScouting','DESC')
                                            ->setParameters(array('listIds' =>$listHotSpots,'farmId'=>$FarmId))
                                            ->getQuery()->getArrayResult();
                       $variations= $em->createQueryBuilder('Repor')
                                            ->select('Repor')
                                            ->from('AppBundle:Reporting','Repor')
                                            ->where('Repor.id in (:listIds)')
                                            ->andWhere('Repor.farmId =:farmId')
                                            ->orderBy('Repor.dateScouting','DESC')
                                            ->setParameters(array('listIds'=>$listVariations,'farmId'=>$FarmId))
                                            ->getQuery()->getArrayResult();

                  $data['hotSpots']   = $hotSpots;
                  $data['variations'] = $variations;
              }else {
                     $data = ["success"=>false];
              }

          return new JsonResponse($data);
      }

    public function getwithSpecificateQueryHotSpots()
    {
             $query =   "SELECT
                            e.*
                        FROM
                            bt_reporting e
                          JOIN
                            ( SELECT
                                  bugId,cropId,cropBlockId,prodDeptId,MAX(dateScouting) AS latest_date_registered
                              FROM
                                 bt_reporting
                              GROUP BY
                                  bugId, cropId,prodDeptId,cropBlockId
                            ) AS grp
                            ON  grp.bugId = e.bugId
                            AND grp.cropId = e.cropId
                            AND grp.cropBlockId = e.cropBlockId
                            AND grp.prodDeptId = e.prodDeptId
                            AND grp.latest_date_registered = e.dateScouting
                            where e.keyIntervalDanger =4 OR e.keyIntervalDanger=3";

            $stmt = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($query);
            $stmt->execute();
            $ListReportingId = [];
            $result     = $stmt->fetchAll();
            foreach ($result as $list) {
               array_push($ListReportingId,intval($list['id']));
            }
            return $ListReportingId;

    }
    public function getwithSpecificateQueryVariations()
    {
             $query =   "SELECT
                            e.*
                        FROM
                            bt_reporting e
                          JOIN
                            ( SELECT
                                  bugId,cropId,cropBlockId,prodDeptId,MAX(dateScouting) AS latest_date_registered
                              FROM
                                 bt_reporting
                              GROUP BY
                                  bugId, cropId,prodDeptId,cropBlockId
                            ) AS grp
                            ON  grp.bugId = e.bugId
                            AND grp.cropId = e.cropId
                            AND grp.cropBlockId = e.cropBlockId
                            AND grp.prodDeptId = e.prodDeptId
                            AND grp.latest_date_registered = e.dateScouting
                            where e.variation IS NOT NULL  and e.variation <> 0 ";




            $stmt = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($query);
            $stmt->execute();
            $ListReportingId = [];
            $result     = $stmt->fetchAll();
            foreach ($result as $list) {
               array_push($ListReportingId,intval($list['id']));
            }
            return $ListReportingId;

    }
   public function menuDetailsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $use = $this->getUser();
        return $this->render('Admin/menu_details.html.twig');
    }



}
