<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Entity\User as BaseUser;
use Sonata\UserBundle\Model\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * User
 *
 * @ORM\Table(name="`bt_user`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 *
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ORM\OneToOne(targetEntity="Farms", mappedBy="farmUser", orphanRemoval=true,cascade={"persist", "remove"})
     */
     private $userFarm;
    /**
    * @ORM\OneToMany(targetEntity="Recording", mappedBy="recordingUser" , orphanRemoval=true,cascade={"persist", "remove"})
    */
     private $userRecording;

      /**
     * @ORM\OneToMany(targetEntity="ProductionDepartement", mappedBy="ProductionDepartementUser" ,cascade={"persist"})
     */
     private $UserProductionDepartement;

     /**
     * @ORM\ManyToMany(targetEntity="ProductionDepartement", inversedBy="ProductionDepartementOperator",cascade={"persist"})
     * @ORM\JoinTable(name="operator_productionDepartement",
     * joinColumns={@ORM\JoinColumn(name="Operator_id",referencedColumnName="id")},
     * inverseJoinColumns ={ @ORM\JoinColumn(name="productionDepartement_id", referencedColumnName="id")}
     * )
     */
     private $operatorProductionDepartement;

      /**
     * @ORM\ManyToMany(targetEntity="Farms", inversedBy="farmsTechnicien",cascade={"persist"})
     * @ORM\JoinTable(name="Technicien_farms",
     * joinColumns={@ORM\JoinColumn(name="technicien_id",referencedColumnName="id")},
     * inverseJoinColumns ={ @ORM\JoinColumn(name="farm_id", referencedColumnName="id")}
     * )
     */
     private $technicienFarms;




    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=100, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=100, nullable=true)
     */
    private $lastname;
    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=100, nullable=true)
     */
    private $phone;
    /**
     * @var integer
     *
     * @ORM\Column(name="manager_id", type="integer",nullable=true)
     */
    private $managerId;
    
      /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
     private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    public function __toString()
    {
        return ( $this->getEmail() ) ? $this->getEmail() : "-";
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    public function setEmail($email)
    {
        $this->email = $email;
        $this->username = $email;

        return $this;
    }

    public function setEmailCanonical($emailCanonical)
    {
        $this->emailCanonical = $emailCanonical;
        $this->usernameCanonical = $emailCanonical;

        return $this;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    public function getFullname()
    {
        $name = $this->username;
        if ($this->firstname || $this->lastname) {
            $name = sprintf('%s %s', $this->firstname, $this->lastname);
        }

        return $name;
    }

   

    public function getRolesAsString()
    {
        $roles = array();
        foreach ($this->getRoles() as $role) {
            $role = explode('_', $role);
            array_shift($role);
            $roles[] = ucfirst(strtolower(implode(' ', $role)));
        }

        return implode(', ', $roles);
    }
   
    public function __construct()
    {
        parent::__construct();
        $this->roles = array('ROLE_USER');
        $this->ProductionDepartementOperator = new \Doctrine\Common\Collections\ArrayCollection();
    }
   

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set farmUser
     *
     * @param \AppBundle\Entity\Farms $farmUser
     *
     * @return User
     */
    public function setFarmUser(\AppBundle\Entity\Farms $farmUser = null)
    {
        $this->farm_user = $farmUser;

        return $this;
    }

    /**
     * Get farmUser
     *
     * @return \AppBundle\Entity\Farms
     */
    public function getFarmUser()
    {
        return $this->farm_user;
    }

    /**
     * Add userRecording
     *
     * @param \AppBundle\Entity\Recording $userRecording
     *
     * @return User
     */
    public function addUserRecording(\AppBundle\Entity\Recording $userRecording)
    {
        $this->user_recording[] = $userRecording;

        return $this;
    }

    /**
     * Remove userRecording
     *
     * @param \AppBundle\Entity\Recording $userRecording
     */
    public function removeUserRecording(\AppBundle\Entity\Recording $userRecording)
    {
        $this->user_recording->removeElement($userRecording);
    }

    /**
     * Get userRecording
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserRecording()
    {
        return $this->user_recording;
    }

   
   


    /**
     * Set managerId
     *
     * @param integer $managerId
     *
     * @return User
     */
    public function setManagerId($managerId)
    {
        $this->managerId = $managerId;

        return $this;
    }

    /**
     * Get managerId
     *
     * @return integer
     */
    public function getManagerId()
    {
        return $this->managerId;
    }



    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add operatorCardlocation
     *
     * @param \AppBundle\Entity\CardLocation $operatorCardlocation
     *
     * @return User
     */
    public function addOperatorCardlocation(\AppBundle\Entity\CardLocation $operatorCardlocation)
    {
        $this->operatorCardlocation[] = $operatorCardlocation;

        return $this;
    }

    /**
     * Remove operatorCardlocation
     *
     * @param \AppBundle\Entity\CardLocation $operatorCardlocation
     */
    public function removeOperatorCardlocation(\AppBundle\Entity\CardLocation $operatorCardlocation)
    {
        $this->operatorCardlocation->removeElement($operatorCardlocation);
    }

    /**
     * Get operatorCardlocation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOperatorCardlocation()
    {
        return $this->operatorCardlocation;
    }

    /**
     * Set userFarm
     *
     * @param \AppBundle\Entity\Farms $userFarm
     *
     * @return User
     */
    public function setUserFarm(\AppBundle\Entity\Farms $userFarm = null)
    {
        $this->userFarm = $userFarm;

        return $this;
    }

    /**
     * Get userFarm
     *
     * @return \AppBundle\Entity\Farms
     */
    public function getUserFarm()
    {
        return $this->userFarm;
    }

    /**
     * Add userProductionDepartement
     *
     * @param \AppBundle\Entity\ProductionDepartement $userProductionDepartement
     *
     * @return User
     */
    public function addUserProductionDepartement(\AppBundle\Entity\ProductionDepartement $userProductionDepartement)
    {
        $this->UserProductionDepartement[] = $userProductionDepartement;

        return $this;
    }

    /**
     * Remove userProductionDepartement
     *
     * @param \AppBundle\Entity\ProductionDepartement $userProductionDepartement
     */
    public function removeUserProductionDepartement(\AppBundle\Entity\ProductionDepartement $userProductionDepartement)
    {
        $this->UserProductionDepartement->removeElement($userProductionDepartement);
    }

    /**
     * Get userProductionDepartement
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserProductionDepartement()
    {
        return $this->UserProductionDepartement;
    }

    /**
     * Add operatorProductionDepartement
     *
     * @param \AppBundle\Entity\ProductionDepartement $operatorProductionDepartement
     *
     * @return User
     */
    public function addOperatorProductionDepartement(\AppBundle\Entity\ProductionDepartement $operatorProductionDepartement)
    {
        $this->operatorProductionDepartement[] = $operatorProductionDepartement;

        return $this;
    }

    /**
     * Remove operatorProductionDepartement
     *
     * @param \AppBundle\Entity\ProductionDepartement $operatorProductionDepartement
     */
    public function removeOperatorProductionDepartement(\AppBundle\Entity\ProductionDepartement $operatorProductionDepartement)
    {
        $this->operatorProductionDepartement->removeElement($operatorProductionDepartement);
    }

    /**
     * Get operatorProductionDepartement
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOperatorProductionDepartement()
    {
        return $this->operatorProductionDepartement;
    }

    /**
     * Add technicienFarm
     *
     * @param \AppBundle\Entity\Farms $technicienFarm
     *
     * @return User
     */
    public function addTechnicienFarm(\AppBundle\Entity\Farms $technicienFarm)
    {
        $this->technicienFarms[] = $technicienFarm;

        return $this;
    }

    /**
     * Remove technicienFarm
     *
     * @param \AppBundle\Entity\Farms $technicienFarm
     */
    public function removeTechnicienFarm(\AppBundle\Entity\Farms $technicienFarm)
    {
        $this->technicienFarms->removeElement($technicienFarm);
    }

    /**
     * Get technicienFarms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTechnicienFarms()
    {
        return $this->technicienFarms;
    }
}
