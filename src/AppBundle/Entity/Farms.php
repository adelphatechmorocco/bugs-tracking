<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;



/**
 * Farms
 *
 * @ORM\Table(name="`bt_farms`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FarmsRepository")
 */
class Farms
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    private $id;

    /**
     *
     * @ORM\OneToOne(targetEntity="User", inversedBy="userFarm")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
     private $farmUser;

      /**
     * @ORM\OneToMany(targetEntity="ProductionDepartement", mappedBy="productiondepartementFarm" , orphanRemoval=true,cascade={"All"})
     */
     private $farmProductiondepartement;

    /**
     * @ORM\OneToMany(targetEntity="ConfigBugFarm", mappedBy="farmId" , orphanRemoval=true,cascade={"All"})
     */
     private $cbf; 
     
   /**
     * @ORM\OneToMany(targetEntity="ConfigCropFarm", mappedBy="farmId" , orphanRemoval=true,cascade={"All"})
     */
     private $ccf;

      /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="technicienFarms",cascade={"persist"})
     */
     private $farmsTechnicien;

    /**
     * @var string
     *
     * @ORM\Column(name="farm_name", type="string", length=60)
     */
    private $farmName;

    /**
     * @var string
     *
     * @ORM\Column(name="address1", type="string", length=255)
     */
    private $address1;

    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=255, nullable=true)
     */
    private $address2;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=50)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string", length=100)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=50)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="phone1", type="string", length=50)
     */
    private $phone1;

    /**
     * @var string
     *
     * @ORM\Column(name="phone2", type="string", length=50, nullable=true)
     */
    private $phone2;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50)
     */
    private $email;
     /**
     * @var float
     *
     * @ORM\Column(name="positionlat", type="float")
     */
    private $positionlat;

    /**
     * @var float
     *
     * @ORM\Column(name="positionlang", type="float")
     */
    private $positionlang;

    /**
     * @var integer
     *
     * @ORM\Column(name="positionzoom", type="integer")
     */
    private $positionzoom;

    /**
     * @var string
     *
     * @ORM\Column(name="polygoninformation", type="text" )
     */
    private $polygoninformation;
   /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
     private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set farmName
     *
     * @param string $farmName
     *
     * @return Farms
     */
    public function setFarmName($farmName)
    {
        $this->farmName = $farmName;

        return $this;
    }

    /**
     * Get farmName
     *
     * @return string
     */
    public function getFarmName()
    {
        return $this->farmName;
    }

    /**
     * Set address1
     *
     * @param string $address1
     *
     * @return Farms
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     *
     * @return Farms
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Farms
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     *
     * @return Farms
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Farms
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set phone1
     *
     * @param string $phone1
     *
     * @return Farms
     */
    public function setPhone1($phone1)
    {
        $this->phone1 = $phone1;

        return $this;
    }

    /**
     * Get phone1
     *
     * @return string
     */
    public function getPhone1()
    {
        return $this->phone1;
    }

    /**
     * Set phone2
     *
     * @param string $phone2
     *
     * @return Farms
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;

        return $this;
    }

    /**
     * Get phone2
     *
     * @return string
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Farms
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productiondepartement_farm = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Farms
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add productiondepartementFarm
     *
     * @param \AppBundle\Entity\ProductionDepartement $productiondepartementFarm
     *
     * @return Farms
     */
    public function addProductiondepartementFarm(\AppBundle\Entity\ProductionDepartement $productiondepartementFarm)
    {
        $this->productiondepartement_farm[] = $productiondepartementFarm;

        return $this;
    }

    /**
     * Remove productiondepartementFarm
     *
     * @param \AppBundle\Entity\ProductionDepartement $productiondepartementFarm
     */
    public function removeProductiondepartementFarm(\AppBundle\Entity\ProductionDepartement $productiondepartementFarm)
    {
        $this->productiondepartement_farm->removeElement($productiondepartementFarm);
    }

    /**
     * Get productiondepartementFarm
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductiondepartementFarm()
    {
        return $this->productiondepartement_farm;
    }
    public function __toString(){
        return ($this->getFarmName()) ? $this->getFarmName() : '-';
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Farms
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return Farms
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Farms
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set polygoninformation
     *
     * @param string $polygoninformation
     *
     * @return Farms
     */
    public function setPolygoninformation($polygoninformation)
    {
        $this->polygoninformation = $polygoninformation;

        return $this;
    }

    /**
     * Get polygoninformation
     *
     * @return string
     */
    public function getPolygoninformation()
    {
        return $this->polygoninformation;
    }

    /**
     * Set farmUser
     *
     * @param \AppBundle\Entity\User $farmUser
     *
     * @return Farms
     */
    public function setFarmUser(\AppBundle\Entity\User $farmUser = null)
    {
        $this->farmUser = $farmUser;

        return $this;
    }

    /**
     * Get farmUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getFarmUser()
    {
        return $this->farmUser;
    }

    /**
     * Add farmProductiondepartement
     *
     * @param \AppBundle\Entity\ProductionDepartement $farmProductiondepartement
     *
     * @return Farms
     */
    public function addFarmProductiondepartement(\AppBundle\Entity\ProductionDepartement $farmProductiondepartement)
    {
        $this->farmProductiondepartement[] = $farmProductiondepartement;

        return $this;
    }

    /**
     * Remove farmProductiondepartement
     *
     * @param \AppBundle\Entity\ProductionDepartement $farmProductiondepartement
     */
    public function removeFarmProductiondepartement(\AppBundle\Entity\ProductionDepartement $farmProductiondepartement)
    {
        $this->farmProductiondepartement->removeElement($farmProductiondepartement);
    }

    /**
     * Get farmProductiondepartement
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFarmProductiondepartement()
    {
        return $this->farmProductiondepartement;
    }

    /**
     * Set positionlat
     *
     * @param float $positionlat
     *
     * @return Farms
     */
    public function setPositionlat($positionlat)
    {
        $this->positionlat = $positionlat;

        return $this;
    }

    /**
     * Get positionlat
     *
     * @return float
     */
    public function getPositionlat()
    {
        return $this->positionlat;
    }

    /**
     * Set positionlang
     *
     * @param float $positionlang
     *
     * @return Farms
     */
    public function setPositionlang($positionlang)
    {
        $this->positionlang = $positionlang;

        return $this;
    }

    /**
     * Get positionlang
     *
     * @return float
     */
    public function getPositionlang()
    {
        return $this->positionlang;
    }

    /**
     * Set positionzoom
     *
     * @param integer $positionzoom
     *
     * @return Farms
     */
    public function setPositionzoom($positionzoom)
    {
        $this->positionzoom = $positionzoom;

        return $this;
    }

    /**
     * Get positionzoom
     *
     * @return integer
     */
    public function getPositionzoom()
    {
        return $this->positionzoom;
    }

    /**
     * Add cbf
     *
     * @param \AppBundle\Entity\ConfigBugFarm $cbf
     *
     * @return Farms
     */
    public function addCbf(\AppBundle\Entity\ConfigBugFarm $cbf)
    {
        $this->cbf[] = $cbf;

        return $this;
    }

    /**
     * Remove cbf
     *
     * @param \AppBundle\Entity\ConfigBugFarm $cbf
     */
    public function removeCbf(\AppBundle\Entity\ConfigBugFarm $cbf)
    {
        $this->cbf->removeElement($cbf);
    }

    /**
     * Get cbf
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCbf()
    {
        return $this->cbf;
    }

    /**
     * Add ccf
     *
     * @param \AppBundle\Entity\ConfigCropFarm $ccf
     *
     * @return Farms
     */
    public function addCcf(\AppBundle\Entity\ConfigCropFarm $ccf)
    {
        $this->ccf[] = $ccf;

        return $this;
    }

    /**
     * Remove ccf
     *
     * @param \AppBundle\Entity\ConfigCropFarm $ccf
     */
    public function removeCcf(\AppBundle\Entity\ConfigCropFarm $ccf)
    {
        $this->ccf->removeElement($ccf);
    }

    /**
     * Get ccf
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCcf()
    {
        return $this->ccf;
    }

    /**
     * Add farmsTechnicien
     *
     * @param \AppBundle\Entity\User $farmsTechnicien
     *
     * @return Farms
     */
    public function addFarmsTechnicien(\AppBundle\Entity\User $farmsTechnicien)
    {
        $this->farmsTechnicien[] = $farmsTechnicien;

        return $this;
    }

    /**
     * Remove farmsTechnicien
     *
     * @param \AppBundle\Entity\User $farmsTechnicien
     */
    public function removeFarmsTechnicien(\AppBundle\Entity\User $farmsTechnicien)
    {
        $this->farmsTechnicien->removeElement($farmsTechnicien);
    }

    /**
     * Get farmsTechnicien
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFarmsTechnicien()
    {
        return $this->farmsTechnicien;
    }
}
