<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ConfigBugFarm
 *
 * @ORM\Table(name="`bt_ConfigBugFarm`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ConfigBugFarmRepository")
 */
class ConfigBugFarm
{
    
     /**
      * @var int
      *
      * @ORM\Column(name="id", type="bigint")
      * @ORM\Id
      * @ORM\GeneratedValue(strategy="AUTO")
      */
      private $id;

        /**
         * @ORM\ManyToOne(targetEntity="Farms",inversedBy="cbf")
         * @ORM\JoinColumn(name="farm_id", referencedColumnName="id")
        */
        private $farmId;
        /**
         * @ORM\ManyToOne(targetEntity="Bug")
         * @ORM\JoinColumn(name="bug_id", referencedColumnName="id")
         */
          private $BugId;

        /**
         * Get id
         *
         * @return integer
         */
        public function getId()
        {
            return $this->id;
        }

        /**
         * Set farmId
         *
         * @param \AppBundle\Entity\Farms $farmId
         *
         * @return ConfigBugFarm
         */
        public function setFarmId(\AppBundle\Entity\Farms $farmId = null)
        {
            $this->farmId = $farmId;

            return $this;
        }

        /**
         * Get farmId
         *
         * @return \AppBundle\Entity\Farms
         */
        public function getFarmId()
        {
            return $this->farmId;
        }

        /**
         * Set bugId
         *
         * @param \AppBundle\Entity\Bug $bugId
         *
         * @return ConfigBugFarm
         */
        public function setBugId(\AppBundle\Entity\Bug $bugId = null)
        {
            $this->BugId = $bugId;

            return $this;
        }

        /**
         * Get bugId
         *
         * @return \AppBundle\Entity\Bug
         */
        public function getBugId()
        {
            return $this->BugId;
        }
}
