<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reporting
 *
 * @ORM\Table(name="`bt_reporting`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReportingRepository")
 */
class Reporting
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\OneToMany(targetEntity="AmountPositionCard", mappedBy="Reporting", cascade={"All"})
     */
     private $amoutcardPosition;

    /**
     * @var string
     *
     * @ORM\Column(name="moyenneamount", type="float", length=110)
     */
    private $moyenneamount;

    /**
     * @var int
     *
     * @ORM\Column(name="cropBlockId", type="integer")
     */
    private $cropBlockId; 
    /**
     * @var int
     *
     * @ORM\Column(name="farmId", type="integer")
     */
     private $farmId;
     /**
     * @var string
     *
     * @ORM\Column(name="farmName", type="string",length=110)
     */
     private $farmName;

    /**
     * @var int
     *
     * @ORM\Column(name="bugId", type="integer")
     */
    private $bugId;

    /**
     * @var string
     *
     * @ORM\Column(name="bugName", type="string", length=110)
     */
    private $bugName;

    /**
     * @var int
     *
     * @ORM\Column(name="cropId", type="integer")
     */
    private $cropId;

    /**
     * @var string
     *
     * @ORM\Column(name="cropName", type="string", length=110)
     */
    private $cropName;

    /**
     * @var int
     *
     * @ORM\Column(name="keyIntervalDanger", type="integer")
     */
    private $keyIntervalDanger;

    /**
     * @var float
     *
     * @ORM\Column(name="dangerMin", type="float")
     */
    private $dangerMin;

    /**
     * @var float
     *
     * @ORM\Column(name="dangerMax", type="float")
     */
    private $dangerMax;

    /**
     * @var array
     *
     * @ORM\Column(name="listChekDanger", type="array")
     */
    private $listChekDanger;


    /**
     * @var int
     *
     * @ORM\Column(name="prodDeptId", type="integer")
     */
    private $prodDeptId;

    /**
     * @var string
     *
     * @ORM\Column(name="prodDeptName", type="string", length=110)
     */
    private $prodDeptName;

    /**
     * @var string
     *
     * @ORM\Column(name="cropBlockName", type="string", length=110)
     */
    private $cropBlockName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateScouting", type="datetimetz")
     */
    private $dateScouting;

    /**
     * @var float
     *
     * @ORM\Column(name="variation", type="float",nullable=true)
     */
    private $variation;
     

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set moyenneamount
     *
     * @param string $moyenneamount
     *
     * @return Reporting
     */
    public function setMoyenneamount($moyenneamount)
    {
        $this->moyenneamount = $moyenneamount;

        return $this;
    }

    /**
     * Get moyenneamount
     *
     * @return string
     */
    public function getMoyenneamount()
    {
        return $this->moyenneamount;
    }

    /**
     * Set cropBlockId
     *
     * @param integer $cropBlockId
     *
     * @return Reporting
     */
    public function setCropBlockId($cropBlockId) 
    {
        $this->cropBlockId = $cropBlockId;

        return $this;
    }

    /**
     * Get cropBlockId
     *
     * @return int
     */
    public function getCropBlockId()
    {
        return $this->cropBlockId;
    }

    /**
     * Set bugId
     *
     * @param integer $bugId
     *
     * @return Reporting
     */
    public function setBugId($bugId)
    {
        $this->bugId = $bugId;

        return $this;
    }

    /**
     * Get bugId
     *
     * @return int
     */
    public function getBugId()
    {
        return $this->bugId;
    }

    /**
     * Set bugName
     *
     * @param string $bugName
     *
     * @return Reporting
     */
    public function setBugName($bugName)
    {
        $this->bugName = $bugName;

        return $this;
    }

    /**
     * Get bugName
     *
     * @return string
     */
    public function getBugName()
    {
        return $this->bugName;
    }

    /**
     * Set cropId
     *
     * @param integer $cropId
     *
     * @return Reporting
     */
    public function setCropId($cropId)
    {
        $this->cropId = $cropId;

        return $this;
    }

    /**
     * Get cropId
     *
     * @return int
     */
    public function getCropId()
    {
        return $this->cropId;
    }

    /**
     * Set cropName
     *
     * @param string $cropName
     *
     * @return Reporting
     */
    public function setCropName($cropName)
    {
        $this->cropName = $cropName;

        return $this;
    }

    /**
     * Get cropName
     *
     * @return string
     */
    public function getCropName()
    {
        return $this->cropName;
    }

    /**
     * Set keyIntervalDanger
     *
     * @param integer $keyIntervalDanger
     *
     * @return Reporting
     */
    public function setKeyIntervalDanger($keyIntervalDanger) 
    {
        $this->keyIntervalDanger = $keyIntervalDanger;

        return $this;
    }

    /**
     * Get keyIntervalDanger
     *
     * @return int
     */
    public function getKeyIntervalDanger()
    {
        return $this->keyIntervalDanger;
    }

    /**
     * Set dangerMin
     *
     * @param float $dangerMin
     *
     * @return Reporting
     */
    public function setDangerMin($dangerMin) 
    {
        $this->dangerMin = $dangerMin;

        return $this;
    }

    /**
     * Get dangerMin
     *
     * @return float
     */
    public function getDangerMin()
    {
        return $this->dangerMin;
    }

    /**
     * Set dangerMax
     *
     * @param float $dangerMax
     *
     * @return Reporting
     */
    public function setDangerMax($dangerMax)
    {
        $this->dangerMax = $dangerMax;

        return $this;
    }

    /**
     * Get dangerMax
     *
     * @return float
     */
    public function getDangerMax()
    {
        return $this->dangerMax;
    }

    /**
     * Set listChekDanger
     *
     * @param array $listChekDanger
     *
     * @return Reporting
     */
    public function setListChekDanger($listChekDanger)
    {
        $this->listChekDanger = $listChekDanger;

        return $this;
    }

    /**
     * Get listChekDanger
     *
     * @return array
     */
    public function getListChekDanger()
    {
        return $this->listChekDanger;
    }

    /**
     * Set prodDeptId
     *
     * @param integer $prodDeptId
     *
     * @return Reporting
     */
    public function setProdDeptId($prodDeptId) 
    {
        $this->prodDeptId = $prodDeptId;

        return $this;
    }

    /**
     * Get prodDeptId
     *
     * @return int
     */
    public function getProdDeptId()
    {
        return $this->prodDeptId;
    }

    /**
     * Set prodDeptName
     *
     * @param string $prodDeptName
     *
     * @return Reporting
     */
    public function setProdDeptName($prodDeptName)
    {
        $this->prodDeptName = $prodDeptName;

        return $this;
    }

    /**
     * Get prodDeptName
     *
     * @return string
     */
    public function getProdDeptName()
    {
        return $this->prodDeptName;
    }

    /**
     * Set cropBlockName
     *
     * @param string $cropBlockName
     *
     * @return Reporting
     */
    public function setCropBlockName($cropBlockName)
    {
        $this->cropBlockName = $cropBlockName;

        return $this;
    }

    /**
     * Get cropBlockName
     *
     * @return string
     */
    public function getCropBlockName()
    {
        return $this->cropBlockName;
    }

    /**
     * Set dateScouting
     *
     * @param \DateTime $dateScouting
     *
     * @return Reporting
     */
    public function setDateScouting($dateScouting)
    {
        $this->dateScouting = $dateScouting;

        return $this;
    }

    /**
     * Get dateScouting
     *
     * @return \DateTime
     */
    public function getDateScouting()
    {
        return $this->dateScouting;
    }

    /**
     * Set variation
     *
     * @param float $variation
     *
     * @return Reporting
     */
    public function setVariation($variation)
    {
        $this->variation = $variation;

        return $this;
    }

    /**
     * Get variation
     *
     * @return float
     */
    public function getVariation()
    {
        return $this->variation;
    }

    /**
     * Set farmId
     *
     * @param integer $farmId
     *
     * @return Reporting
     */
    public function setFarmId($farmId)
    {
        $this->farmId = $farmId;

        return $this;
    }

    /**
     * Get farmId
     *
     * @return integer
     */
    public function getFarmId()
    {
        return $this->farmId;
    }

    /**
     * Set farmName
     *
     * @param string $farmName
     *
     * @return Reporting
     */
    public function setFarmName($farmName)
    {
        $this->farmName = $farmName;

        return $this;
    }

    /**
     * Get farmName
     *
     * @return string
     */
    public function getFarmName()
    {
        return $this->farmName;
    }
    public function __toString(){
        return  "Reporting -". $this->getFarmName();

    }

    

     
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->amoutcardPosition = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add amoutcardPosition
     *
     * @param \AppBundle\Entity\AmountPositionCard $amoutcardPosition
     *
     * @return Reporting
     */
    public function addAmoutcardPosition(\AppBundle\Entity\AmountPositionCard $amoutcardPosition)
    {
        $this->amoutcardPosition[] = $amoutcardPosition;

        return $this;
    }

    /**
     * Remove amoutcardPosition
     *
     * @param \AppBundle\Entity\AmountPositionCard $amoutcardPosition
     */
    public function removeAmoutcardPosition(\AppBundle\Entity\AmountPositionCard $amoutcardPosition)
    {
        $this->amoutcardPosition->removeElement($amoutcardPosition);
    }

    /**
     * Get amoutcardPosition
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAmoutcardPosition()
    {
        return $this->amoutcardPosition;
    }
}
