<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * AmountPositionCard
 *
 * @ORM\Table(name="`bt_amount_position_card`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AmountPositionCardRepository")
 *
 */
class AmountPositionCard
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     
    /**
     * @ORM\ManyToOne(targetEntity="Reporting",inversedBy="amoutcardPosition",cascade={"persist"})
     * @ORM\JoinColumn(name="reporting_id", referencedColumnName="id", nullable=false)
     */
    private $Reporting;
     /**
      * @var float
      *
      * @ORM\Column(name="amount", type="float")
     */
    private $amount;
    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=100,nullable=true)
     */
    private $latitude; 
    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length=100,nullable=true)
     */
    private $longitude;
    /**
     * @var int
     *
     * @ORM\Column(name="cardId", type="integer")
     */
    private $cardId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return AmountPositionCard
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return AmountPositionCard
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return AmountPositionCard
     */
    public function setLongitude($longitude)  
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set cardId
     *
     * @param integer $cardId
     *
     * @return AmountPositionCard
     */
    public function setCardId($cardId) 
    {
        $this->cardId = $cardId;

        return $this;
    }

    /**
     * Get cardId
     *
     * @return integer
     */
    public function getCardId()
    {
        return $this->cardId;
    }

    /**
     * Set reporting
     *
     * @param \AppBundle\Entity\Reporting $reporting
     *
     * @return AmountPositionCard
     */ 
    public function setReporting(\AppBundle\Entity\Reporting $reporting) 
    {
        $this->Reporting = $reporting;

        return $this;
    }

    /**
     * Get reporting
     *
     * @return \AppBundle\Entity\Reporting
     */
    public function getReporting()
    {
        return $this->Reporting;
    }
}
