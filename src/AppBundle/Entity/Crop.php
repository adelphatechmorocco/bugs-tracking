<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Crop
 *
 * @ORM\Table(name="`bt_crop`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CropRepository")
 * @Vich\Uploadable
 */
class Crop
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
     private $id;

     /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
      private $enabled;

     
     /**
     *
     * @ORM\ManyToOne(targetEntity="CropType", inversedBy="corptypeCrop" ,cascade={"remove"})
     * @ORM\JoinColumn(name="croptype_id", referencedColumnName="id" )
     */
     private $cropCorptype;

      /**
     * @ORM\OneToMany(targetEntity="BugsCropsConfig", mappedBy="crop",orphanRemoval=true ,cascade={"All"})
     */
     private $cropconfig;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text",nullable=true)
     */
    private $description;
    /**
     * @Vich\UploadableField(mapping="image_bug", fileNameProperty="path",nullable=true)
     *
     * @var File
     */
     private $pathFile;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255,nullable=true)
     */
    private $path;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
     private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
     
   
  /**
     * @param array $path
     */
    public function setPathFile($path = null)
    {
       
        if ($path instanceof File) {
             $this->pathFile = $path;
             $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getPathFile()
    {
        return $this->pathFile;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Crop
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Crop
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Crop
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set cropCb
     *
     * @param \AppBundle\Entity\CropBlock $cropCb
     *
     * @return Crop
     */
    public function setCropCb(\AppBundle\Entity\CropBlock $cropCb = null)
    {
        $this->crop_cb = $cropCb;

        return $this;
    }

    /**
     * Get cropCb
     *
     * @return \AppBundle\Entity\CropBlock
     */
    public function getCropCb()
    {
        return $this->crop_cb;
    }

    /**
     * Set cropCt
     *
     * @param \AppBundle\Entity\CropType $cropCt
     *
     * @return Crop
     */
    public function setCropCt(\AppBundle\Entity\CropType $cropCt = null)
    {
        $this->crop_ct = $cropCt;

        return $this;
    }

    /**
     * Get cropCt
     *
     * @return \AppBundle\Entity\CropType
     */
    public function getCropCt()
    {
        return $this->crop_ct;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Crop
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
    public function __toString(){
        return ($this->getName() ) ? $this->getName() : '-';
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Crop
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return Crop
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Crop
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set cropCorptype
     *
     * @param \AppBundle\Entity\CropType $cropCorptype
     *
     * @return Crop
     */
    public function setCropCorptype(\AppBundle\Entity\CropType $cropCorptype = null)
    {
        $this->cropCorptype = $cropCorptype;

        return $this;
    }

    /**
     * Get cropCorptype
     *
     * @return \AppBundle\Entity\CropType
     */
    public function getCropCorptype()
    {
        return $this->cropCorptype;
    }



    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Crop
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cropconfig = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add cropconfig
     *
     * @param \AppBundle\Entity\BugsCropsConfig $cropconfig
     *
     * @return Crop
     */
    public function addCropconfig(\AppBundle\Entity\BugsCropsConfig $cropconfig)
    {
        $this->cropconfig[] = $cropconfig;

        return $this;
    }

    /**
     * Remove cropconfig
     *
     * @param \AppBundle\Entity\BugsCropsConfig $cropconfig
     */
    public function removeCropconfig(\AppBundle\Entity\BugsCropsConfig $cropconfig)
    {
        $this->cropconfig->removeElement($cropconfig);
    }

    /**
     * Get cropconfig
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCropconfig()
    {
        return $this->cropconfig;
    }
}
