<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * CropBlock
 *
 * @ORM\Table(name="`bt_crop_block`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CropBlockRepository")
 */
class CropBlock
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
     private $id;

      /**
       * @ORM\ManyToOne(targetEntity="ProductionDepartement", inversedBy="productiondepartementCropblock")
       * @ORM\JoinColumn(name="productiondepartement_id", referencedColumnName="id")
       */
       private $cropblockProductiondepartement;
    /**
     * @ORM\ManyToOne(targetEntity="Crop")
     * $@ORM\JoinColumn(name="Crop_id", referencedColumnName="id")
     */
        private $Crop;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="area", type="float")
     */
    private $area;

    /**
     * @var float
     *
     * @ORM\Column(name="positionlat", type="float")
     */
    private $positionlat;

    /**
     * @var float
     *
     * @ORM\Column(name="positionlang", type="float")
     */
    private $positionlang;

    /**
     * @var integer
     *
     * @ORM\Column(name="positionzoom", type="integer")
     */
    private $positionzoom;

  
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="date")
     */
    private $startDate;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="date",nullable=true)
     */
    private $endDate;
         /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
     private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
     /**
     * @var string
     *
     * @ORM\Column(name="polygoninformation", type="text", nullable=true )
     */
    private $polygoninformation;
    /**
     * @var float
     *
     * @ORM\Column(name="centerOfPolyLat", type="float")
     */
    private $centerOfPolyLat;
   /**
     * @var float
     *
     * @ORM\Column(name="centerOfPolyLang", type="float")
     */
    private $centerOfPolyLang;

   

  public function __toString(){
        return ($this->getName()) ? $this->getName() : '-';
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CropBlock
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set area
     *
     * @param float $area
     *
     * @return CropBlock
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return float
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set positionlat
     *
     * @param float $positionlat
     *
     * @return CropBlock
     */
    public function setPositionlat($positionlat)
    {
        $this->positionlat = $positionlat;

        return $this;
    }

    /**
     * Get positionlat
     *
     * @return float
     */
    public function getPositionlat()
    {
        return $this->positionlat;
    }

    /**
     * Set positionlang
     *
     * @param float $positionlang
     *
     * @return CropBlock
     */
    public function setPositionlang($positionlang)
    {
        $this->positionlang = $positionlang;

        return $this;
    }

    /**
     * Get positionlang
     *
     * @return float
     */
    public function getPositionlang()
    {
        return $this->positionlang;
    }

    /**
     * Set positionzoom
     *
     * @param integer $positionzoom
     *
     * @return CropBlock
     */
    public function setPositionzoom($positionzoom)
    {
        $this->positionzoom = $positionzoom;

        return $this;
    }

    /**
     * Get positionzoom
     *
     * @return integer
     */
    public function getPositionzoom()
    {
        return $this->positionzoom;
    }

   
    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return CropBlock
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return CropBlock
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CropBlock
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return CropBlock
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set polygoninformation
     *
     * @param string $polygoninformation
     *
     * @return CropBlock
     */
    public function setPolygoninformation($polygoninformation)
    {
        $this->polygoninformation = $polygoninformation;

        return $this;
    }

    /**
     * Get polygoninformation
     *
     * @return string
     */
    public function getPolygoninformation()
    {
        return $this->polygoninformation;
    }

    /**
     * Set cropblockProductiondepartement
     *
     * @param \AppBundle\Entity\ProductionDepartement $cropblockProductiondepartement
     *
     * @return CropBlock
     */
    public function setCropblockProductiondepartement(\AppBundle\Entity\ProductionDepartement $cropblockProductiondepartement = null)
    {
        $this->cropblockProductiondepartement = $cropblockProductiondepartement;

        return $this;
    }

    /**
     * Get cropblockProductiondepartement
     *
     * @return \AppBundle\Entity\ProductionDepartement
     */
    public function getCropblockProductiondepartement()
    {
        return $this->cropblockProductiondepartement;
    }

    /**
     * Set crop
     *
     * @param \AppBundle\Entity\Crop $crop
     *
     * @return CropBlock
     */
    public function setCrop(\AppBundle\Entity\Crop $crop = null)
    {
        $this->Crop = $crop;

        return $this;
    }

    /**
     * Get crop
     *
     * @return \AppBundle\Entity\Crop
     */
    public function getCrop()
    {
        return $this->Crop;
    }

   
    /**
     * Set centerOfPolyLat
     *
     * @param float $centerOfPolyLat
     *
     * @return CropBlock
     */
    public function setCenterOfPolyLat($centerOfPolyLat)
    {
        $this->centerOfPolyLat = $centerOfPolyLat;

        return $this;
    }

    /**
     * Get centerOfPolyLat
     *
     * @return float
     */
    public function getCenterOfPolyLat()
    {
        return $this->centerOfPolyLat;
    }

    /**
     * Set centerOfPolyLang
     *
     * @param float $centerOfPolyLang
     *
     * @return CropBlock
     */
    public function setCenterOfPolyLang($centerOfPolyLang)
    {
        $this->centerOfPolyLang = $centerOfPolyLang;

        return $this;
    }

    /**
     * Get centerOfPolyLang
     *
     * @return float
     */
    public function getCenterOfPolyLang()
    {
        return $this->centerOfPolyLang;
    }
}
