<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * ProductionDepartement
 *
 * @ORM\Table(name="`bt_production_departement`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductionDepartementRepository")
 */
class ProductionDepartement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Farms", inversedBy="farmProductiondepartement")
     * @ORM\JoinColumn(name="farm_id", referencedColumnName="id")
     */
     private $productiondepartementFarm;
     
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="UserProductionDepartement")
     * @ORM\JoinColumn(name="grower_id", referencedColumnName="id")
     */
     private $ProductionDepartementUser;

     /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="operatorProductionDepartement",cascade={"persist"})
     */
     private $ProductionDepartementOperator;
     



    /**
     * @ORM\OneToMany(targetEntity="CropBlock", mappedBy="cropblockProductiondepartement" , orphanRemoval=true,cascade={"persist", "remove"})
     */
     private $productiondepartementCropblock;

   /**
    * @ORM\OneToMany(targetEntity="CardLocation", mappedBy="cardlocationProductiondepartement" , orphanRemoval=true,cascade={"persist", "remove"})
    */
    private $ProductiondepartementCardlocation;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
     private $name;
     /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
     private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
     /**
     * @var string
     *
     * @ORM\Column(name="polygoninformation", type="text", nullable=true )
     */
    private $polygoninformation;
         /**
     * @var float
     *
     * @ORM\Column(name="positionlat", type="float")
     */
    private $positionlat;

    /**
     * @var float
     *
     * @ORM\Column(name="positionlang", type="float")
     */
    private $positionlang;
    /**
     * @var float
     *
     * @ORM\Column(name="centerOfPolyLat", type="float")
     */
    private $centerOfPolyLat;
   /**
     * @var float
     *
     * @ORM\Column(name="centerOfPolyLang", type="float")
     */
    private $centerOfPolyLang;

    /**
     * @var integer
     *
     * @ORM\Column(name="positionzoom", type="integer")
     */

    private $positionzoom;

    
    /**
     * @var float
     *
     * @ORM\Column(name="area", type="float")
     */
    private $area;
        
        
    public function __toString(){

        return ($this->getName() ) ? $this->getName() : "-";
    }
    
  
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productiondepartementCropblock = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ProductiondepartementCardlocation = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ProductionDepartement
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ProductionDepartement
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ProductionDepartement
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set polygoninformation
     *
     * @param string $polygoninformation
     *
     * @return ProductionDepartement
     */
    public function setPolygoninformation($polygoninformation)
    {
        $this->polygoninformation = $polygoninformation;

        return $this;
    }

    /**
     * Get polygoninformation
     *
     * @return string
     */
    public function getPolygoninformation()
    {
        return $this->polygoninformation;
    }

    /**
     * Set positionlat
     *
     * @param float $positionlat
     *
     * @return ProductionDepartement
     */
    public function setPositionlat($positionlat)
    {
        $this->positionlat = $positionlat;

        return $this;
    }

    /**
     * Get positionlat
     *
     * @return float
     */
    public function getPositionlat()
    {
        return $this->positionlat;
    }

    /**
     * Set positionlang
     *
     * @param float $positionlang
     *
     * @return ProductionDepartement
     */
    public function setPositionlang($positionlang)
    {
        $this->positionlang = $positionlang;

        return $this;
    }

    /**
     * Get positionlang
     *
     * @return float
     */
    public function getPositionlang()
    {
        return $this->positionlang;
    }

    /**
     * Set positionzoom
     *
     * @param integer $positionzoom
     *
     * @return ProductionDepartement
     */
    public function setPositionzoom($positionzoom)
    {
        $this->positionzoom = $positionzoom;

        return $this;
    }

    /**
     * Get positionzoom
     *
     * @return integer
     */
    public function getPositionzoom()
    {
        return $this->positionzoom;
    }

    /**
     * Set productiondepartementFarm
     *
     * @param \AppBundle\Entity\Farms $productiondepartementFarm
     *
     * @return ProductionDepartement
     */
    public function setProductiondepartementFarm(\AppBundle\Entity\Farms $productiondepartementFarm = null)
    {
        $this->productiondepartementFarm = $productiondepartementFarm;

        return $this;
    }

    /**
     * Get productiondepartementFarm
     *
     * @return \AppBundle\Entity\Farms
     */
    public function getProductiondepartementFarm()
    {
        return $this->productiondepartementFarm;
    }

    /**
     * Add productiondepartementCropblock
     *
     * @param \AppBundle\Entity\CropBlock $productiondepartementCropblock
     *
     * @return ProductionDepartement
     */
    public function addProductiondepartementCropblock(\AppBundle\Entity\CropBlock $productiondepartementCropblock)
    {
        $this->productiondepartementCropblock[] = $productiondepartementCropblock;

        return $this;
    }

    /**
     * Remove productiondepartementCropblock
     *
     * @param \AppBundle\Entity\CropBlock $productiondepartementCropblock
     */
    public function removeProductiondepartementCropblock(\AppBundle\Entity\CropBlock $productiondepartementCropblock)
    {
        $this->productiondepartementCropblock->removeElement($productiondepartementCropblock);
    }

    /**
     * Get productiondepartementCropblock
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductiondepartementCropblock()
    {
        return $this->productiondepartementCropblock;
    }

    /**
     * Add productiondepartementCardlocation
     *
     * @param \AppBundle\Entity\CardLocation $productiondepartementCardlocation
     *
     * @return ProductionDepartement
     */
    public function addProductiondepartementCardlocation(\AppBundle\Entity\CardLocation $productiondepartementCardlocation)
    {
        $this->ProductiondepartementCardlocation[] = $productiondepartementCardlocation;

        return $this;
    }

    /**
     * Remove productiondepartementCardlocation
     *
     * @param \AppBundle\Entity\CardLocation $productiondepartementCardlocation
     */
    public function removeProductiondepartementCardlocation(\AppBundle\Entity\CardLocation $productiondepartementCardlocation)
    {
        $this->ProductiondepartementCardlocation->removeElement($productiondepartementCardlocation);
    }

    /**
     * Get productiondepartementCardlocation
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductiondepartementCardlocation()
    {
        return $this->ProductiondepartementCardlocation;
    }

 

    /**
     * Set productionDepartementUser
     *
     * @param \AppBundle\Entity\User $productionDepartementUser
     *
     * @return ProductionDepartement
     */
    public function setProductionDepartementUser(\AppBundle\Entity\User $productionDepartementUser = null)
    {
        $this->ProductionDepartementUser = $productionDepartementUser;

        return $this;
    }

    /**
     * Get productionDepartementUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getProductionDepartementUser()
    {
        return $this->ProductionDepartementUser;
    }

    /**
     * Set centerOfPolyLat
     *
     * @param float $centerOfPolyLat
     *
     * @return ProductionDepartement
     */
    public function setCenterOfPolyLat($centerOfPolyLat)
    {
        $this->centerOfPolyLat = $centerOfPolyLat;

        return $this;
    }

    /**
     * Get centerOfPolyLat
     *
     * @return float
     */
    public function getCenterOfPolyLat()
    {
        return $this->centerOfPolyLat;
    }

    /**
     * Set centerOfPolyLang
     *
     * @param float $centerOfPolyLang
     *
     * @return ProductionDepartement
     */
    public function setCenterOfPolyLang($centerOfPolyLang)
    {
        $this->centerOfPolyLang = $centerOfPolyLang;

        return $this;
    }

    /**
     * Get centerOfPolyLang
     *
     * @return float
     */
    public function getCenterOfPolyLang()
    {
        return $this->centerOfPolyLang;
    }

    /**
     * Set area
     *
     * @param float $area
     *
     * @return ProductionDepartement
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return float
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Add productionDepartementOperator
     *
     * @param \AppBundle\Entity\User $productionDepartementOperator
     *
     * @return ProductionDepartement
     */
    public function addProductionDepartementOperator(\AppBundle\Entity\User $productionDepartementOperator)
    {
        $this->ProductionDepartementOperator[] = $productionDepartementOperator;

        return $this;
    }

    /**
     * Remove productionDepartementOperator
     *
     * @param \AppBundle\Entity\User $productionDepartementOperator
     */
    public function removeProductionDepartementOperator(\AppBundle\Entity\User $productionDepartementOperator)
    {
        $this->ProductionDepartementOperator->removeElement($productionDepartementOperator);
    }

    /**
     * Get productionDepartementOperator
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductionDepartementOperator()
    {
        return $this->ProductionDepartementOperator;
    }
}
