<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * Recording
 *
 * @ORM\Table(name="`bt_recording`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RecordingRepository")
 */
class Recording
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
      private $id;

      /**
       * @ORM\ManyToOne(targetEntity="User", inversedBy="userRecording")
       * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
       */
       private $recordingUser;

       /**
       * @ORM\ManyToOne(targetEntity="CardLocation", inversedBy="cardlocationRecording")
       * @ORM\JoinColumn(name="cardlocation_id", referencedColumnName="id")
       */
       private $recordingCardlocation; 
        

     /**
     * @ORM\OneToMany(targetEntity="RecordingBugs", mappedBy="recordingbugsRecording", cascade={"All"})
    
     */
     private $recordingRecordingbugs;

        /**
         * @var \Date
         *
         * @ORM\Column(name="scouting_date", type="date")
         */
        private $scoutingDate;

 
     /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
     private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    


    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set scoutingDate
     *
     * @param \DateTime $scoutingDate
     *
     * @return Recording
     */
    public function setScoutingDate($scoutingDate)
    {
        $this->scoutingDate = $scoutingDate;

        return $this;
    }

    /**
     * Get scoutingDate
     *
     * @return \DateTime
     */
    public function getScoutingDate()
    {
        return $this->scoutingDate;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return Recording
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Recording
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Recording
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set recordingUser
     *
     * @param \AppBundle\Entity\User $recordingUser
     *
     * @return Recording
     */
    public function setRecordingUser(\AppBundle\Entity\User $recordingUser = null)
    {
        $this->recordingUser = $recordingUser;

        return $this;
    }

    /**
     * Get recordingUser
     *
     * @return \AppBundle\Entity\User
     */
    public function getRecordingUser()
    {
        return $this->recordingUser;
    }

    /**
     * Set recordingCardlocation
     *
     * @param \AppBundle\Entity\CardLocation $recordingCardlocation
     *
     * @return Recording
     */
    public function setRecordingCardlocation(\AppBundle\Entity\CardLocation $recordingCardlocation = null)
    {
        $this->recordingCardlocation = $recordingCardlocation;

        return $this;
    }

    /**
     * Get recordingCardlocation
     *
     * @return \AppBundle\Entity\CardLocation
     */
    public function getRecordingCardlocation()
    {
        return $this->recordingCardlocation;
    }

 
     public function __toString(){

        return ($this->getId()) ? $this->getId() : '-';
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->recordingRecordingbugs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add recordingRecordingbug
     *
     * @param \AppBundle\Entity\RecordingBugs $recordingRecordingbug
     *
     * @return Recording
     */
    public function addRecordingRecordingbug(\AppBundle\Entity\RecordingBugs $recordingRecordingbug)
    {
        $recordingRecordingbug->setRecordingbugsRecording($this);
        $this->recordingRecordingbugs[] = $recordingRecordingbug;

        return $this;
    }

    /**
     * Remove recordingRecordingbug
     *
     * @param \AppBundle\Entity\RecordingBugs $recordingRecordingbug
     */
    public function removeRecordingRecordingbug(\AppBundle\Entity\RecordingBugs $recordingRecordingbug)
    {
        $this->recordingRecordingbugs->removeElement($recordingRecordingbug);
    }

    /**
     * Get recordingRecordingbugs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecordingRecordingbugs()
    {
        return $this->recordingRecordingbugs;
    }
}
