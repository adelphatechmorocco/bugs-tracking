<?php

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * RecordingBugs
 *
 * @ORM\Table(name="`bt_bugs_crops_config`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RecordingBugsRepository")
 */
class BugsCropsConfig
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Bug", inversedBy="bugconfig")
     * @ORM\JoinColumn(name="bug_id", referencedColumnName="id", nullable=false)
     */
    private $bug;

    /**
     * @ORM\ManyToOne(targetEntity="Crop", inversedBy="cropconfig")
     * @ORM\JoinColumn(name="crop_id", referencedColumnName="id", nullable=false)
     */
    private $crop;
    /**
     * @var int
     *
     * @ORM\Column(name="max", type="integer")
     */
    private $max;
    /**
     * @var int
     *
     * @ORM\Column(name="min", type="integer")
     */
    private $min;



    public function __toString()
    {
        return $this->getId();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set max
     *
     * @param integer $max
     *
     * @return BugsCropsConfig
     */
    public function setMax($max)
    {
        $this->max = $max;

        return $this;
    }

    /**
     * Get max
     *
     * @return integer
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * Set min
     *
     * @param integer $min
     *
     * @return BugsCropsConfig
     */
    public function setMin($min)
    {
        $this->min = $min;

        return $this;
    }

    /**
     * Get min
     *
     * @return integer
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * Set bug
     *
     * @param \AppBundle\Entity\bug $bug
     *
     * @return BugsCropsConfig
     */
    public function setBug(\AppBundle\Entity\bug $bug)
    {
        $this->bug = $bug;

        return $this;
    }

    /**
     * Get bug
     *
     * @return \AppBundle\Entity\bug
     */
    public function getBug()
    {
        return $this->bug;
    }

    /**
     * Set crop
     *
     * @param \AppBundle\Entity\Crop $crop
     *
     * @return BugsCropsConfig
     */
    public function setCrop(\AppBundle\Entity\Crop $crop)
    {
        $this->crop = $crop;

        return $this;
    }

    /**
     * Get crop
     *
     * @return \AppBundle\Entity\Crop
     */
    public function getCrop()
    {
        return $this->crop;
    }
}
