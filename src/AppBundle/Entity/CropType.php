<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * CropType
 *
 * @ORM\Table(name="`bt_crop_type`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CropTypeRepository")
 */
class CropType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Crop", mappedBy="cropCorptype" , orphanRemoval=true,cascade={"persist", "remove"})
     */
    private $corptypeCrop;

    /**
     * @var string
     *
     * @ORM\Column(name="crop_type", type="string", length=255)
     */
    private $Type;
  /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
     private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
        /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
     private $enabled;

   

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cropType
     *
     * @param string $cropType
     *
     * @return CropType
     */
    public function setCropType($cropType)
    {
        $this->cropType = $cropType;

        return $this;
    }

    /**
     * Get cropType
     *
     * @return string
     */
    public function getCropType()
    {
        return $this->cropType;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return CropType
     */
    public function setType($type)
    {
        $this->Type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->Type;
    }

    /**
     * Set corptypeC
     *
     * @param \AppBundle\Entity\Crop $corptypeC
     *
     * @return CropType
     */
    public function setCorptypeC(\AppBundle\Entity\Crop $corptypeC = null)
    {
        $this->corptype_c = $corptypeC;

        return $this;
    }

    /**
     * Get corptypeC
     *
     * @return \AppBundle\Entity\Crop
     */
    public function getCorptypeC()
    {
        return $this->corptype_c;
    }
    public function __toString(){

        return ( $this->getType() ) ? $this->getType() : "-";
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->corptype_c = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return CropType
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return CropType
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Add corptypeC
     *
     * @param \AppBundle\Entity\Crop $corptypeC
     *
     * @return CropType
     */
    public function addCorptypeC(\AppBundle\Entity\Crop $corptypeC)
    {
        $this->corptype_c[] = $corptypeC;

        return $this;
    }

    /**
     * Remove corptypeC
     *
     * @param \AppBundle\Entity\Crop $corptypeC
     */
    public function removeCorptypeC(\AppBundle\Entity\Crop $corptypeC)
    {
        $this->corptype_c->removeElement($corptypeC);
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CropType
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add corptypeCrop
     *
     * @param \AppBundle\Entity\Crop $corptypeCrop
     *
     * @return CropType
     */
    public function addCorptypeCrop(\AppBundle\Entity\Crop $corptypeCrop)
    {
        $this->corptypeCrop[] = $corptypeCrop;

        return $this;
    }

    /**
     * Remove corptypeCrop
     *
     * @param \AppBundle\Entity\Crop $corptypeCrop
     */
    public function removeCorptypeCrop(\AppBundle\Entity\Crop $corptypeCrop)
    {
        $this->corptypeCrop->removeElement($corptypeCrop);
    }

    /**
     * Get corptypeCrop
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCorptypeCrop()
    {
        return $this->corptypeCrop;
    }

   

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return CropType
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}
