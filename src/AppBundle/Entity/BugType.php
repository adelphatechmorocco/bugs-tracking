<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * BugType
 *
 * @ORM\Table(name="`bt_bug_type`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BugTypeRepository")
 */
class BugType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToMany(targetEntity="Bug", mappedBy="bugBugType" , orphanRemoval=true,cascade={"persist", "remove"})
     */
    private $bugtypeBug;


    /**
     * @var string
     *
     * @ORM\Column(name="BugType", type="string", length=255)
     */
    private $bugType;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;
    /**
     * @ORM\Column(type="datetime",nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;
   /**
     * @ORM\Column(type="datetime",nullable=true)
     *
     * @var \DateTime
     */
    private $createAt;
    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
     private $enabled;

     
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bugType
     *
     * @param string $bugType
     *
     * @return BugType
     */
    public function setBugType($bugType)
    {
        $this->bugType = $bugType;

        return $this;
    }

    /**
     * Get bugType
     *
     * @return string
     */
    public function getBugType()
    {
        return $this->bugType;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return BugType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set bugtypeB
     *
     * @param \AppBundle\Entity\Bug $bugtypeB
     *
     * @return BugType
     */
    public function setBugtypeB(\AppBundle\Entity\Bug $bugtypeB = null)
    {
        $this->bugtype_b = $bugtypeB;

        return $this;
    }

    /**
     * Get bugtypeB
     *
     * @return \AppBundle\Entity\Bug
     */
    public function getBugtypeB()
    {
        return $this->bugtype_b;
    }


    public function __toString(){
            return ($this->getBugType())? $this->getBugType() : "-";
    }
    


    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return BugType
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return BugType
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set bugtypeBug
     *
     * @param \AppBundle\Entity\Bug $bugtypeBug
     *
     * @return BugType
     */
    public function setBugtypeBug(\AppBundle\Entity\Bug $bugtypeBug = null)
    {
        $this->bugtypeBug = $bugtypeBug;

        return $this;
    }

    /**
     * Get bugtypeBug
     *
     * @return \AppBundle\Entity\Bug
     */
    public function getBugtypeBug()
    {
        return $this->bugtypeBug;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bugtypeBug = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add bugtypeBug
     *
     * @param \AppBundle\Entity\Bug $bugtypeBug
     *
     * @return BugType
     */
    public function addBugtypeBug(\AppBundle\Entity\Bug $bugtypeBug)
    {
        $this->bugtypeBug[] = $bugtypeBug;

        return $this;
    }

    /**
     * Remove bugtypeBug
     *
     * @param \AppBundle\Entity\Bug $bugtypeBug
     */
    public function removeBugtypeBug(\AppBundle\Entity\Bug $bugtypeBug)
    {
        $this->bugtypeBug->removeElement($bugtypeBug);
    }

   

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return BugType
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}
