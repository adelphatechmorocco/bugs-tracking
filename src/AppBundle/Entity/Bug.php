<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Bug
 *
 * @ORM\Table(name="`bt_bug`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BugRepository")
 * @Vich\Uploadable
 */
class Bug
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

      /**
       * @ORM\ManyToOne(targetEntity="BugType", inversedBy="bugtypeBug")
       * @ORM\JoinColumn(name="bugtype_id", referencedColumnName="id")
       */
      private $bugBugType;


     /**
     * @ORM\OneToMany(targetEntity="BugsCropsConfig", mappedBy="bug",orphanRemoval=true, cascade={"All"})
     */
     private $bugconfig;
       

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
     private $name;
    /**
     * @var string
     *
     * @ORM\Column(name="nameType", type="string", length=50)
     */
     private $nameType;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text",nullable=true)
     */
    private $description;

     /**
     * @Vich\UploadableField(mapping="image_bug", fileNameProperty="path",nullable=true)
     *
     * @var File
     */
     private $pathFile;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255,nullable=true)
     */
    private $path;   
     /**
     * @var integer
     *
     * @ORM\Column(name="priority", type="integer")
     */
     private $priority;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
     private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
     /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
     private $enabled;


    /**
     * @param array $path
     */
    public function setPathFile($path = null)
    {
       
        if ($path instanceof File) {
            $this->pathFile = $path;
            $this->updatedAt = new \DateTime('now');
          
        }
    }

    /**
     * @return File
     */
    public function getPathFile()
    {
        return $this->pathFile;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Bug
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
  

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Bug
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Set bugBt
     *
     * @param \AppBundle\Entity\BugType $bugBt
     *
     * @return Bug
     */
    public function setBugBt(\AppBundle\Entity\BugType $bugBt = null)
    {
        $this->bug_bt = $bugBt;

        return $this;
    }

    /**
     * Get bugBt
     *
     * @return \AppBundle\Entity\BugType
     */
    public function getBugBt()
    {
        return $this->bug_bt;
    }
    

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Bug
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
    public function __toString(){


            if($this->getName() && $this->getPriority())
            {
                    return $this->getName().'-P'. $this->getPriority();

            }elseif ($this->getName() && !$this->getPriority()) {
                      return $this->getName();
            }elseif (!$this->getName() && $this->getPriority()) {
               
                      return   $this->getPriority();
            }else{
                return '-';
            }

    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Bug
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return Bug
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Bug
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set bugBugType
     *
     * @param \AppBundle\Entity\BugType $bugBugType
     *
     * @return Bug
     */
    public function setBugBugType(\AppBundle\Entity\BugType $bugBugType = null)
    {
        $this->bugBugType = $bugBugType;

        return $this;
    }

    /**
     * Get bugBugType
     *
     * @return \AppBundle\Entity\BugType
     */
    public function getBugBugType()
    {
        return $this->bugBugType;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     *
     * @return Bug
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bugRecording = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add bugRecording
     *
     * @param \AppBundle\Entity\Recording $bugRecording
     *
     * @return Bug
     */
    public function addBugRecording(\AppBundle\Entity\Recording $bugRecording)
    {
        $this->bugRecording[] = $bugRecording;

        return $this;
    }

    /**
     * Remove bugRecording
     *
     * @param \AppBundle\Entity\Recording $bugRecording
     */
    public function removeBugRecording(\AppBundle\Entity\Recording $bugRecording)
    {
        $this->bugRecording->removeElement($bugRecording);
    }

    /**
     * Get bugRecording
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBugRecording()
    {
        return $this->bugRecording;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Bug
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set nameType
     *
     * @param string $nameType
     *
     * @return Bug
     */
    public function setNameType($nameType)
    {
        $this->nameType = $nameType;

        return $this;
    }

    /**
     * Get nameType
     *
     * @return string
     */
    public function getNameType()
    {
        return $this->nameType;
    }

    /**
     * Add bugconfig
     *
     * @param \AppBundle\Entity\BugsCropsConfig $bugconfig
     *
     * @return Bug
     */
    public function addBugconfig(\AppBundle\Entity\BugsCropsConfig $bugconfig)
    {
        $this->bugconfig[] = $bugconfig;

        return $this;
    }

    /**
     * Remove bugconfig
     *
     * @param \AppBundle\Entity\BugsCropsConfig $bugconfig
     */
    public function removeBugconfig(\AppBundle\Entity\BugsCropsConfig $bugconfig)
    {
        $this->bugconfig->removeElement($bugconfig);
    }

    /**
     * Get bugconfig
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBugconfig()
    {
        return $this->bugconfig;
    }
}
