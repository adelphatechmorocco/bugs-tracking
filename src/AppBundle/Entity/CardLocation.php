<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * CardLocation
 *
 * @ORM\Table(name="`bt_card_location`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CardLocationRepository")
 */
class CardLocation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="ProductionDepartement", inversedBy="ProductiondepartementCardlocation")
    * @ORM\JoinColumn(name="ProductionDepartement_id", referencedColumnName="id")
    */
    private $cardlocationProductiondepartement;
     

    /**
    * @ORM\OneToMany(targetEntity="Recording", mappedBy="recordingCardlocation" , orphanRemoval=true,cascade={"persist", "remove"})
    */
    private $cardlocationRecording;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=100,nullable=true)
     */
    private $latitude; 
    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length=100,nullable=true)
     */
    private $longitude;
       /**
     * @var float
     *
     * @ORM\Column(name="positionlat", type="float")
     */
    private $positionlat;

    /**
     * @var float
     *
     * @ORM\Column(name="positionlang", type="float")
     */
    private $positionlang;

    /**
     * @var integer
     *
     * @ORM\Column(name="positionzoom", type="integer")
     */
    private $positionzoom;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
     private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="date")
     */
    private $startDate;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="date",nullable=true)
     */
    private $endDate;
         /**
     * @var integer
     *
     * @ORM\Column(name="priority", type="integer")
     */
     private $priority;


    public function __toString(){
        return ($this->getName()) ? $this->getName() : '-';
    }
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cardlocationRecording = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CardLocation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return CardLocation
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return CardLocation
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set positionlat
     *
     * @param float $positionlat
     *
     * @return CardLocation
     */
    public function setPositionlat($positionlat)
    {
        $this->positionlat = $positionlat;

        return $this;
    }

    /**
     * Get positionlat
     *
     * @return float
     */
    public function getPositionlat()
    {
        return $this->positionlat;
    }

    /**
     * Set positionlang
     *
     * @param float $positionlang
     *
     * @return CardLocation
     */
    public function setPositionlang($positionlang)
    {
        $this->positionlang = $positionlang;

        return $this;
    }

    /**
     * Get positionlang
     *
     * @return float
     */
    public function getPositionlang()
    {
        return $this->positionlang;
    }

    /**
     * Set positionzoom
     *
     * @param integer $positionzoom
     *
     * @return CardLocation
     */
    public function setPositionzoom($positionzoom)
    {
        $this->positionzoom = $positionzoom;

        return $this;
    }

    /**
     * Get positionzoom
     *
     * @return integer
     */
    public function getPositionzoom()
    {
        return $this->positionzoom;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return CardLocation
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return CardLocation
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set cardlocationProductiondepartement
     *
     * @param \AppBundle\Entity\ProductionDepartement $cardlocationProductiondepartement
     *
     * @return CardLocation
     */
    public function setCardlocationProductiondepartement(\AppBundle\Entity\ProductionDepartement $cardlocationProductiondepartement = null)
    {
        $this->cardlocationProductiondepartement = $cardlocationProductiondepartement;

        return $this;
    }

    /**
     * Get cardlocationProductiondepartement
     *
     * @return \AppBundle\Entity\ProductionDepartement
     */
    public function getCardlocationProductiondepartement()
    {
        return $this->cardlocationProductiondepartement;
    }

    /**
     * Add cardlocationRecording
     *
     * @param \AppBundle\Entity\Recording $cardlocationRecording
     *
     * @return CardLocation
     */
    public function addCardlocationRecording(\AppBundle\Entity\Recording $cardlocationRecording)
    {
        $this->cardlocationRecording[] = $cardlocationRecording;

        return $this;
    }

    /**
     * Remove cardlocationRecording
     *
     * @param \AppBundle\Entity\Recording $cardlocationRecording
     */
    public function removeCardlocationRecording(\AppBundle\Entity\Recording $cardlocationRecording)
    {
        $this->cardlocationRecording->removeElement($cardlocationRecording);
    }

    /**
     * Get cardlocationRecording
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCardlocationRecording()
    {
        return $this->cardlocationRecording;
    }

   


    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return CardLocation
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return CardLocation
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     *
     * @return CardLocation
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }
}
