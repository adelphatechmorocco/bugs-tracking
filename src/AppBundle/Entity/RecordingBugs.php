<?php

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * RecordingBugs
 *
 * @ORM\Table(name="`bt_recording_bugs`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RecordingBugsRepository")
 */
class RecordingBugs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Recording", inversedBy="recordingRecordingbugs",cascade={"persist"})
     * @ORM\JoinColumn(name="recording_id", referencedColumnName="id", nullable=false)
     */
    private $recordingbugsRecording;

    /**
     * @ORM\ManyToOne(targetEntity="Bug")
     * @ORM\JoinColumn(name="bugs_id", referencedColumnName="id", nullable=false)
     */
    private $recordingbugsBugs;
    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="integer")
     */
    private $amount;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return RecordingBugs
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set recordingbugsRecording
     *
     * @param \AppBundle\Entity\Recording $recordingbugsRecording
     *
     * @return RecordingBugs
     */
    public function setRecordingbugsRecording(\AppBundle\Entity\Recording $recordingbugsRecording)
    {
        $this->recordingbugsRecording = $recordingbugsRecording;

        return $this;
    }

    /**
     * Get recordingbugsRecording
     *
     * @return \AppBundle\Entity\Recording
     */
    public function getRecordingbugsRecording()
    {
        return $this->recordingbugsRecording;
    }

    /**
     * Set recordingbugsBugs
     *
     * @param \AppBundle\Entity\Bug $recordingbugsBugs
     *
     * @return RecordingBugs
     */
    public function setRecordingbugsBugs(\AppBundle\Entity\Bug $recordingbugsBugs)
    {
        $this->recordingbugsBugs = $recordingbugsBugs;

        return $this;
    }

    /**
     * Get recordingbugsBugs
     *
     * @return \AppBundle\Entity\Bugs
     */
    public function getRecordingbugsBugs()
    {
        return $this->recordingbugsBugs;
    }
    public function __toString()
    {
        return $this->getId();
    }

}
