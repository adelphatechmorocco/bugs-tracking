<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ConfigCropFarm
 *
 * @ORM\Table(name="`bt_ConfigCropFarm`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ConfigCropFarmRepository")
 */
class ConfigCropFarm
{
    
     /**
      * @var int
      *
      * @ORM\Column(name="id", type="bigint")
      * @ORM\Id
      * @ORM\GeneratedValue(strategy="AUTO")
      */
      private $id;

       /**
        * @ORM\ManyToOne(targetEntity="Farms",inversedBy="ccf")
        * @ORM\JoinColumn(name="farm_id", referencedColumnName="id")
        */
      private $farmId;
        /**
         * @ORM\ManyToOne(targetEntity="Crop")
         * @ORM\JoinColumn(name="crop_id", referencedColumnName="id")
         */
      private $CropId;

        

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set farmId
     *
     * @param \AppBundle\Entity\Farms $farmId
     *
     * @return ConfigCropFarm
     */
    public function setFarmId(\AppBundle\Entity\Farms $farmId = null)
    {
        $this->farmId = $farmId;

        return $this;
    }

    /**
     * Get farmId
     *
     * @return \AppBundle\Entity\Farms
     */
    public function getFarmId()
    {
        return $this->farmId;
    }

    /**
     * Set cropId
     *
     * @param \AppBundle\Entity\Crop $cropId
     *
     * @return ConfigCropFarm
     */
    public function setCropId(\AppBundle\Entity\Crop $cropId = null)
    {
        $this->CropId = $cropId;

        return $this;
    }

    /**
     * Get cropId
     *
     * @return \AppBundle\Entity\Crop
     */
    public function getCropId()
    {
        return $this->CropId;
    }
}
